﻿using Docitt.Application;
using Docitt.Application.Client;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using Moq;
using RestSharp;
using System.Collections.Generic;
using Xunit;

namespace LendFoundry.Application.Client.Tests
{
    public class ApplicationServiceClientTests
    {
        private ApplicationService ApplicationServiceClient { get; }

        private IRestRequest Request { get; set; }

        private Mock<IServiceClient> MockServiceClient { get; }

        public ApplicationServiceClientTests()
        {
            MockServiceClient = new Mock<IServiceClient>();
            ApplicationServiceClient = new ApplicationService(MockServiceClient.Object);
        }

        [Fact]
        public void Client_Get_Application()
        {
            MockServiceClient.Setup(s => s.Execute<ApplicationResponse>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(
                    () =>

                        new ApplicationResponse()
                        {
                            ApplicationNumber = "123"
                        }
                );

            var result = ApplicationServiceClient.GetByApplicationNumber("123");
            Assert.Equal("/{applicationNumber}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public void Client_Add_Application()
        {
            var application = new ApplicationRequest();

            MockServiceClient.Setup(s => s.Execute<ApplicationResponse>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(
                    () =>
                        new ApplicationResponse());

            var result =  ApplicationServiceClient.Add(application);
            Assert.Equal("/", Request.Resource);
            Assert.Equal(Method.PUT, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public void Client_UpdateApplication()
        {
            var application = new ApplicationUpdateRequest();


            MockServiceClient.Setup(s => s.Execute(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(true);

            ApplicationServiceClient.UpdateApplication("123", application);
            Assert.Equal("/{applicationNumber}", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
        }
        [Fact]
        public void Client_UpdateAddresses()
        {
            MockServiceClient.Setup(s => s.Execute(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(true);

            ApplicationServiceClient.UpdateAddresses("123","123", new List<IAddress>());
            Assert.Equal("/{applicationNumber}/{applicantId}/address", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
        }

        [Fact]
        public void Client_UpdatePrimaryAddress()
        {
            MockServiceClient.Setup(s => s.Execute(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(true);
            ApplicationServiceClient.UpdatePrimaryAddress("123", new Address());
            Assert.Equal("/{applicationNumber}/address/primary", Request.Resource);
            Assert.Equal(Method.PUT, Request.Method);
        }

        [Fact]
        public void Client_UpdatePhoneNumbers()
        {
            MockServiceClient.Setup(s => s.Execute(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(true);

            ApplicationServiceClient.UpdatePhoneNumbers("123", "123", new List<IPhoneNumber>());
            Assert.Equal("/{applicationNumber}/{applicantId}/phonenumber", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
        }

        [Fact]
        public void Client_UpdateprimaryPhoneNumber()
        {
            MockServiceClient.Setup(s => s.Execute(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(true);

            ApplicationServiceClient.UpdatePrimaryPhoneNumber("123", "123", new PhoneNumber());
            Assert.Equal("/{applicationNumber}/{applicantId}/phonenumber/primary", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
        }

        [Fact]
        public void Client_UpdateApplicant()
        {
            MockServiceClient.Setup(s => s.Execute(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(true);

            ApplicationServiceClient.UpdateApplicant("123", "123", new ApplicantUpdateRequest());
            Assert.Equal("/{applicationNumber}/{applicantId}", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
        }

        [Fact]
        public void Client_GetApplicationNumbersByApplicantNumbers()
        {
            MockServiceClient.Setup(s => s.Execute<List<string>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(
                    () =>
                        new List<string>()
                        {
                            "123"
                        }
                );

            var result = ApplicationServiceClient.GetApplicationNumbersByApplicant("123");
            Assert.Equal("/applicant/{applicantId}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public void Client_AddBank()
        {
            MockServiceClient.Setup(s => s.Execute<BankAccount>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(
                    () =>
                        new BankAccount());

            ApplicationServiceClient.AddBank("123", new BankAccount());
            Assert.Equal("/{applicationNumber}/banks", Request.Resource);
            Assert.Equal(Method.PUT, Request.Method);
        }

        [Fact]
        public void Client_UpdateBank()
        {
            MockServiceClient.Setup(s => s.Execute<BankAccount>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(
                    () =>
                        new BankAccount());

            ApplicationServiceClient.UpdateBank("123","123", new BankAccount());
            Assert.Equal("/{applicationNumber}/banks/{bankId}", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
        }


        [Fact]
        public void Client_DeleteBank()
        {
            MockServiceClient.Setup(s => s.Execute(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(
                    () =>
                        true
                );

            ApplicationServiceClient.DeleteBank("123", "123");
            Assert.Equal("/{applicationNumber}/banks/{bankId}", Request.Resource);
            Assert.Equal(Method.DELETE, Request.Method);
        }

        [Fact]
        public void Client_SetBankAsPrimaryBank()
        {
            MockServiceClient.Setup(s => s.Execute(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(
                    () =>
                            true
                );

            ApplicationServiceClient.SetBankAsPrimary("123", "123");
            Assert.Equal("/{applicationNumber}/banks/{bankId}/primary", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
        }

        [Fact]
        public void Client_GetAllBanks()
        {
            MockServiceClient.Setup(s => s.Execute<List<BankAccount>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(
                    () =>
                            new List<BankAccount>()
                );

            ApplicationServiceClient.GetAllBanks("123");
            Assert.Equal("/{applicationNumber}/banks", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
        }

        [Fact]
        public void Client_GetBank()
        {
            MockServiceClient.Setup(s => s.Execute<BankAccount>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(
                    () =>
                            new BankAccount()
                );

            ApplicationServiceClient.GetBank("123","123");
            Assert.Equal("/{applicationNumber}/banks/{bankId}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
        }

        //[Fact(Skip = "Integrated test - executed w/ server only")]
        //public void GetByApplicationNumberIntegratedOne()
        //{
        //    // Host
        //    string host = "localhost";
        //    int port = 5000;

        //    // Token generation
        //    var tokenHandler = new TokenHandler(null, null, null);
        //    var token = tokenHandler.Issue("my-tenant", "application");
        //    var reader = new StaticTokenReader(token.Value);

        //    // Service client generation
        //    IServiceClient serviceClient = new ServiceClient(
        //        accessor: new EmptyHttpContextAccessor(),
        //        logger: Mock.Of<ILogger>(),
        //        tokenReader: reader,
        //        endpoint: host,
        //        port: port
        //    );

        //    var client = new ApplicationService(serviceClient);
        //    var result = client.GetByApplicationNumber("LCUSA-0000004");
        //}
    }
}
