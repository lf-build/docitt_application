﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using Moq;
using LendFoundry.Foundation.Logging;
using LendFoundry.Email.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Email;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub;
using Docitt.Application;
using Docitt.Applicant;
using Docitt.Application.Client;

namespace LendFoundry.Application.Tests
{
    public class ApplicationDeclinedEventSubscriptionTest
    {
        private IEventSubscription<ApplicationDeclined> ApplicationDeclinedEventSubscription { get; set; }

        private Mock<IApplicationRepository> ApplicationRepository { get; } = new Mock<IApplicationRepository>();
        
        private Mock<IEmailServiceFactory> EmailServiceFactory { get; } = new Mock<IEmailServiceFactory>();

        private Mock<IEmailService> EmailService { get; } = new Mock<IEmailService>();

        private Mock<ILoggerFactory> LoggerFactory { get; } = new Mock<ILoggerFactory>();

        private Mock<ILogger> Logger { get; } = new Mock<ILogger>();

        private Mock<IEventInfo> EventInfo { get; } = new Mock<IEventInfo>();

        private Mock<IApplication> Application { get; } = new Mock<IApplication>();

        private Mock<IApplicationDeclined> ApplicationDeclined { get; } = new Mock<IApplicationDeclined>();

        private Mock<IApplicantService> ApplicantService { get; } = new Mock<IApplicantService>();

        private Mock<Docitt.Applicant.IApplicant> Applicant { get; } = new Mock<Docitt.Applicant.IApplicant>();

        private Mock<IApplicantData> ApplicantData { get; } = new Mock<IApplicantData>();

        private Mock<IAddress> Address { get; } = new Mock<IAddress>();

        private Mock<IConfigurationService<Docitt.Application.Configuration>> ConfigurationService { get; } = new Mock<IConfigurationService<Docitt.Application.Configuration>>();

        private Mock<IApplicationRepositoryFactory> ApplicationRepositoryFactory { get; } = new Mock<IApplicationRepositoryFactory>();

        private Mock<IApplicantServiceFactory> ApplicantServiceFactory { get; } = new Mock<IApplicantServiceFactory>();

        private Mock<IConfigurationServiceFactory<Docitt.Application.Configuration>> ConfigurationServiceFactory { get; } = new Mock<IConfigurationServiceFactory<Docitt.Application.Configuration>>();

        private Mock<ITokenReaderFactory> TokenHandlerFactory { get; } = new Mock<ITokenReaderFactory>();


        private void SetupApplicationDeclinedEventSubscription()
        {
           //ApplicationDeclinedEventSubscription = new ApplicationDeclinedEventSubscription(
           //     ApplicationRepositoryFactory.Object,
           //     ApplicantServiceFactory.Object,
           //     LoggerFactory.Object,
           //     EmailServiceFactory.Object,
           //     ConfigurationServiceFactory.Object ,
           //     TokenHandlerFactory.Object
           //); 
        }


        private void SetupApplicant()
        {
            Applicant.SetupGet(a => a.FirstName).Returns("FirstName");
            Applicant.SetupGet(a => a.LastName).Returns("LastName");
            Applicant.SetupGet(a => a.Email).Returns("Email");
        }

        private void SetupApplication()
        {
            Address.SetupGet(a => a.City).Returns("City");
            Address.SetupGet(a => a.Country).Returns("Country");
            Address.SetupGet(a => a.IsPrimary).Returns(true);
            Address.SetupGet(a => a.Line1).Returns("Line1");
            Address.SetupGet(a => a.State).Returns("State");
            Address.SetupGet(a => a.ZipCode).Returns("ZipCode");

            ApplicantData.SetupGet(m => m.Addresses)
                .Returns(new List<IAddress>(new[] { Address.Object }));

            ApplicantData.SetupGet(a => a.ApplicantId).Returns("ApplicantId");
            ApplicantData.SetupGet(a => a.IsPrimary).Returns(true);

            Application.SetupGet(a => a.Applicants)
                .Returns(new List<IApplicantData>(new IApplicantData[] { ApplicantData.Object }));

        }

        private void SetupFactories()
        {
            ApplicationRepositoryFactory.Setup(r => r.Create(It.IsAny<ITokenReader>())).Returns(ApplicationRepository.Object);
            ApplicantServiceFactory.Setup(r => r.Create(It.IsAny<ITokenReader>())).Returns(ApplicantService.Object);
            LoggerFactory.Setup(r => r.Create(It.IsAny<ILogContext>())).Returns(Logger.Object);
            EmailServiceFactory.Setup(r => r.Create(It.IsAny<ITokenReader>())).Returns(EmailService.Object);
            ConfigurationServiceFactory.Setup(r => r.Create(It.IsAny<ITokenReader>())).Returns(ConfigurationService.Object);
            TokenHandlerFactory.Setup(r => r.Create(It.IsAny<string>(), It.IsAny<string>())).Returns(new Mock<ITokenReader>().Object);
        }

        [Fact]
        public void ApplicationDeclinedTriggeredAndApplicationNotFoundShouldLogIt()
        {
            // Given
            SetupApplicant();
            SetupApplication();
            SetupFactories();

            ApplicationRepository.Setup(x => x.GetByApplicationNumber(It.IsAny<string>()))
                .Returns((Docitt.Application.Application)null);

            Logger.Setup(x => x.Warn(It.IsAny<string>()));

            SetupApplicationDeclinedEventSubscription();

            //When
            ApplicationDeclinedEventSubscription.Handle(EventInfo.Object, 
                new ApplicationDeclined { ApplicationNumber = "LUSA-000021" });

            //Then
            ApplicationRepository.Verify(x => x.GetByApplicationNumber(It.IsAny<string>()), Times.Once);
            Logger.Verify(x => x.Warn(It.IsAny<string>()), Times.Once);

        }

        [Fact]
        public void ApplicationDeclinedTriggeredAndSendEmailNotification()
        {
            // Given
            SetupApplicant();
            SetupApplication();
            SetupFactories();

            var applicationDeclined = new ApplicationDeclined { ApplicationNumber = "LUSA-000021" };

            ApplicationRepository.Setup(x => x.GetByApplicationNumber(It.IsAny<string>()))
                .Returns(Application.Object);

            ApplicantService.Setup(x => x.Get(It.IsAny<string>()))
                .Returns(Applicant.Object);

            ConfigurationService.Setup(x => x.Get()).Returns(new Docitt.Application.Configuration()
                {
                    Notification = new NotificationMapping()
                    {
                        Template = "AAN",
                        Version = "v1"
                    }
                }
            );

            EmailService.Setup(x => x.Send(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<object>()))
                .Returns(Task.FromResult(true));

            Logger.Setup(x => x.Info(It.IsAny<string>()));


            SetupApplicationDeclinedEventSubscription();

            //When
            ApplicationDeclinedEventSubscription.Handle(EventInfo.Object, applicationDeclined);

            //Then
            ApplicationRepository.Verify(x => x.GetByApplicationNumber(It.IsAny<string>()), Times.Once);
            EmailService.Verify(x => x.Send(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<object>()), Times.Once);
            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Once);

        }

    }
}
