﻿using System;
using System.Collections.Generic;
using LendFoundry.Application.Mocks;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Moq;
using Xunit;
using LendFoundry.StatusManagement;
using LendFoundry.StatusManagement.Client;
using Docitt.Application;

namespace LendFoundry.Application.Tests
{
    public class ApplicationListenerTests
    {
        protected Mock<IConfigurationServiceFactory<Docitt.Application.Configuration>> ConfigurationFactory { get; } =
            new Mock<IConfigurationServiceFactory<Docitt.Application.Configuration>>();

        protected Mock<IConfigurationService<Docitt.Application.Configuration>> ConfigurationService { get; } =
            new Mock<IConfigurationService<Docitt.Application.Configuration>>();

        protected Mock<ITenantTime> TenantTime { get; } = new Mock<ITenantTime>();
        protected Mock<ITenantTimeFactory> TenantTimeFactory { get; } = new Mock<ITenantTimeFactory>();
        protected Mock<LendFoundry.Security.Tokens.ITokenHandler> TokenHandler { get; } = new Mock<LendFoundry.Security.Tokens.ITokenHandler>();
        protected Mock<ILogger> Logger { get; } = new Mock<ILogger>();
        protected Mock<ILoggerFactory> LoggerFactory { get; } = new Mock<ILoggerFactory>();

        protected Mock<IApplicationRepositoryFactory> ApplicationFactory { get; } =
            new Mock<IApplicationRepositoryFactory>();

        protected Mock<IStatusManagementServiceFactory> StatusManagementFactory { get; } = new Mock<IStatusManagementServiceFactory>();

        protected Mock<ITenantServiceFactory> TenantServiceFactory { get; } = new Mock<ITenantServiceFactory>();
        protected Mock<IEventHubClientFactory> EventHubFactory { get; } = new Mock<IEventHubClientFactory>();
        private Mock<IEventHubClient> EventHubClient { get; } = new Mock<IEventHubClient>();
        private Mock<ITenantService> TenantService { get; } = new Mock<ITenantService>();

        private Mock<IEntityStatusService> StatusManagementService { get; } = new Mock<IEntityStatusService>();


        //[Fact]
        //public void ApplicationListnerTest()
        //{
        //    var fakeEventHub = new FakeEventHub();

        //    try
        //    {
        //        SetupLogger();

        //        SetupConfiguration(GetConfiguration());

        //        SetupTenantService(GetDummyTenants());


        //        EventHubFactory.Setup(x => x.Create(It.IsAny<LendFoundry.Security.Tokens.ITokenReader>()))
        //            .Returns(() => fakeEventHub);
        //        var DummyApplicationNumber = "123";
        //        var DummyApplicantId = "A-123";
        //        StatusManagementService.Setup(s => s.GetStatusByEntity(It.IsAny<string>(), It.IsAny<string>()))
        //            .Returns(new StatusResponse
        //            {
        //                Code = "InProgress",
        //                Label = "InProgress",
        //                Name = "InProgress"
        //            });

        //        StatusManagementFactory.Setup(a => a.Create(It.IsAny<LendFoundry.Security.Tokens.ITokenReader>()))
        //            .Returns(StatusManagementService.Object);

        //        var applications = new List<IApplication>
        //        {
        //            new Docitt.Application.Application
        //            {
        //                Id = "123456",
        //                ApplicationNumber = DummyApplicationNumber,
        //                Applicants = new List<IApplicantData>
        //                {
        //                    new ApplicantData
        //                    {
        //                        ApplicantId = DummyApplicantId,
        //                        CurrentEmployer = new Employer
        //                        {
        //                            Name = "Gateway",
        //                            Address = new Address
        //                            {
        //                                City = "Ahmedabad",
        //                                Country = "India"
        //                            }
        //                        },
        //                        PhoneNumbers = new List<IPhoneNumber>
        //                        {
        //                            new PhoneNumber {Number = "9825820589"}
        //                        }
        //                    }
        //                },
        //            }
        //        };

        //        var fakeApplicationRepository = new FakeApplicationRepository(new UtcTenantTime(), applications);

        //        ApplicationFactory.Setup(x => x.Create(It.IsAny<LendFoundry.Security.Tokens.ITokenReader>())).Returns(fakeApplicationRepository);

        //        var applicationListener = new ApplicationListener(ConfigurationFactory.Object,
        //            TokenHandler.Object, EventHubFactory.Object,
        //            ApplicationFactory.Object,
        //            LoggerFactory.Object, TenantServiceFactory.Object, StatusManagementFactory.Object);

        //        applicationListener.Start();

        //        fakeEventHub.Publish("ApplicationStatusChanged", new { EntityId = DummyApplicationNumber, NewStatus = "InProgress", NewStatusName = "InProgress" });

        //        var application = fakeApplicationRepository.GetByApplicationNumber(DummyApplicationNumber);
        //        Assert.Equal("InProgress", application.Status.Name);
        //    }
        //    finally
        //    {
        //        fakeEventHub.Close();
        //    }
        //}

        [Fact]
        public void ApplicationListnerRegisterTwoListnerForTwoTenants()
        {
            //SetupLogger();

            //SetupConfiguration(GetConfiguration());

            //SetupTenantService(GetDummyTenants());

            //EventHubFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
            //   .Returns(EventHubClient.Object);

            //var applicationListener = new ApplicationListener(ConfigurationFactory.Object,
            //    TokenHandler.Object, EventHubFactory.Object,
            //    ApplicationFactory.Object,
            //    LoggerFactory.Object, TenantServiceFactory.Object, StatusManagementFactory.Object);

            //applicationListener.Start();

            //EventHubClient.Verify(x => x.On(It.IsAny<string>(),It.IsAny<Action<EventInfo>>()), Times.Exactly(2));
        }

        [Fact]
        public void ApplicationListnerShouldThrowExceptionOnNullConfiguration()
        {

            //SetupLogger();
            //SetupConfiguration(null);
            //SetupTenantService(new List<TenantInfo>()
            //{
            //    new TenantInfo
            //    {
            //        Id = "Tenant1",
            //        Name = "Tenant1"
            //    },
            //    new TenantInfo()
            //    {
            //        Id = "Tenant2",
            //        Name = "Tenant2"
            //    },
            //});

            //EventHubFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
            //   .Returns(EventHubClient.Object);

            //var applicationListener = new ApplicationListener(ConfigurationFactory.Object,
            //    TokenHandler.Object, EventHubFactory.Object,
            //    ApplicationFactory.Object,
            //    LoggerFactory.Object, TenantServiceFactory.Object, StatusManagementFactory.Object);

            //Assert.Throws<ArgumentException>(()=>applicationListener.Start());
        }

        private void SetupLogger()
        {
            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Info(It.IsAny<string>(), It.IsAny<object>()));
            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
            LoggerFactory.Setup(x => x.Create(It.IsAny<ILogContext>()))
                            .Returns(Logger.Object);
        }

        private void SetupConfiguration(Docitt.Application.Configuration configuration)
        {
            ConfigurationService.Setup(x => x.Get())
               .Returns(configuration);

            ConfigurationFactory.Setup(
                    x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(ConfigurationService.Object);
        }

        private void SetupTenantService(List<TenantInfo> tenants)
        {
            TokenHandler.Setup(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Mock.Of<IToken>());

            TenantTime.Setup(x => x.Today).Returns(new DateTimeOffset(DateTime.Now));
            TenantTimeFactory.Setup(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()))
                .Returns(TenantTime.Object);

            TenantService.Setup(x => x.GetActiveTenants()).Returns(tenants);
            TenantServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>())).Returns(TenantService.Object);
        }

        private static List<TenantInfo> GetDummyTenants()
        {
            return new List<TenantInfo>()
            {
                new TenantInfo()
                {
                    Id = "Tenant1",
                    Name = "Tenant1"
                },
                new TenantInfo()
                {
                    Id = "Tenant2",
                    Name = "Tenant2"
                },
            };
        }

        private static Docitt.Application.Configuration GetConfiguration()
        {
            return new Docitt.Application.Configuration
            {
                Events =
                    new[]
                    {
                        new EventMapping {Name = "ApplicationStatusChanged", ApplicationNumber = "{Data.EntityId}"}
                    }
            };
        }
        
    }
}
