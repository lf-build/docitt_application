﻿using System.Collections.Generic;
using LendFoundry.Application.Mocks;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Microsoft.AspNet.Mvc;
using Moq;
using Xunit;
using System;
using LendFoundry.StatusManagement.Client;
using Docitt.Application;
using Docitt.Applicant;
using Docitt.Application.Client;
using LendFoundry.Foundation.Client;

namespace LendFoundry.Application.Api.Tests
{
    public class ApplicationControllerTest
    {
        private IApplicationService applicationService;

        public ApplicationControllerTest(IApplicationService applicationService)
        {
            this.applicationService = applicationService;
        }

        private static ApplicationControllerTest GetController(List<IApplication> applications = null, IEnumerable<IApplicant> applicants = null)
        {
            var applicantService = new FakeApplicantService(new UtcTenantTime(), applicants ?? new List<IApplicant>());

            return new ApplicationControllerTest(GetApplicationService(applicantService, applications));
        }

        private static ApplicationControllerTest GetController(IApplicantService applicantService)
        {
            return new ApplicationControllerTest(GetApplicationService(applicantService));
        }

        private static ApplicationControllerTest GetController(IApplicationService applicationService, IApplicantService applicantService)
        {
            return new ApplicationControllerTest(applicationService);
        }

        private static  IApplicantService GetApplicantService(IEnumerable<IApplicant> applicants = null)
        {
            return new FakeApplicantService(new UtcTenantTime(), applicants ?? new List<IApplicant>());
        }

        private static IApplicationService GetApplicationService(IApplicantService applicantService, List<IApplication> applications = null)
        {
            return new ApplicationService(Mock.Of<IServiceClient>());
        }

        private static Mock<IConfiguration> Configuration { get; } = new Mock<IConfiguration>();

        [Fact(Skip = "Fix after demo code preparation")]
        public void AddApplicationReturnApplicationIdOnValidApplicant()
        {
            var applicantService = GetApplicantService();
            //var response = GetController(applicantService).Add(new Docitt.Application.ApplicantRequest
            //{
            //    Applicants = new List<Docitt.Application.ApplicantionRequest>
            //    {
            //        new Docitt.Application.ApplicationRequest
            //        {
            //            FirstName = "Nayan",
            //            LastName = "Paregi",
            //            Email = "nayan.gp@sigmainfo.net",
            //            //Password = "nayan",
            //            Identities = new List<IIdentity>
            //            {
            //                new Identity {Name = "LId", Number = "12345"}
            //            },
            //            Ssn = "123456",
            //            Addresses = new List<IAddress>
            //            {
            //                new Address { Line1 = "qwe", City ="qwe", State = "qwe", ZipCode ="123", Country = "qwe", Type = AddressType.Home }
            //            },
            //            PhoneNumbers = new List<IPhoneNumber>
            //            {
            //                new PhoneNumber { Number = "123", Type = "qwe" }
            //            },
            //            StatedGrossAnnualIncome = 1000
            //        },
            //        new Docitt.Application.ApplicantRequest
            //        {
            //            FirstName = "DummyFName",
            //            LastName = "DummyLName",
            //            Email = "dummy@sigmainfo.net",
            //            //Password = "dummy",
            //            Identities = new List<IIdentity>
            //            {
            //                new Identity {Name = "LId", Number = "12345"}
            //            },
            //            Ssn = "123456",
            //            Addresses = new List<IAddress>
            //            {
            //                new Address { Line1 = "qwe", City ="qwe", State = "qwe", ZipCode ="123", Country = "qwe", Type = AddressType.Home }
            //            },
            //            PhoneNumbers = new List<IPhoneNumber>
            //            {
            //                new PhoneNumber { Number = "123", Type = "qwe" }
            //            },
            //            StatedGrossAnnualIncome = 1000
            //        }
            //    },
            //    Amount = 500000000,
            //    Purpose = "qwertyuiop",
            //    Scores = new[] { new Score { Date = DateTimeOffset.Now, Name = "name", Value = "value" } },
            //    Source = new Source { Id = "123", Type = SourceType.Broker }
            //});

            //Assert.IsType<HttpOkObjectResult>(response);
            //var application = ((HttpOkObjectResult)response).Value as IApplicationResponse;

            //Assert.NotNull(application);

            //Assert.NotNull(application.ApplicationNumber);
            //Assert.Equal("1", application.ApplicationNumber);

            ////check if applicant is inserted to separate collection/repository
            //var applicant1 = applicantService.Get(application.Applicants[0].ApplicantId);
            //Assert.NotNull(applicant1);
            //Assert.Equal("nayan.gp@sigmainfo.net", applicant1.Email);

            //var applicant2 = applicantService.Get(application.Applicants[1].ApplicantId);
            //Assert.NotNull(applicant2);
            //Assert.Equal("dummy@sigmainfo.net", applicant2.Email);
        }

        [Fact]
        public void AddApplicationReturnErrorOnIncompleteRequest()
        {
            //var response = GetController().Add(new ApplicationRequest());
            //Assert.IsType<ErrorResult>(response);
            //var result = response as ErrorResult;
            //Assert.NotNull(result);
            //Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public void AddApplicationReturnErrorOnNullRequest()
        {
            //var response = GetController().Add(null);
            //Assert.IsType<ErrorResult>(response);
            //var result = response as ErrorResult;
            //Assert.NotNull(result);
            //Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public void UpdateApplicationReturn404WithInvalidApplicationNumber()
        {
            //var response = GetController().UpdateApplication("InvalidApplicationNumber", new ApplicationUpdateRequest()
            //{
            //    Amount = 500000000,
            //});

            //Assert.IsType<ErrorResult>(response);
            //var result = response as ErrorResult;
            //Assert.NotNull(result);
            //Assert.Equal(404, result.StatusCode);
        }


        [Fact]
        public void UpdateApplicationReturn204WithValidApplicationNumber()
        {
            //var dummyApplicants = new List<IApplicant>
            //{
            //    new Docitt.Applicant.Applicant
            //    {
            //        Id = "123",
            //        Email = "nayan.paregi@gmail.com",
            //        FirstName = "Nayan",
            //        LastName = "Paregi",
            //        MiddleName = "G"
            //    },
            //};

            //var dummyApplication = new List<IApplication>
            //{
            //    new Docitt.Application.Application
            //    {
            //        Id = "123456",
            //        ApplicationNumber = "4567",
            //        Applicants = new List<IApplicantData>
            //        {
            //            new ApplicantData
            //            {
            //                ApplicantId = "123",
            //                CurrentEmployer = new Employer
            //                {
            //                    Name = "Gateway",
            //                    Address = new Address
            //                    {
            //                        City = "Ahmedabad",
            //                        Country = "India"
            //                    }
            //                },
            //                PhoneNumbers = new List<IPhoneNumber>
            //                {
            //                    new PhoneNumber {Number = "9825820589"}
            //                }
            //            }
            //        }
            //    }
            //};

            //var applicantService = GetApplicantService(dummyApplicants);
            //var applicationService = GetApplicationService(applicantService, dummyApplication);

            //var response = GetController(applicationService, applicantService)
            //    .UpdateApplication("4567", new ApplicationUpdateRequest()
            //    {
            //        Purpose = "New Purpose",
            //        Amount = 500000000
            //    });

            //Assert.IsType<NoContentResult>(response);

            //var application = applicationService.GetByApplicationNumber("4567");
            //Assert.Equal(application.Purpose, "New Purpose");
            //Assert.Equal(application.Amount, 500000000);

        }

        [Fact]
        public void UpdateApplicantReturn404WithInvalidApplicationNumber()
        {
            //var response = GetController()
            //    .UpdateApplicant("InvalidApplicationNumber", "InvalidApplicant", new ApplicantUpdateRequest());

            //Assert.IsType<ErrorResult>(response);
            //var result = response as ErrorResult;
            //Assert.NotNull(result);
            //Assert.Equal(404, result.StatusCode);
        }

        [Fact]
        public void UpdateApplicantReturn204WithValidApplicationNumber()
        {
            //var dummyApplicants = new List<IApplicant>
            //{
            //    new Docitt.Applicant.Applicant
            //    {
            //        Id = "123",
            //        Email = "nayan.paregi@gmail.com",
            //        FirstName = "Nayan",
            //        LastName = "Paregi",
            //        MiddleName = "G"
            //    },
            //};

            //var dummyApplication = new List<IApplication>
            //{
            //    new Docitt.Application.Application
            //    {
            //        Id = "123456",
            //        ApplicationNumber = "4567",
            //        Applicants = new List<IApplicantData>
            //        {
            //            new ApplicantData
            //            {
            //                ApplicantId = "123",
            //                CurrentEmployer = new Employer
            //                {
            //                    Name = "Gateway",
            //                    Address = new Address
            //                    {
            //                        City = "Ahmedabad",
            //                        Country = "India"
            //                    }
            //                },
            //                PhoneNumbers = new List<IPhoneNumber>
            //                {
            //                    new PhoneNumber {Number = "9825820589"}
            //                }
            //            }
            //        }
            //    }
            //};

            //var applicantService = GetApplicantService(dummyApplicants);
            //var applicationService = GetApplicationService(applicantService, dummyApplication);

            //var response = GetController(applicationService, applicantService)
            //    .UpdateApplicant("4567", "123", new ApplicantUpdateRequest() { StatedGrossAnnualIncome = 2305.2 });

            //Assert.IsType<NoContentResult>(response);

            //var application = applicationService.GetByApplicationNumber("4567");
            //Assert.Equal(application.Applicants[0].StatedGrossAnnualIncome, 2305.2);

        }

        [Fact]
        public void UpdateApplicantAddressReturn404WithInvalidApplicationNumber()
        {
            //var response = GetController()
            //    .UpdateAddresses("InvalidApplicationNumber", "InvalidApplicant",
            //        new List<Address>() { new Address() { City = "Ahmedabad" } });

            //Assert.IsType<ErrorResult>(response);
            //var result = response as ErrorResult;
            //Assert.NotNull(result);
            //Assert.Equal(404, result.StatusCode);
        }

        [Fact]
        public void UpdateApplicantAddressReturn204WithValidApplicationNumber()
        {
            //var dummyApplicants = new List<IApplicant>
            //{
            //    new Docitt.Applicant.Applicant
            //    {
            //        Id = "123",
            //        Email = "nayan.paregi@gmail.com",
            //        FirstName = "Nayan",
            //        LastName = "Paregi",
            //        MiddleName = "G"
            //    },
            //};

            //var dummyApplication = new List<IApplication>
            //{
            //    new Docitt.Application.Application
            //    {
            //        Id = "123456",
            //        ApplicationNumber = "4567",
            //        Applicants = new List<IApplicantData>
            //        {
            //            new ApplicantData
            //            {
            //                ApplicantId = "123",
            //                CurrentEmployer = new Employer
            //                {
            //                    Name = "Gateway",
            //                    Address = new Address
            //                    {
            //                        City = "Ahmedabad",
            //                        Country = "India"
            //                    }
            //                },
            //                PhoneNumbers = new List<IPhoneNumber>
            //                {
            //                    new PhoneNumber {Number = "9825820589"}
            //                }
            //            }
            //        }
            //    }
            //};

            //var applicantService = GetApplicantService(dummyApplicants);
            //var applicationService = GetApplicationService(applicantService, dummyApplication);

            //var response = GetController(applicationService, applicantService)
            //    .UpdateAddresses("4567", "123", new List<Address>() { new Address() { City = "Irvine" } });

            //Assert.IsType<NoContentResult>(response);

            //var application = applicationService.GetByApplicationNumber("4567");
            //Assert.Equal("Irvine", application.Applicants[0].Addresses[0].City);

        }

        //[Fact]
        //public void GetApplicationReturnOk()
        //{
        //    var dummyApplicants = new List<IApplicant>
        //    {
        //        new Docitt.Applicant.Applicant
        //        {
        //            Id = "123",
        //            Email = "nayan.paregi@gmail.com",
        //            FirstName = "Nayan",
        //            LastName = "Paregi",
        //            MiddleName = "G"
        //        },
        //    };
        //    var dummyApplications = new List<IApplication>
        //    {
        //        new Docitt.Application.Application
        //        {
        //            Id = "123",
        //            ApplicationNumber = "123",
        //            Applicants = new List<IApplicantData>
        //            {
        //                new ApplicantData
        //                {
        //                    ApplicantId = "123",
        //                    CurrentEmployer = new Employer
        //                    {
        //                        Name = "Gateway",
        //                        Address = new Address
        //                        {
        //                            City = "Ahmedabad",
        //                            Country = "India"
        //                        }
        //                    },
        //                    PhoneNumbers = new List<IPhoneNumber>
        //                    {
        //                        new PhoneNumber {Number = "9825820589"}
        //                    }
        //                }
        //            }
        //        }
        //    };
        //    var applicationController = GetController(dummyApplications, dummyApplicants);
        //    var response = applicationController.Get("123");
        //    Assert.IsType<HttpOkObjectResult>(response);
        //    var application = ((HttpOkObjectResult)response).Value as IApplicationResponse;

        //    Assert.NotNull(application);

        //    Assert.NotNull(application.ApplicationNumber);
        //    Assert.Equal("123", application.ApplicationNumber);
        //}

        //[Fact]
        //public void GetApplicationReturn404()
        //{
        //    var applicationController = GetController();
        //    var response = applicationController.Get("456");
        //    Assert.IsType<ErrorResult>(response);
        //    var result = response as ErrorResult;
        //    Assert.NotNull(result);
        //    Assert.Equal(result.StatusCode, 404);
        //}

        [Fact]
        public void GetApplicationReturn400()
        {
            //var applicationController = GetController();
            //var response = applicationController.Get(null);
            //Assert.IsType<ErrorResult>(response);
            //var result = response as ErrorResult;
            //Assert.NotNull(result);
            //Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public void GetApplicationNumbersByApplicantNumbers400()
        {
            //var applicationController = GetController();
            //var response = applicationController.GetApplicationNumbersByApplicant(null);
            //Assert.IsType<ErrorResult>(response);
            //var result = response as ErrorResult;
            //Assert.NotNull(result);
            //Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public void GetApplicationNumbersByApplicantNumbers400WithEmptyCollection()
        {
            //var applicationController = GetController();
            //var response = applicationController.GetApplicationNumbersByApplicant("");
            //Assert.IsType<ErrorResult>(response);
            //var result = response as ErrorResult;
            //Assert.NotNull(result);
            //Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public void GetApplicationNumbersByApplicantNumbersReturnApplicationNumber()
        {
            //var dummyApplications = new List<IApplication>
            //{
            //    new Docitt.Application.Application
            //    {
            //        ApplicationNumber = "123",
            //        Applicants = new List<IApplicantData>
            //        {
            //            new ApplicantData
            //            {
            //                ApplicantId = "A-123-1"
            //            },
            //            new ApplicantData
            //            {
            //                ApplicantId = "A-123-1"
            //            }
            //        }
            //    },
            //    new Docitt.Application.Application
            //    {
            //        ApplicationNumber = "456",
            //        Applicants = new List<IApplicantData>
            //        {
            //            new ApplicantData
            //            {
            //                ApplicantId = "A-123-1"
            //            },
            //            new ApplicantData
            //            {
            //                ApplicantId = "A-456-1"
            //            }
            //        }
            //    },
            //    new Docitt.Application.Application
            //    {
            //        ApplicationNumber = "789",
            //        Applicants = new List<IApplicantData>
            //        {
            //            new ApplicantData
            //            {
            //                ApplicantId = "A-789-1"
            //            },
            //            new ApplicantData
            //            {
            //                ApplicantId = "A-789-1"
            //            }
            //        }
            //    }
            //};


            //var applicationController = GetController(dummyApplications);
            //var response = applicationController.GetApplicationNumbersByApplicant("A-123-1");
            //Assert.IsType<HttpOkObjectResult>(response);
            //var applicationNumbers = ((HttpOkObjectResult)response).Value as List<string>;

            //Assert.NotNull(applicationNumbers);
            //Assert.NotEmpty(applicationNumbers);
            //Assert.Contains(applicationNumbers, s => s == "123");
            //Assert.Contains(applicationNumbers, s => s == "456");
            //Assert.DoesNotContain(applicationNumbers, s => s == "789");


        }

        [Fact]
        public void GetApplicationNumbersByApplicantNumbers404WithNotFound()
        {
            //var applicationController = GetController();
            //var response = applicationController.GetApplicationNumbersByApplicant("A-Invalid");
            //Assert.IsType<ErrorResult>(response);
            //var result = response as ErrorResult;
            //Assert.NotNull(result);
            //Assert.Equal(404, result.StatusCode);
        }

        [Fact]
        public void IsDuplicateReturnOk()
        {
            //var dummyApplicants = new List<IApplicant>
            //{
            //    new Docitt.Applicant.Applicant
            //    {
            //        Id = "123",
            //        Email = "nayan.paregi@gmail.com",
            //        FirstName = "Nayan",
            //        LastName = "Paregi",
            //        MiddleName = "G",
            //        Ssn = "123456789",
            //        DateOfBirth = new DateTime(1978, 8, 14)
            //    },
            //    new Docitt.Applicant.Applicant
            //    {
            //        Id = "456",
            //        Email = "nayan.paregi@gmail.com",
            //        FirstName = "Nayan",
            //        LastName = "Paregi",
            //        MiddleName = "G",
            //        Ssn = "123456789",
            //        DateOfBirth = new DateTime(1978, 8, 14)
            //    }
            //};

            //var dummyApplications = new List<IApplication>
            //{
            //    new Docitt.Application.Application
            //    {
            //        Id = "123",
            //        ApplicationNumber = "123",
            //        Status = new Status { Code = "100.01", Name = "New" },
            //        Applicants = new List<IApplicantData>
            //        {
            //            new ApplicantData
            //            {
            //                ApplicantId = "123",
            //                CurrentEmployer = new Employer
            //                {
            //                    Name = "Gateway",
            //                    Address = new Address
            //                    {
            //                        City = "Ahmedabad",
            //                        Country = "India"
            //                    }
            //                },
            //                PhoneNumbers = new List<IPhoneNumber>
            //                {
            //                    new PhoneNumber {Number = "9825820589"}
            //                },
            //                IsPrimary = true
            //            }
            //        }
            //    }
            //};

            //Configuration.Setup(x => x.DuplicateStatuses).Returns(new[] { "100.01", "100.02", "100.03" });

            //var applicationController = GetController(dummyApplications, dummyApplicants);

            //var response = applicationController.IsDuplicate("123");
            //Assert.IsType<HttpOkObjectResult>(response);
            //Assert.Equal(((HttpOkObjectResult)response).StatusCode.Value, 200);
            //Assert.True(Convert.ToBoolean(((HttpOkObjectResult)response).Value));
        }
    }
}