﻿using System.Collections.Generic;
using LendFoundry.Foundation.Services;
using Moq;
using RestSharp;
using Xunit;
using LendFoundry.Foundation.Client;
using Docitt.Application;
using Docitt.Application.Client;

namespace LendFoundry.Application.Client.Tests
{
    public class ApplicationServiceClientTests
    {
        private ApplicationService ApplicationServiceClient { get; }
        private IRestRequest Request { get; set; }
        private Mock<IServiceClient> MockServiceClient { get; }

        public ApplicationServiceClientTests()
        {
            MockServiceClient = new Mock<IServiceClient>();
            ApplicationServiceClient = new ApplicationService(MockServiceClient.Object);
        }

        [Fact]
        public void Client_Get_Application()
        {
            MockServiceClient.Setup(s => s.Execute<ApplicationResponse>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(
                    () =>

                        new ApplicationResponse()
                        {
                            ApplicationNumber = "123"
                        }
                );

            var result = ApplicationServiceClient.GetByApplicationNumber("123");
            Assert.Equal("/{applicationNumber}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public void Client_Add_Application()
        {
            var application = new ApplicationRequest();

            MockServiceClient.Setup(s => s.Execute<ApplicationResponse>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(
                    () =>
                        new ApplicationResponse());

            var result =  ApplicationServiceClient.Add(application);
            Assert.Equal("/", Request.Resource);
            Assert.Equal(Method.PUT, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public void Client_UpdateApplication()
        {
            var application = new ApplicationUpdateRequest();


            MockServiceClient.Setup(s => s.Execute(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(true);

            ApplicationServiceClient.UpdateApplication("123", application);
            Assert.Equal("/{applicationNumber}", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
        }
        [Fact]
        public void Client_UpdateAddresses()
        {
            MockServiceClient.Setup(s => s.Execute(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(true);

            ApplicationServiceClient.UpdateAddresses("123","123", new List<IAddress>());
            Assert.Equal("/{applicationNumber}/{applicantId}/address", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
        }

        [Fact]
        public void Client_UpdatePhoneNumbers()
        {
            MockServiceClient.Setup(s => s.Execute(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(true);

            ApplicationServiceClient.UpdatePhoneNumbers("123", "123", new List<IPhoneNumber>());
            Assert.Equal("/{applicationNumber}/{applicantId}/phonenumber", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
        }

        [Fact]
        public void Client_UpdateApplicant()
        {
            MockServiceClient.Setup(s => s.Execute(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(true);

            ApplicationServiceClient.UpdateApplicant("123", "123", new ApplicantUpdateRequest());
            Assert.Equal("/{applicationNumber}/{applicantId}", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
        }

        [Fact]
        public void Client_GetApplicationNumbersByApplicantNumbers()
        {
            MockServiceClient.Setup(s => s.Execute<List<string>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(
                    () =>
                        new List<string>()
                        {
                            "123"
                        }
                );

            var result = ApplicationServiceClient.GetApplicationNumbersByApplicant("123");
            Assert.Equal("/applicant/{applicantId}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public void Client_IsDuplicate()
        {
            MockServiceClient.Setup(s => s.Execute<bool>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(true);

            var result = ApplicationServiceClient.IsDuplicate("123");
            Assert.Equal("/is-duplicate/{applicationNumber}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }
    }
}