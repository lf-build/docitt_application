﻿using Docitt.Applicant;
using System;
using System.Collections.Generic;
using Docitt.Applicant;
using Docitt.Application;

namespace LendFoundry.Application.Mocks
{
    public class InMemoryObjects
    {
        public List<IApplicant> DummyApplicants = new List<IApplicant>
        {
            new Applicant
            {
                Id = "123",
                Email = "nayan.paregi@gmail.com",
                FirstName = "Nayan",
                LastName = "Paregi",
                MiddleName = "G",
                Ssn = "123456789",
                DateOfBirth = new DateTime(1978, 8, 14)
            },
            new Applicant
            {
                Id = "456",
                Email = "nayan.paregi@gmail.com",
                FirstName = "Nayan",
                LastName = "Paregi",
                MiddleName = "G",
                Ssn = "123456789",
                DateOfBirth = new DateTime(1978, 8, 14)
            }
        };

        public List<IApplication> DummyApplications = new List<IApplication>
        {
            new Docitt.Application.Application
            {
                Id = "123",
                ApplicationNumber = "123",
                Status = new Status { Code = "100.01", Name = "New" },
                Applicants = new List<IApplicantData>
                {
                    new ApplicantData
                    {
                        ApplicantId = "123",
                        CurrentEmployer = new Employer
                        {
                            Name = "Gateway",
                            Address = new Address
                            {
                                City = "Ahmedabad",
                                Country = "India"
                            }
                        },
                        PhoneNumbers = new List<IPhoneNumber>
                        {
                            new PhoneNumber {Number = "9825820589"}
                        },
                        IsPrimary = true
                    }
                }
            }
        };
    }
}