﻿using System;
using System.Collections.Generic;
using System.Linq;
using Docitt.Applicant;
using LendFoundry.Application.Mocks;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Moq;
using Xunit;
using LendFoundry.NumberGenerator;
using Docitt.Application;
using Docitt.Application.Events;
using Docitt.Application.Client;
using LendFoundry.Foundation.Client;

namespace LendFoundry.Application.Tests
{
    public class ApplicationServiceTests : InMemoryObjects
    {
        private static FakeApplicantService GetApplicantService(IEnumerable<IApplicant> applicants = null)
        {
            return new FakeApplicantService(new UtcTenantTime(), applicants ?? new List<IApplicant>());
        }

        private static FakeApplicationRepository GetFakeApplicationRepository(List<IApplication> applications = null)
        {
            return new FakeApplicationRepository(new UtcTenantTime(), applications ?? new List<IApplication>());
        }

        private static Mock<Docitt.Application.IConfiguration> Configuration { get; set; } = new Mock<Docitt.Application.IConfiguration>();

        private static IApplicationService GetApplicationService(IApplicantService applicantService)
        {
            return new ApplicationService(Mock.Of<IServiceClient>());
        }

        private static IApplicationService GetApplicationService(IApplicantService applicantService, IApplicationRepository fakeApplicationRepository)
        {
            return new ApplicationService(Mock.Of<IServiceClient>());
        }

        private static IApplicationService GetApplicationService(List<IApplication> applications = null, IEnumerable<IApplicant> applicants = null)
        {
            return new ApplicationService(Mock.Of<IServiceClient>());
        }

        private static IApplicationService GetApplicationService(IApplicantService applicantService, List<IApplication> applications)
        {
            return new ApplicationService(Mock.Of<IServiceClient>());
        }

        [Fact]
        public void Get_WithEmptyOrNull_ThrowsArgumentException()
        {
            var service = GetApplicationService();
            Assert.Throws<ArgumentException>(() => service.GetByApplicationNumber(string.Empty));
            Assert.Throws<ArgumentException>(() => service.GetByApplicationNumber(" "));
            Assert.Throws<ArgumentException>(() => service.GetByApplicationNumber(null));
        }

        [Fact]
        public void Get_NonExistingRiskCode_ThrowsNotFoundException()
        {
            var service = GetApplicationService();
            Assert.Throws<NotFoundException>(() => service.GetByApplicationNumber("nonexisting"));
        }

        [Fact]
        public void Get_ExistingRecord_ReturnsRecord()
        {
            var service = GetApplicationService(new List<IApplication>
            {
                new Docitt.Application.Application
                {
                    ApplicationNumber = "123"
                }
            });
            var result = service.GetByApplicationNumber("123");
            Assert.NotNull(result);
            Assert.Equal("123", result.ApplicationNumber);
        }

        [Fact]
        public void Add_RollbackTestWhenAddApplicantFail()
        {
            var fakeApplicantService = GetApplicantService(new List<IApplicant>
            {
                new Docitt.Applicant.Applicant
                {
                    Id = "1",
                    Email = "nayan.paregi@gmail.com"
                }
            });

            var service = GetApplicationService(fakeApplicantService);

            Assert.Throws<ApplicantWithSameEmailAlreadyExist>(() => service.Add(new ApplicationRequest
            {
                Applicants = new List<Docitt.Application.IApplicantRequest>
                {
                    new Docitt.Application.ApplicantRequest
                    {
                        FirstName = "Nayan",
                        LastName = "Paregi",
                        Email = "user1@gmail.com",
                        //Password = "nayan",
                        Identities = new List<IIdentity>
                        {
                            new Identity {Name = "LId", Number = "12345"}
                        },
                        Ssn = "123456"
                    },
                    new Docitt.Application.ApplicantRequest
                    {
                        FirstName = "DummyFName",
                        LastName = "DummyLName",
                        Email = "nayan.paregi@gmail.com",
                        //Password = "dummy",
                        Identities = new List<IIdentity>
                        {
                            new Identity {Name = "LId", Number = "12345"}
                        },
                        Ssn = "123456"
                    }
                },
                Amount = 500000000,
                Purpose = "qwertyuiop",
                Scores = new[] { new Score { Date = DateTimeOffset.Now, Name = "name", Value = "value" } },
                Source = new Source { Id = "123", Type = SourceType.Broker }
            }));

            //Make sure applicant's rollbacked
            Assert.Equal(1, fakeApplicantService.Applicants.Count);
            //Just check if add is called for first applicant and then rollbacked after failure in second applicant
            Assert.Equal(3, fakeApplicantService.CurrentId);
        }

        [Fact]
        public void Add_RollbackTestWhenAddApplicationFail()
        {
            var fakeApplicantService = GetApplicantService(new List<IApplicant>
            {
                new Docitt.Applicant.Applicant
                {
                    Id="1",
                    Email = "user1@gmail.com"
                }
            });

            var service = GetApplicationService(fakeApplicantService);

            Assert.Throws<ArgumentException>(() => service.Add(new ApplicationRequest
            {
                Applicants = new List<Docitt.Application.IApplicantRequest>
                {
                    new Docitt.Application.ApplicantRequest
                    {
                        FirstName = "Nayan",
                        LastName = "Paregi",
                        Email = "user2@gmail.com",
                        //Password = "nayan",
                        Identities = new List<IIdentity>
                        {
                            new Identity {Name = "LId", Number = "12345"}
                        },
                        Ssn = "123456",
                        Addresses = new List<IAddress>
                        {
                            new Address { Line1 = "qwe", City ="qwe", State = "qwe", ZipCode ="123", Country = "qwe", Type = AddressType.Home }
                        },
                        PhoneNumbers = new List<IPhoneNumber>
                        {
                            new PhoneNumber { Number = "123", Type = "qwe" }
                        },
                        StatedGrossAnnualIncome = 1000,
                        HomeOwnership = HomeOwnershipType.Mortgage
                    },
                    new Docitt.Application.ApplicantRequest
                    {
                        FirstName = "DummyFName",
                        LastName = "DummyLName",
                        Email = "user3@gmail.com",
                        //Password = "dummy",
                        Identities = new List<IIdentity>
                        {
                            new Identity {Name = "LId", Number = "12345"}
                        },
                        Ssn = "123456",
                        Addresses = new List<IAddress>
                        {
                            new Address { Line1 = "qwe", City ="qwe", State = "qwe", ZipCode ="123", Country = "qwe", Type = AddressType.Home }
                        },
                        PhoneNumbers = new List<IPhoneNumber>
                        {
                            new PhoneNumber { Number = "123", Type = "qwe" }
                        },
                        StatedGrossAnnualIncome = 1000,
                        HomeOwnership = HomeOwnershipType.Mortgage
                    }
                },
                Amount = 2500,
                Purpose = "Should fail by Fake Repo",
                Scores = new[] { new Score { Date = DateTimeOffset.Now, Name = "name", Value = "value" } },
                Source = new Source { Id = "123", Type = SourceType.Broker }
            }));

            //Make sure applicant's rollbacked
            Assert.Equal(1, fakeApplicantService.Applicants.Count);
            //Just check if add is called for first applicant and then rollbacked after failure in second applicant
            Assert.Equal(4, fakeApplicantService.CurrentId);
        }

        [Fact]
        public void Add_CommitTest()
        {
            var fakeApplicantService = GetApplicantService(new List<IApplicant>
            {
                new Docitt.Applicant.Applicant
                {
                    Id="1",
                    Email = "user1@gmail.com"
                }
            });
            var fakeApplicationRepository = GetFakeApplicationRepository();
            var service = GetApplicationService(fakeApplicantService, fakeApplicationRepository);

            service.Add(new ApplicationRequest
            {
                Applicants = new List<Docitt.Application.IApplicantRequest>
                {
                    new Docitt.Application.ApplicantRequest
                    {
                        FirstName = "Nayan",
                        LastName = "Paregi",
                        Email = "nayan.gp@sigmainfo.net",
                        //Password = "nayan",
                        Identities = new List<IIdentity>
                        {
                            new Identity {Name = "LId", Number = "12345"}
                        },
                        Ssn = "123456",
                        Addresses = new List<IAddress>
                        {
                            new Address { Line1 = "qwe", City ="qwe", State = "qwe", ZipCode ="123", Country = "qwe", Type = AddressType.Home }
                        },
                        PhoneNumbers = new List<IPhoneNumber>
                        {
                            new PhoneNumber { Number = "123", Type = "qwe" }
                        },
                        StatedGrossAnnualIncome = 1000,
                        HomeOwnership = HomeOwnershipType.Mortgage
                    },
                    new Docitt.Application.ApplicantRequest
                    {
                        FirstName = "DummyFName",
                        LastName = "DummyLName",
                        Email = "dummy@sigmainfo.net",
                        //Password = "dummy",
                        Identities = new List<IIdentity>
                        {
                            new Identity {Name = "LId", Number = "12345"}
                        },
                        Ssn = "123456",
                        Addresses = new List<IAddress>
                        {
                            new Address { Line1 = "qwe", City ="qwe", State = "qwe", ZipCode ="123", Country = "qwe", Type = AddressType.Home }
                        },
                        PhoneNumbers = new List<IPhoneNumber>
                        {
                            new PhoneNumber { Number = "123", Type = "qwe" }
                        },
                        StatedGrossAnnualIncome = 1000,
                        HomeOwnership = HomeOwnershipType.Mortgage
                    }
                },
                Amount = 500000000,
                Purpose = "qwertyuiop",
                Scores = new[] { new Score { Date = DateTimeOffset.Now, Name = "name", Value = "value" } },
                Source = new Source { Id = "123", Type = SourceType.Broker }
            });

            //Make sure applicant's rollbacked
            Assert.Equal(3, fakeApplicantService.Applicants.Count);
            //Just check if add is called for first applicant and then rollbacked after failure in second applicant
            Assert.Equal(4, fakeApplicantService.CurrentId);
            Assert.Equal(1, fakeApplicationRepository.Applications.Count);

        }

        [Fact]
        public void Add()
        {
            var fakeApplicantService = GetApplicantService();
            var fakeApplicationRepository = GetFakeApplicationRepository();
            var service = GetApplicationService(fakeApplicantService, fakeApplicationRepository);

            var addedApplication = service.Add(new ApplicationRequest
            {
                Applicants = new List<Docitt.Application.IApplicantRequest>
                {
                    new Docitt.Application.ApplicantRequest
                    {
                        FirstName = "Nayan",
                        LastName = "Paregi",
                        Email = "nayan.gp@sigmainfo.net",
                        //Password = "nayan",
                        Identities = new List<IIdentity>
                        {
                            new Identity {Name = "LId", Number = "12345"}
                        },
                        Ssn = "123456",
                        Addresses = new List<IAddress>
                        {
                            new Address { Line1 = "qwe", City ="qwe", State = "qwe", ZipCode ="123", Country = "qwe", Type = AddressType.Home }
                        },
                        PhoneNumbers = new List<IPhoneNumber>
                        {
                            new PhoneNumber { Number = "123", Type = "qwe" }
                        },
                        StatedGrossAnnualIncome = 1000,
                        HomeOwnership = HomeOwnershipType.Mortgage
                    },
                    new Docitt.Application.ApplicantRequest
                    {
                        FirstName = "DummyFName",
                        LastName = "DummyLName",
                        Email = "dummy@sigmainfo.net",
                        //Password = "dummy",
                        Identities = new List<IIdentity>
                        {
                            new Identity {Name = "LId", Number = "12345"}
                        },
                        Ssn = "123456",
                        Addresses = new List<IAddress>
                        {
                            new Address { Line1 = "qwe", City ="qwe", State = "qwe", ZipCode ="123", Country = "qwe", Type = AddressType.Home }
                        },
                        PhoneNumbers = new List<IPhoneNumber>
                        {
                            new PhoneNumber { Number = "123", Type = "qwe" }
                        },
                        StatedGrossAnnualIncome = 1000,
                        HomeOwnership = HomeOwnershipType.Mortgage
                    }
                },
                Amount = 500000000,
                Purpose = "qwertyuiop",
                Scores = new[] { new Score { Date = DateTimeOffset.Now, Name = "name", Value = "value" } },
                Source = new Source { Id = "123", Type = SourceType.Broker }
            });

            Assert.Equal("1", addedApplication.ApplicationNumber);
            Assert.Equal(2, fakeApplicantService.Applicants.Count);
            Assert.Equal("1", addedApplication.ApplicationNumber);
        }

        [Fact]
        public void GetApplicationNumbersByApplicantIds_ReturnsRecord()
        {
            var service = GetApplicationService(new List<IApplication>
            {
                new Docitt.Application.Application
                {
                    ApplicationNumber = "123",
                    Applicants = new List<IApplicantData>
                    {
                        new ApplicantData
                        {
                            ApplicantId = "A-123"
                        }
                    }
                }
            });
            var result = service.GetApplicationNumbersByApplicant("A-123");
            Assert.NotNull(result);
            Assert.Equal(1, result.Count);
            Assert.Equal("123", result[0]);

        }

        [Fact]
        public void GetApplicationNumbersByApplicantIds_ThrowsNotFoundException()
        {
            var service = GetApplicationService(new List<IApplication>
            {
                new Docitt.Application.Application
                {
                    ApplicationNumber = "123",
                    Applicants = new List<IApplicantData>
                    {
                        new ApplicantData
                        {
                            ApplicantId = "A-123"
                        }
                    }
                }
            });

            Assert.Throws<NotFoundException>(() =>
            {
                service.GetApplicationNumbersByApplicant("A-INVALID");
            });


        }

        [Fact]
        public void UpdateApplication_ThrowsArgumentException()
        {
            var service = GetApplicationService();
            Assert.Throws<ArgumentException>(() => service.UpdateApplication(null, null));
            Assert.Throws<ArgumentNullException>(() => service.UpdateApplication("123", null));
        }

        [Fact]
        public void UpdateApplication_ThrowsNotFoundException()
        {
            var service = GetApplicationService();
            Assert.Throws<NotFoundException>(() => service.UpdateApplication("123", new ApplicationUpdateRequest()));
        }

        [Fact]
        public void UpdateAddresses_ThrowsArgumentException()
        {
            var service = GetApplicationService();
            Assert.Throws<ArgumentException>(() => service.UpdateAddresses(null, null, null));
            Assert.Throws<ArgumentException>(() => service.UpdateAddresses("123", null, null));
        }

        [Fact]
        public void UpdateAddresses_ThrowsNotFoundException()
        {
            var service = GetApplicationService();
            Assert.Throws<NotFoundException>(() => service.UpdateAddresses("123", "123", null));
        }

        [Fact]
        public void UpdateAddresses_ApplicantNotExist_ThrowsNotFoundException()
        {
            var service = GetApplicationService(new List<IApplication>
            {
                new Docitt.Application.Application
                {
                    ApplicationNumber = "123",
                    Applicants = new List<IApplicantData>
                    {
                        new ApplicantData
                        {
                            ApplicantId = "A-123"
                        }
                    }
                }
            });
            Assert.Throws<NotFoundException>(() => service.UpdateAddresses("123", "A-456", null));
        }

        [Fact]
        public void UpdatePrimaryAddress_ThrowsArgumentException()
        {
            var service = GetApplicationService();
            Assert.Throws<ArgumentException>(() => service.UpdatePrimaryAddress(null, null));
            Assert.Throws<ArgumentNullException>(() => service.UpdatePrimaryAddress("123", null));
            //add argument exceptions for the validation checks
            Assert.Throws<ArgumentException>(() => service.UpdatePrimaryAddress("123", new Address { Line1 = "", City = "Irvine", State = "CA", ZipCode = "92606" }));
            Assert.Throws<ArgumentException>(() => service.UpdatePrimaryAddress("123", new Address { Line1 = "532 Santa Maria Drive", City = "", State = "CA", ZipCode = "92606" }));
            Assert.Throws<ArgumentException>(() => service.UpdatePrimaryAddress("123", new Address { Line1 = "532 Santa Maria Drive", City = "Irvine", State = "", ZipCode = "92606" }));
            Assert.Throws<ArgumentException>(() => service.UpdatePrimaryAddress("123", new Address { Line1 = "532 Santa Maria Drive", City = "Irvine", State = "CA", ZipCode = "" }));
        }

        [Fact]
        public void UpdatePrimaryAddress_ApplicantNotExist_ThrowsNotFoundException()
        {
            var service = GetApplicationService(new List<IApplication>
            {
                new Docitt.Application.Application
                {
                    ApplicationNumber = "123",
                    Applicants = new List<IApplicantData>
                    {
                        new ApplicantData
                        {
                            ApplicantId = "A-123",
                            IsPrimary = false
                        }
                    }
                }
            });
            Assert.Throws<NotFoundException>(() => service.UpdatePrimaryAddress("123", new Address { City = "Irvine", Country = "USA", ZipCode = "92606", Line1 = "532 Santa Maria Drive", State = "CA", Type = AddressType.Home }));
        }

        [Fact]
        public void UpdatePrimaryAddress_ApplicationNotExist_ThrowsNotFoundException()
        {
            var service = GetApplicationService(new List<IApplication>
            {
                new Docitt.Application.Application
                {
                    ApplicationNumber = "123",
                    Applicants = new List<IApplicantData>
                    {
                        new ApplicantData
                        {
                            ApplicantId = "A-123",
                            IsPrimary = true
                        }
                    }
                }
            });
            Assert.Throws<NotFoundException>(() => service.UpdatePrimaryAddress("456", new Address { City = "Irvine", Country = "USA", ZipCode = "92606", Line1 = "532 Santa Maria Drive", State = "CA", Type = AddressType.Home }));
        }

        [Fact]
        public void UpdatePhoneNumbers_ThrowsArgumentException()
        {
            var service = GetApplicationService();
            Assert.Throws<ArgumentException>(() => service.UpdatePhoneNumbers(null, null, null));
            Assert.Throws<ArgumentException>(() => service.UpdatePhoneNumbers("123", null, null));
        }

        [Fact]
        public void UpdatePhoneNumbers_ApplicantNotExist_ThrowsNotFoundException()
        {
            var service = GetApplicationService(new List<IApplication>
            {
                new Docitt.Application.Application
                {
                    ApplicationNumber = "123",
                    Applicants = new List<IApplicantData>
                    {
                        new ApplicantData
                        {
                            ApplicantId = "A-123"
                        }
                    }
                }
            });
            Assert.Throws<NotFoundException>(() => service.UpdatePhoneNumbers("123", "A-456", null));
        }

        [Fact]
        public void UpdatePrimaryPhoneNumber_ThrowsArgumentException()
        {
            var service = GetApplicationService();
            Assert.Throws<ArgumentException>(() => service.UpdatePrimaryPhoneNumber(null, null, null));
            Assert.Throws<ArgumentException>(() => service.UpdatePrimaryPhoneNumber("123", null, null));
            Assert.Throws<ArgumentNullException>(() => service.UpdatePrimaryPhoneNumber("123", "123", null));
            Assert.Throws<ArgumentException>(() => service.UpdatePrimaryPhoneNumber("123", "123", new PhoneNumber()));
            Assert.Throws<ArgumentException>(() => service.UpdatePrimaryPhoneNumber("123", "123", new PhoneNumber { Number = "", Type = "Home" }));
            Assert.Throws<ArgumentException>(() => service.UpdatePrimaryPhoneNumber("123", "123", new PhoneNumber { Number = "123", Type = "" }));
        }

        [Fact]
        public void UpdatePrimaryPhoneNumber_ApplicantNotExist_ThrowsNotFoundException()
        {
            var service = GetApplicationService(new List<IApplication>
            {
                new Docitt.Application.Application
                {
                    ApplicationNumber = "123",
                    Applicants = new List<IApplicantData>
                    {
                        new ApplicantData
                        {
                            ApplicantId = "A-123"
                        }
                    }
                }
            });
            Assert.Throws<NotFoundException>(() => service.UpdatePrimaryPhoneNumber("123", "A-456", new PhoneNumber { Number = "123", Type = "123" }));
        }

        [Fact]
        public void UpdatePrimaryPhoneNumber_ApplicationNotExist_ThrowsNotFoundException()
        {
            var service = GetApplicationService(new List<IApplication>
            {
                new Docitt.Application.Application
                {
                    ApplicationNumber = "123",
                    Applicants = new List<IApplicantData>
                    {
                        new ApplicantData
                        {
                            ApplicantId = "A-123"
                        }
                    }
                }
            });
            Assert.Throws<NotFoundException>(() => service.UpdatePrimaryPhoneNumber("456", "A-456", new PhoneNumber { Number = "123", Type = "123" }));
        }

        [Fact]
        public void UpdateApplicant_ThrowsArgumentException()
        {
            var service = GetApplicationService();
            Assert.Throws<ArgumentException>(() => service.UpdateApplicant(null, null, null));
            Assert.Throws<ArgumentException>(() => service.UpdateApplicant("123", null, null));
        }

        [Fact]
        public void UpdateApplicant_ApplicantNotExist_ThrowsNotFoundException()
        {
            var service = GetApplicationService(new List<IApplication>
            {
                new Docitt.Application.Application
                {
                    ApplicationNumber = "123",
                    Applicants = new List<IApplicantData>
                    {
                        new ApplicantData
                        {
                            ApplicantId = "A-123"
                        }
                    }
                }
            });
            Assert.Throws<NotFoundException>(() => service.UpdateApplicant("123", "A-456", null));
        }

        [Fact]
        public void ShouldAddBankAccount()
        {
            var applicantService = GetApplicantService(GetDummyApplicants());
            var service = GetApplicationService(applicantService, GetDummyApplication(new List<IBankAccount>()));

            const string routingNumber = "123";
            const string accountNumber = "456";
            const BankAccountType accountType = BankAccountType.Checking;
            const bool isPrimary = true;
            var bankAccount = service.AddBank(DummyApplicationNumber, new BankAccount
            {
                RoutingNumber = routingNumber,
                AccountNumber = accountNumber,
                AccountType = accountType,
                IsPrimary = isPrimary
            });

            var bankAccountSaved = service.GetBank(DummyApplicationNumber, bankAccount.Id);
            Assert.Equal(routingNumber, bankAccountSaved.RoutingNumber);
            Assert.Equal(accountNumber, bankAccountSaved.AccountNumber);
            Assert.Equal(accountType, bankAccountSaved.AccountType);
            Assert.Equal(isPrimary, bankAccountSaved.IsPrimary);

        }

        [Fact]
        public void ShouldThrowArgumentExceptionOnMissingRequiredField()
        {
            var applicantService = GetApplicantService(GetDummyApplicants());
            var service = GetApplicationService(applicantService, GetDummyApplication(new List<IBankAccount>()));

            const string routingNumber = "123";
            const string accountNumber = "456";
            const BankAccountType accountType = BankAccountType.Checking;
            const bool isPrimary = true;

            Assert.Throws<InvalidArgumentException>(() =>
            {
                service.AddBank(DummyApplicationNumber, new BankAccount
                {
                    RoutingNumber = "",
                    AccountNumber = accountNumber,
                    AccountType = accountType,
                    IsPrimary = isPrimary
                });
            });

            Assert.Throws<InvalidArgumentException>(() =>
            {
                service.AddBank(DummyApplicationNumber, new BankAccount
                {
                    RoutingNumber = null,
                    AccountNumber = accountNumber,
                    AccountType = accountType,
                    IsPrimary = isPrimary
                });
            });

            Assert.Throws<InvalidArgumentException>(() =>
            {
                service.AddBank(DummyApplicationNumber, new BankAccount
                {
                    RoutingNumber = routingNumber,
                    AccountNumber = "",
                    AccountType = accountType,
                    IsPrimary = isPrimary
                });
            });

            Assert.Throws<InvalidArgumentException>(() =>
            {
                service.AddBank(DummyApplicationNumber, new BankAccount
                {
                    RoutingNumber = routingNumber,
                    AccountNumber = null,
                    AccountType = accountType,
                    IsPrimary = isPrimary
                });
            });

            Assert.Throws<InvalidArgumentException>(() =>
            {
                service.AddBank(DummyApplicationNumber, new BankAccount
                {
                    RoutingNumber = routingNumber,
                    AccountNumber = accountNumber,
                    AccountType = BankAccountType.Undefined,
                    IsPrimary = isPrimary
                });
            });

            Assert.Throws<InvalidArgumentException>(() =>
            {
                service.AddBank(DummyApplicationNumber, new BankAccount
                {
                    RoutingNumber = routingNumber,
                    AccountNumber = accountNumber,
                    AccountType = (BankAccountType)900,
                    IsPrimary = isPrimary
                });
            });

            Assert.Throws<ArgumentNullException>(() =>
            {
                service.AddBank(null, new BankAccount
                {
                    RoutingNumber = routingNumber,
                    AccountNumber = accountNumber,
                    AccountType = BankAccountType.Checking,
                    IsPrimary = isPrimary
                });
            });

            Assert.Throws<ArgumentNullException>(() =>
            {
                service.AddBank("", new BankAccount
                {
                    RoutingNumber = routingNumber,
                    AccountNumber = accountNumber,
                    AccountType = BankAccountType.Checking,
                    IsPrimary = isPrimary
                });
            });
            Assert.Throws<ArgumentNullException>(() =>
            {
                service.AddBank(DummyApplicationNumber, null);
            });
        }

        [Fact]
        public void ShouldNotAllowToAddDuplicateBankAccount()
        {
            const string routingNumber = "123";
            const string accountNumber = "456";
            const BankAccountType accountType = BankAccountType.Checking;
            const bool isPrimary = true;
            var applicantService = GetApplicantService(GetDummyApplicants());
            var service = GetApplicationService(applicantService, GetDummyApplication());

            service.AddBank(DummyApplicationNumber, new BankAccount
            {
                RoutingNumber = routingNumber,
                AccountNumber = accountNumber,
                AccountType = accountType,
                IsPrimary = isPrimary
            });

            Assert.Throws<InvalidOperationException>(() => service.AddBank(DummyApplicationNumber, new BankAccount
            {
                RoutingNumber = routingNumber,
                AccountNumber = accountNumber,
                AccountType = accountType,
                IsPrimary = isPrimary
            }));

        }

        [Fact]
        public void ShouldSetBankAccountAsPrimary()
        {
            var applicantService = GetApplicantService(GetDummyApplicants());
            var service = GetApplicationService(applicantService, GetDummyApplication(new List<IBankAccount>
            {
                        new BankAccount { Id="BA-1", IsPrimary = true},
                        new BankAccount { Id="BA-2" },
                        new BankAccount { Id="BA-3" },
                    }));

            service.SetBankAsPrimary(DummyApplicationNumber, "BA-3");

            var bankAccountSaved = service.GetBank(DummyApplicationNumber, "BA-3");
            Assert.Equal(true, bankAccountSaved.IsPrimary);
            bankAccountSaved = service.GetBank(DummyApplicationNumber, "BA-1");
            Assert.Equal(false, bankAccountSaved.IsPrimary);
            bankAccountSaved = service.GetBank(DummyApplicationNumber, "BA-2");
            Assert.Equal(false, bankAccountSaved.IsPrimary);
        }

        [Fact]
        public void ShouldThrowBankAccountNotFoundOnSetBankAsPrimary()
        {
            var applicantService = GetApplicantService(GetDummyApplicants());
            var service = GetApplicationService(applicantService, GetDummyApplication());

            Assert.Throws<NotFoundException>(() => { service.SetBankAsPrimary("123", "BA-3"); });

        }

        [Fact]
        public void ShouldHaveOnlyOnePrimaryAccountOnApplicationCreation()
        {
            var applicantService = GetApplicantService(GetDummyApplicants());
            var service = GetApplicationService(applicantService);
            var application = service.Add(new ApplicationRequest
            {
                Applicants = new List<Docitt.Application.IApplicantRequest>
                {
                    new Docitt.Application.ApplicantRequest
                    {
                        FirstName = "Nayan",
                        LastName = "Paregi",
                        Email = "user2@gmail.com",
                        //Password = "nayan",
                        Identities = new List<IIdentity>
                        {
                            new Identity {Name = "LId", Number = "12345"}
                        },
                        Ssn = "123456",
                        Addresses = new List<IAddress>
                        {
                            new Address
                            {
                                Line1 = "qwe",
                                City = "qwe",
                                State = "qwe",
                                ZipCode = "123",
                                Country = "qwe",
                                Type = AddressType.Home
                            }
                        },
                        PhoneNumbers = new List<IPhoneNumber>
                        {
                            new PhoneNumber {Number = "123", Type = "qwe"}
                        },
                        StatedGrossAnnualIncome = 1000,
                        HomeOwnership = HomeOwnershipType.Mortgage
                    }
                },
                Amount = 500000000,
                Purpose = "qwertyuiop",
                Scores = new[] { new Score { Date = DateTimeOffset.Now, Name = "name", Value = "value" } },
                Source = new Source { Id = "123", Type = SourceType.Broker },
                BankAccounts = new List<IBankAccount>()
                {
                    new BankAccount() { AccountNumber = "1", AccountType = BankAccountType.Checking, RoutingNumber = "R1",IsPrimary = true},
                    new BankAccount() {AccountNumber = "2", AccountType = BankAccountType.Checking, RoutingNumber = "R1",},
                    new BankAccount() {AccountNumber = "3", AccountType = BankAccountType.Checking, RoutingNumber = "R1"},
                }
            });


            var bankAccountSaved = service.GetBank(application.ApplicationNumber, "1-R1");
            Assert.Equal(true, bankAccountSaved.IsPrimary);
            bankAccountSaved = service.GetBank(application.ApplicationNumber, "2-R1");
            Assert.Equal(false, bankAccountSaved.IsPrimary);
            bankAccountSaved = service.GetBank(application.ApplicationNumber, "3-R1");
            Assert.Equal(false, bankAccountSaved.IsPrimary);
        }

        [Fact]
        public void ShouldHaveOnlyOnePrimaryAccountOnAddBankAccount()
        {
            var applicantService = GetApplicantService(GetDummyApplicants());
            var service = GetApplicationService(applicantService, GetDummyApplication(new List<IBankAccount>
            {
                        new BankAccount { Id="BA-1", IsPrimary = true},
                        new BankAccount { Id="BA-2" },
                        new BankAccount { Id="BA-3" },
                    }));

            service.AddBank(DummyApplicationNumber,
                new BankAccount
                {
                    AccountNumber = "BA",
                    RoutingNumber = "4",
                    AccountType = BankAccountType.Checking,
                    IsPrimary = true
                });

            var bankAccountSaved = service.GetBank(DummyApplicationNumber, "BA-1");
            Assert.Equal(false, bankAccountSaved.IsPrimary);
            bankAccountSaved = service.GetBank(DummyApplicationNumber, "BA-2");
            Assert.Equal(false, bankAccountSaved.IsPrimary);
            bankAccountSaved = service.GetBank(DummyApplicationNumber, "BA-3");
            Assert.Equal(false, bankAccountSaved.IsPrimary);
            bankAccountSaved = service.GetBank(DummyApplicationNumber, "BA-4");
            Assert.Equal(true, bankAccountSaved.IsPrimary);
        }

        [Fact]
        public void ShouldHaveOnlyOnePrimaryAccountOnUpdateBankAccount()
        {
            var applicantService = GetApplicantService(GetDummyApplicants());
            var service = GetApplicationService(applicantService, GetDummyApplication(new List<IBankAccount>
            {
                        new BankAccount { Id="BA-1", IsPrimary = true},
                        new BankAccount { Id="BA-2" },
                        new BankAccount { Id="BA-3" },
                    }));

            service.UpdateBank(DummyApplicationNumber, "BA-3",
                new BankAccount
                {
                    AccountNumber = "BA",
                    RoutingNumber = "4",
                    AccountType = BankAccountType.Checking,
                    IsPrimary = true
                });

            var bankAccountSaved = service.GetBank(DummyApplicationNumber, "BA-1");
            Assert.Equal(false, bankAccountSaved.IsPrimary);
            bankAccountSaved = service.GetBank(DummyApplicationNumber, "BA-2");
            Assert.Equal(false, bankAccountSaved.IsPrimary);
            bankAccountSaved = service.GetBank(DummyApplicationNumber, "BA-4");
            Assert.Equal(true, bankAccountSaved.IsPrimary);
        }

        [Fact]
        public void ShouldHaveOnlyOnePrimaryAccountOnSetBankAsPrimary()
        {
            var applicantService = GetApplicantService(GetDummyApplicants());
            var service = GetApplicationService(applicantService, GetDummyApplication(new List<IBankAccount>
            {
                        new BankAccount { Id="BA-1", IsPrimary = true},
                        new BankAccount { Id="BA-2" },
                        new BankAccount { Id="BA-3" },
                    }));
            service.SetBankAsPrimary(DummyApplicationNumber, "BA-3");

            var bankAccountSaved = service.GetBank(DummyApplicationNumber, "BA-1");
            Assert.Equal(false, bankAccountSaved.IsPrimary);
            bankAccountSaved = service.GetBank(DummyApplicationNumber, "BA-2");
            Assert.Equal(false, bankAccountSaved.IsPrimary);
            bankAccountSaved = service.GetBank(DummyApplicationNumber, "BA-3");
            Assert.Equal(true, bankAccountSaved.IsPrimary);
        }

        [Fact]
        public void ShouldModifyBankAccount()
        {
            var applicantService = GetApplicantService(GetDummyApplicants());
            var service = GetApplicationService(applicantService, GetDummyApplication(new List<IBankAccount>
            {
                new BankAccount {Id = "BA-1", IsPrimary = true},
                new BankAccount {Id = "BA-2"},
                new BankAccount {Id = "BA-3"},
                    }));

            const string routingNumber = "123";
            const string accountNumber = "456";
            const BankAccountType accountType = BankAccountType.Checking;
            const bool isPrimary = true;
            service.UpdateBank(DummyApplicationNumber, "BA-3", new BankAccount
            {
                AccountType = accountType,
                AccountNumber = accountNumber,
                RoutingNumber = routingNumber,
                IsPrimary = isPrimary
            });

            var updatedBank = service.GetBank(DummyApplicationNumber, "456-123");
            Assert.Equal(updatedBank.AccountNumber, accountNumber);
            Assert.Equal(updatedBank.RoutingNumber, routingNumber);
            Assert.Equal(updatedBank.AccountType, accountType);
            Assert.Equal(updatedBank.IsPrimary, isPrimary);


        }

        [Fact]
        public void ShouldThrowExceptionOnNonExistingBankAccount()
        {
            var applicantService = GetApplicantService(GetDummyApplicants());
            var service = GetApplicationService(applicantService, GetDummyApplication(new List<IBankAccount>
            {
                        new BankAccount { Id="BA-1", IsPrimary = true},
                        new BankAccount { Id="BA-2" },
                        new BankAccount { Id="BA-3" },
                    }));

            const string routingNumber = "123";
            const string accountNumber = "456";
            const BankAccountType accountType = BankAccountType.Checking;
            const bool isPrimary = true;

            Assert.Throws<NotFoundException>(() =>
            {
                service.UpdateBank(DummyApplicationNumber, "BA-10", new BankAccount
                {
                    AccountType = accountType,
                    AccountNumber = accountNumber,
                    RoutingNumber = routingNumber,
                    IsPrimary = isPrimary
                });
            });
        }

        [Fact]
        public void ShouldNotAllowDuplicateOnUpdateBankAccount()
        {
            const string routingNumber = "123";
            const string accountNumber = "456";
            const BankAccountType accountType = BankAccountType.Checking;
            const bool isPrimary = true;
            var applicantService = GetApplicantService(GetDummyApplicants());
            var service = GetApplicationService(applicantService, GetDummyApplication(new List<IBankAccount>
            {
                        new BankAccount {Id = "BA-1", IsPrimary = true},
                        new BankAccount {Id = "BA-2"},
                        new BankAccount
                        {
                            Id = accountNumber + "-" + routingNumber,
                            AccountType = accountType,
                            AccountNumber = accountNumber,
                            RoutingNumber = routingNumber,
                            IsPrimary = isPrimary
                        },
                    }));

            Assert.Throws<InvalidOperationException>(() =>
            {
                service.UpdateBank(DummyApplicationNumber, "BA-2", new BankAccount
                {
                    AccountType = accountType,
                    AccountNumber = accountNumber,
                    RoutingNumber = routingNumber,
                    IsPrimary = isPrimary
                });
            });
        }

        [Fact]
        public void ShouldRemoveBankAccount()
        {
            var applicantService = GetApplicantService(GetDummyApplicants());
            var service = GetApplicationService(applicantService, GetDummyApplication(new List<IBankAccount>
            {
                        new BankAccount {Id = "BA-1", IsPrimary = true},
                        new BankAccount {Id = "BA-2"},
                        new BankAccount {Id = "BA-3"},
            }));
            service.DeleteBank(DummyApplicationNumber, "BA-3");
            var bankAccounts = service.GetAllBanks(DummyApplicationNumber);
            var collection = bankAccounts as IBankAccount[] ?? bankAccounts.ToArray();
            Assert.DoesNotContain(collection, account => account.Id == "BA-3");
            Assert.Contains(collection, account => account.Id == "BA-1");
            Assert.Contains(collection, account => account.Id == "BA-2");


        }

        [Fact]
        public void ShouldThrowExceptionOnTryToRemoveActiveBankAccount()
        {
            var applicantService = GetApplicantService(GetDummyApplicants());
            var service = GetApplicationService(applicantService, GetDummyApplication(new List<IBankAccount>
            {
                        new BankAccount {Id = "BA-1", IsPrimary = true},
                        new BankAccount {Id = "BA-2"},
                        new BankAccount {Id = "BA-3"},
            }));
            Assert.Throws<InvalidOperationException>(() => service.DeleteBank(DummyApplicationNumber, "BA-1"));

        }

        [Fact]
        public void ShouldThrowExceptionOnTryToRemoveLastBankAccount()
        {
            var applicantService = GetApplicantService(GetDummyApplicants());
            var service = GetApplicationService(applicantService, GetDummyApplication(new List<IBankAccount>
            {
                        new BankAccount {Id = "BA-1", IsPrimary = true}
                    }));
            Assert.Throws<InvalidOperationException>(() => service.DeleteBank(DummyApplicationNumber, "BA-1"));
        }

        [Fact]
        public void ShouldGetAllBankAccounts()
        {
            var applicantService = GetApplicantService(GetDummyApplicants());
            var applicationService = GetApplicationService(applicantService, GetDummyApplication(new List<IBankAccount>
            {
                        new BankAccount {Id = "BA-1", IsPrimary = true},
                        new BankAccount {Id = "BA-2"},
                        new BankAccount {Id = "BA-3"},
                    }));

            var banks = applicationService.GetAllBanks(DummyApplicationNumber);
            Assert.Equal(3, banks.Count());
        }

        [Fact]
        public void EnableAutoPayment()
        {
            var applicantService = GetApplicantService(GetDummyApplicants());
            var applications = GetDummyApplication(new List<IBankAccount>
            {
                new BankAccount { Id = "BA-1", IsPrimary = true },
                new BankAccount { Id = "BA-2" },
                new BankAccount { Id = "BA-3" },
            });

            var applicationService = GetApplicationService(applicantService, applications);
            applicationService.EnableAutoPayment(applications.First().ApplicationNumber);

            var bankAccounts = applicationService.GetAllBanks(applications.First().ApplicationNumber);
            Assert.True(bankAccounts.FirstOrDefault(b => b.IsPrimary).AutoPayment);
        }

        [Fact]
        public void EnableAutoPaymentWhenHaveNoApplicationNumberValid()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                var applicantService = GetApplicantService(GetDummyApplicants());
                var applications = GetDummyApplication(new List<IBankAccount>
                {
                    new BankAccount { Id = "BA-1", IsPrimary = true },
                    new BankAccount { Id = "BA-2" },
                    new BankAccount { Id = "BA-3" },
                });
                var applicationService = GetApplicationService(applicantService, applications);
                applicationService.EnableAutoPayment(string.Empty);
            });
        }

        [Fact]
        public void EnableAutoPaymentWhenHaveNoPrimaryBank()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                var applicantService = GetApplicantService(GetDummyApplicants());
                var applications = GetDummyApplication(new List<IBankAccount>
                {
                    new BankAccount { Id = "BA-1" },
                    new BankAccount { Id = "BA-2" },
                    new BankAccount { Id = "BA-3" },
                });
                var applicationService = GetApplicationService(applicantService, applications);
                applicationService.EnableAutoPayment(applications.First().ApplicationNumber);
            });
        }

        [Fact]
        public void DisableAutoPayment()
        {
            var applicantService = GetApplicantService(GetDummyApplicants());
            var applications = GetDummyApplication(new List<IBankAccount>
            {
                new BankAccount { Id = "BA-1", IsPrimary = true },
                new BankAccount { Id = "BA-2" },
                new BankAccount { Id = "BA-3" },
            });

            var applicationService = GetApplicationService(applicantService, applications);
            applicationService.DisableAutoPayment(applications.First().ApplicationNumber);

            var bankAccounts = applicationService.GetAllBanks(applications.First().ApplicationNumber);
            Assert.False(bankAccounts.FirstOrDefault(b => b.IsPrimary).AutoPayment);
        }

        [Fact]
        public void DisableAutoPaymentWhenHaveNoApplicationNumberValid()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                var applicantService = GetApplicantService(GetDummyApplicants());
                var applications = GetDummyApplication(new List<IBankAccount>
                {
                    new BankAccount { Id = "BA-1", IsPrimary = true },
                    new BankAccount { Id = "BA-2" },
                    new BankAccount { Id = "BA-3" },
                });
                var applicationService = GetApplicationService(applicantService, applications);
                applicationService.DisableAutoPayment(string.Empty);
            });
        }

        [Fact]
        public void DisableAutoPaymentWhenHaveNoPrimaryBank()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                var applicantService = GetApplicantService(GetDummyApplicants());
                var applications = GetDummyApplication(new List<IBankAccount>
                {
                    new BankAccount { Id = "BA-1" },
                    new BankAccount { Id = "BA-2" },
                    new BankAccount { Id = "BA-3" },
                });
                var applicationService = GetApplicationService(applicantService, applications);
                applicationService.DisableAutoPayment(applications.First().ApplicationNumber);
            });
        }

        [Fact]
        public void Add_WhenHasMultipleAutoPaymentApplicationFail()
        {
            var fakeApplicantService = GetApplicantService(new List<IApplicant>
            {
                new Docitt.Applicant.Applicant
                {
                    Id="1",
                    Email = "user1@gmail.com"
                }
            });

            var service = GetApplicationService(fakeApplicantService);

            Assert.Throws<InvalidArgumentException>(() => service.Add(new ApplicationRequest
            {
                Applicants = new List<Docitt.Application.IApplicantRequest>
                {
                    new Docitt.Application.ApplicantRequest
                    {
                        FirstName = "Nayan",
                        LastName = "Paregi",
                        Email = "user2@gmail.com",
                        //Password = "nayan",
                        Identities = new List<IIdentity>
                        {
                            new Identity {Name = "LId", Number = "12345"}
                        },
                        Ssn = "123456",
                        Addresses = new List<IAddress>
                        {
                            new Address { Line1 = "qwe", City ="qwe", State = "qwe", ZipCode ="123", Country = "qwe", Type = AddressType.Home }
                        },
                        PhoneNumbers = new List<IPhoneNumber>
                        {
                            new PhoneNumber { Number = "123", Type = "qwe" }
                        },
                        StatedGrossAnnualIncome = 1000,
                        HomeOwnership = HomeOwnershipType.Mortgage
                    },
                    new Docitt.Application.ApplicantRequest
                    {
                        FirstName = "DummyFName",
                        LastName = "DummyLName",
                        Email = "user3@gmail.com",
                        //Password = "dummy",
                        Identities = new List<IIdentity>
                        {
                            new Identity {Name = "LId", Number = "12345"}
                        },
                        Ssn = "123456",
                        Addresses = new List<IAddress>
                        {
                            new Address { Line1 = "qwe", City ="qwe", State = "qwe", ZipCode ="123", Country = "qwe", Type = AddressType.Home }
                        },
                        PhoneNumbers = new List<IPhoneNumber>
                        {
                            new PhoneNumber { Number = "123", Type = "qwe" }
                        },
                        StatedGrossAnnualIncome = 1000,
                        HomeOwnership = HomeOwnershipType.Mortgage
                    }
                },
                Amount = 2500,
                Purpose = "Should fail by Fake Repo",
                Scores = new[] { new Score { Date = DateTimeOffset.Now, Name = "name", Value = "value" } },
                Source = new Source { Id = "123", Type = SourceType.Broker },
                BankAccounts = new List<IBankAccount>
                {
                    new BankAccount { Id = "BA-1", AccountNumber = "BA", RoutingNumber = "1" , AccountType = BankAccountType.Checking, AutoPayment = true },
                    new BankAccount { Id = "BA-2", AccountNumber = "BA", RoutingNumber = "2" , AccountType = BankAccountType.Checking, AutoPayment = true },
                    new BankAccount { Id = "BA-3", AccountNumber = "BA", RoutingNumber = "3" , AccountType = BankAccountType.Checking, AutoPayment = true },
                }
            }));

            //Make sure applicant's rollbacked
            Assert.Equal(1, fakeApplicantService.Applicants.Count);
            //Just check if add is called for first applicant and then rollbacked after failure in second applicant
            Assert.Equal(4, fakeApplicantService.CurrentId);
        }

        [Fact]
        public void ShouldHaveOnlyOneAutoPaymentAccountOnAddBankAccount()
        {
            var applicantService = GetApplicantService(GetDummyApplicants());
            var service = GetApplicationService(applicantService, GetDummyApplication(new List<IBankAccount>
            {
                        new BankAccount { Id="BA-1", IsPrimary = true, AutoPayment = true },
                        new BankAccount { Id="BA-2" },
                        new BankAccount { Id="BA-3" },
                    }));

            service.AddBank(DummyApplicationNumber,
                new BankAccount
                {
                    AccountNumber = "BA",
                    RoutingNumber = "4",
                    AccountType = BankAccountType.Checking,
                    IsPrimary = true,
                    AutoPayment = true
                });

            var bankAccountSaved = service.GetBank(DummyApplicationNumber, "BA-1");
            Assert.Equal(false, bankAccountSaved.IsPrimary);
            Assert.Equal(false, bankAccountSaved.AutoPayment);

            bankAccountSaved = service.GetBank(DummyApplicationNumber, "BA-2");
            Assert.Equal(false, bankAccountSaved.IsPrimary);
            Assert.Equal(false, bankAccountSaved.AutoPayment);

            bankAccountSaved = service.GetBank(DummyApplicationNumber, "BA-3");
            Assert.Equal(false, bankAccountSaved.IsPrimary);
            Assert.Equal(false, bankAccountSaved.AutoPayment);

            bankAccountSaved = service.GetBank(DummyApplicationNumber, "BA-4");
            Assert.Equal(true, bankAccountSaved.IsPrimary);
            Assert.Equal(true, bankAccountSaved.AutoPayment);
        }

        [Fact]
        public void ShouldHaveOnlyOneAutoPaymentAccountOnUpdateBankAccount()
        {
            var applicantService = GetApplicantService(GetDummyApplicants());
            var service = GetApplicationService(applicantService, GetDummyApplication(new List<IBankAccount>
            {
                new BankAccount { Id="BA-1", IsPrimary = true, AutoPayment = true},
                new BankAccount { Id="BA-2" },
                new BankAccount { Id="BA-3" },
            }));

            service.UpdateBank(DummyApplicationNumber, "BA-3",
                new BankAccount
                {
                    AccountNumber = "BA",
                    RoutingNumber = "4",
                    AccountType = BankAccountType.Checking,
                    AutoPayment = true,
                    IsPrimary = true
                });

            var bankAccountSaved = service.GetBank(DummyApplicationNumber, "BA-1");
            Assert.Equal(false, bankAccountSaved.IsPrimary);
            Assert.Equal(false, bankAccountSaved.AutoPayment);

            bankAccountSaved = service.GetBank(DummyApplicationNumber, "BA-2");
            Assert.Equal(false, bankAccountSaved.IsPrimary);
            Assert.Equal(false, bankAccountSaved.AutoPayment);

            bankAccountSaved = service.GetBank(DummyApplicationNumber, "BA-4");
            Assert.Equal(true, bankAccountSaved.IsPrimary);
            Assert.Equal(true, bankAccountSaved.AutoPayment);
        }

        [Fact]
        public void UpdateAnnualIncomeWhenNoApplicationNumberInformed()
        {
            var service = GetService();

            Assert.Throws<InvalidArgumentException>(() =>
            {
                service.UpdateAnnualIncome("", 5000);
            });

            Assert.Throws<InvalidArgumentException>(() =>
            {
                service.UpdateAnnualIncome(string.Empty, 5000);
            });
        }

        [Fact]
        public void UpdateAnnualIncomeWhenNoIncomeValueInformed()
        {
            var appNumber = "LUCSA_000004";
            var service = GetService();

            Assert.Throws<InvalidArgumentException>(() =>
            {
                service.UpdateAnnualIncome(appNumber, 0);
            });

            Assert.Throws<InvalidArgumentException>(() =>
            {
                service.UpdateAnnualIncome(appNumber, -1);
            });

            Assert.Throws<InvalidArgumentException>(() =>
            {
                service.UpdateAnnualIncome(appNumber, Double.MinValue);
            });
        }

        [Fact]
        public void UpdateAnnualIncomeWhenNoApplicationFound()
        {
            var appNumber = "LUSA_000004";
            var service = GetService();

            Assert.Throws<NotFoundException>(() =>
            {
                service.UpdateAnnualIncome(appNumber, 5000);
            });
        }

        [Fact]
        public void UpdateAnnualMustToTriggerEventWhenNewValueApplied()
        {
            var tenant = "my-tenant";
            var appNumber = "LUSA_000004";
            var newAnnualIncome = 15000;
            //---------
            var repository = new FakeApplicationRepository(Mock.Of<ITenantTime>());
            repository.Add(new Docitt.Application.Application
            {
                Amount = 5000,
                Applicants = new List<IApplicantData> { },
                ApplicationNumber = appNumber,
                BankAccounts = new List<IBankAccount> { },
                ExpirationDate = new TimeBucket(DateTime.Now.AddDays(1)),
                Purpose = "Purpose",
                ScoreRange = new ScoreRange(),
                Scores = new List<Score>(),
                Source = new Source(),
                Status = new Status { Code = "100.01", Name = "New" },
                SubmittedDate = new TimeBucket(DateTime.Now),
                TenantId = tenant,
                VerifiedAnnualIncome = 5000
            });
            //---------
            var eventHub = new Mock<IEventHubClient>();
            eventHub.Setup(s => s.Publish(It.IsAny<ApplicationModified>()));
            //---------

            var service = GetService(repository, eventHub.Object);
            service.UpdateAnnualIncome(appNumber, newAnnualIncome);
            var data = service.GetByApplicationNumber(appNumber);

            Assert.NotNull(data);
            Assert.Equal(appNumber, data.ApplicationNumber);
            Assert.Equal(newAnnualIncome, data.VerifiedAnnualIncome);
            eventHub.Verify(s => s.Publish(It.IsAny<ApplicationModified>()), Times.Once);
        }

        private const string DummyApplicantId = "123";
        private const string DummyApplicationNumber = "4567";

        private static List<IApplicant> GetDummyApplicants()
        {
            return new List<IApplicant>
            {
                new Docitt.Applicant.Applicant
                {
                    Id = DummyApplicantId,
                    Email = "nayan.paregi@gmail.com",
                    FirstName = "Nayan",
                    LastName = "Paregi",
                    MiddleName = "G"
                },
            };
        }

        private static List<IApplication> GetDummyApplication(List<IBankAccount> bankAccounts = null)
        {
            return new List<IApplication>
            {
                new Docitt.Application.Application
                {
                    Id = "123456",
                    ApplicationNumber = DummyApplicationNumber,
                    Applicants = new List<IApplicantData>
                    {
                        new ApplicantData
                        {
                            ApplicantId = DummyApplicantId,
                            CurrentEmployer = new Employer
                            {
                                Name = "Gateway",
                                Address = new Address
                                {
                                    City = "Ahmedabad",
                                    Country = "India"
                                }
                            },
                            PhoneNumbers = new List<IPhoneNumber>
                            {
                                new PhoneNumber {Number = "9825820589"}
                            }
                        }
                    },
                    BankAccounts = bankAccounts
                }
            };
        }

        [Fact]
        public void IsDuplicate_ExecutionOk()
        {
            Configuration.Setup(x => x.DuplicateStatuses).Returns(new[] { "100.01", "100.02", "100.03" });

            var service = GetApplicationService(DummyApplications, DummyApplicants);

            var result = service.IsDuplicate("123");

            Assert.NotNull(result);
            Assert.True(result);

            Configuration.Verify(x => x.DuplicateStatuses, Times.AtLeastOnce);
        }

        [Fact]
        public void IsDuplicate_When_Without_ApplicationNumber()
        {
            var service = GetApplicationService();
            Assert.Throws<InvalidArgumentException>(() => service.IsDuplicate(string.Empty));
        }

        [Fact]
        public void IsDuplicate_When_NotFound_Application()
        {
            var service = GetApplicationService(new List<IApplication>());
            Assert.Throws<NotFoundException>(() => service.IsDuplicate("123"));
        }

        [Fact]
        public void IsDuplicate_When_Without_Configuration()
        {
            var dummyApplications = new List<IApplication>
            {
                new Docitt.Application.Application
                {
                    Id = "123",
                    ApplicationNumber = "123",
                    Applicants = new List<IApplicantData>
                    {
                        new ApplicantData
                        {
                            ApplicantId = DummyApplicantId,
                            CurrentEmployer = new Employer
                            {
                                Name = "Gateway",
                                Address = new Address
                                {
                                    City = "Ahmedabad",
                                    Country = "India"
                                }
                            },
                            PhoneNumbers = new List<IPhoneNumber>
                            {
                                new PhoneNumber {Number = "9825820589"}
                            }
                        },
                        new ApplicantData
                        {
                            ApplicantId = DummyApplicantId,
                            CurrentEmployer = new Employer
                            {
                                Name = "Gateway",
                                Address = new Address
                                {
                                    City = "Ahmedabad",
                                    Country = "India"
                                }
                            },
                            PhoneNumbers = new List<IPhoneNumber>
                            {
                                new PhoneNumber {Number = "9825820589"}
                            }
                        }
                    }
                }
            };
            var dummyApplicants = new List<IApplicant>
            {
                new Docitt.Applicant.Applicant
                {
                    Id = DummyApplicantId,
                    Email = "nayan.paregi@gmail.com",
                    FirstName = "Nayan",
                    LastName = "Paregi",
                    MiddleName = "G"
                },
                new Docitt.Applicant.Applicant
                {
                    Id = DummyApplicantId,
                    Email = "nayan.paregi@gmail.com",
                    FirstName = "Nayan",
                    LastName = "Paregi",
                    MiddleName = "G"
                }
            };
            //var service = GetApplicationService(DummyApplications, DummyApplicants);
            var service = new ApplicationService(Mock.Of<IServiceClient>());
            Assert.Throws<InvalidArgumentException>(() => service.IsDuplicate("123"));
        }

        [Fact]
        public void IsDuplicate_When_Without_DuplicateStatuses()
        {
            Configuration.Setup(x => x.DuplicateStatuses);

            var service = GetApplicationService(DummyApplications, DummyApplicants);
            Assert.Throws<InvalidArgumentException>(() => service.IsDuplicate("123"));
        }

        [Fact]
        public void IsDuplicate_When_Without_Applicants()
        {
            Configuration.Setup(x => x.DuplicateStatuses).Returns(new[] { "100.01", "100.02", "100.03" });

            var applications = DummyApplications;
            applications.ForEach(a => a.Applicants.Clear());

            var service = GetApplicationService(applications);
            Assert.Throws<NotFoundException>(() => service.IsDuplicate("123"));

        }

        // This method is a generic way to have the ApplicationService with all dependencies as needed,
        // my suggestion is use this one instead of create many overloads.
        private static IApplicationService GetService
        (
            IApplicationRepository applicationRepository = null,
            IEventHubClient eventhub = null,
            ITenantTime tenantTime = null,
            Docitt.Application.IConfiguration configuration = null,
            ILogger logger = null,
            IApplicantService applicantService = null,
            IGeneratorService numberGenerator = null
        )
        {
            var inTimeApplicationRepository = applicationRepository ?? Mock.Of<IApplicationRepository>();
            var inTimeEventHub = eventhub ?? Mock.Of<IEventHubClient>();
            var inTimeTenantTime = tenantTime ?? Mock.Of<ITenantTime>();
            var inTimeConfiguration = configuration ?? Mock.Of<Docitt.Application.IConfiguration>();
            var inTimeLogger = logger ?? Mock.Of<ILogger>();
            var inTimeApplicantService = applicantService ?? Mock.Of<IApplicantService>();
            var inTimeNumberGenerator = numberGenerator ?? Mock.Of<IGeneratorService>();

            return new ApplicationService(Mock.Of<IServiceClient>());

        }
    }
}