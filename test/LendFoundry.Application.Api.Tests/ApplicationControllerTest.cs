﻿using Docitt.Applicant;
using Docitt.Application;
using Docitt.Application.Client;
using LendFoundry.Application.Mocks;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Microsoft.AspNet.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace LendFoundry.Application.Api.Tests
{
    public class ApplicationControllerTest
    {
        [Fact]
        public void AddApplicationReturnApplicationIdOnValidApplicant()
        {
            var applicantService = GetApplicantService();
            //var response = GetController(applicantService).Add(new ApplicationRequest
            //{
            //    Applicants = new List<Docitt.Application.IApplicantRequest>
            //    {
            //        new Docitt.Application.ApplicantRequest
            //        {
            //            FirstName = "Nayan",
            //            LastName = "Paregi",
            //            Email = "nayan.gp@sigmainfo.net",
            //            //Password = "nayan",
            //            Identities = new List<IIdentity>
            //            {
            //                new Identity {Name = "LId", Number = "12345"}
            //            },
            //            Ssn = "123456",
            //            Addresses = new List<IAddress>
            //            {
            //                new Address { Line1 = "qwe", City ="qwe", State = "qwe", ZipCode ="123", Country = "qwe", Type = AddressType.Home }
            //            },
            //            PhoneNumbers = new List<IPhoneNumber>
            //            {
            //                new PhoneNumber { Number = "123", Type = "qwe" }
            //            },
            //            StatedGrossAnnualIncome = 1000,
            //            HomeOwnership = HomeOwnershipType.Own
            //        },
            //        new Docitt.Application.ApplicantRequest
            //        {
            //            FirstName = "DummyFName",
            //            LastName = "DummyLName",
            //            Email = "dummy@sigmainfo.net",
            //            //Password = "dummy",
            //            Identities = new List<IIdentity>
            //            {
            //                new Identity {Name = "LId", Number = "12345"}
            //            },
            //            Ssn = "123456",
            //            Addresses = new List<IAddress>
            //            {
            //                new Address { Line1 = "qwe", City ="qwe", State = "qwe", ZipCode ="123", Country = "qwe", Type = AddressType.Home }
            //            },
            //            PhoneNumbers = new List<IPhoneNumber>
            //            {
            //                new PhoneNumber { Number = "123", Type = "qwe" }
            //            },
            //            StatedGrossAnnualIncome = 1000,
            //            HomeOwnership = HomeOwnershipType.Own
            //        }
            //    },
            //    Amount = 500000000,
            //    Purpose = "qwertyuiop",
            //    Scores = new[] { new Score { Date = DateTimeOffset.Now, Name = "name", Value = "value" } },
            //    Source = new Source { Id = "123", Type = SourceType.Broker }
            //});

            //Assert.IsType<HttpOkObjectResult>(response);
            //var application = ((HttpOkObjectResult)response).Value as IApplicationResponse;

            //Assert.NotNull(application);

            //Assert.NotNull(application.ApplicationNumber);
            //Assert.Equal("1", application.ApplicationNumber);

            ////check if applicant is inserted to separate collection/repository
            //var applicant1 = applicantService.Get(application.Applicants[0].ApplicantId);
            //Assert.NotNull(applicant1);
            //Assert.Equal("nayan.gp@sigmainfo.net", applicant1.Email);

            //var applicant2 = applicantService.Get(application.Applicants[1].ApplicantId);
            //Assert.NotNull(applicant2);
            //Assert.Equal("dummy@sigmainfo.net", applicant2.Email);
        }

        [Fact]
        public void AddApplicationReturnErrorOnIncompleteRequest()
        {
            //var response = GetController().Add(new ApplicationRequest());
            //Assert.IsType<ErrorResult>(response);
            //var result = response as ErrorResult;
            //Assert.NotNull(result);
            //Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public void AddApplicationReturnErrorOnNullRequest()
        {
            //var response = GetController().Add(null);
            //Assert.IsType<ErrorResult>(response);
            //var result = response as ErrorResult;
            //Assert.NotNull(result);
            //Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public void UpdateApplicationReturn404WithInvalidApplicationNumber()
        {
            //var response = GetController().UpdateApplication("InvalidApplicationNumber", new ApplicationUpdateRequest
            //{
            //    Amount = 500000000,
            //});

            //Assert.IsType<ErrorResult>(response);
            //var result = response as ErrorResult;
            //Assert.NotNull(result);
            //Assert.Equal(404, result.StatusCode);
        }

        [Fact]
        public void UpdateApplicationReturn204WithValidApplicationNumber()
        {
            //var dummyApplicants = new List<Docitt.Applicant.IApplicant>
            //{
            //    new Docitt.Applicant.Applicant
            //    {
            //        Id = "123",
            //        Email = "nayan.paregi@gmail.com",
            //        FirstName = "Nayan",
            //        LastName = "Paregi",
            //        MiddleName = "G"
            //    },
            //};

            //var dummyApplication = new List<Docitt.Application.IApplication>
            //{
            //    new Docitt.Application.Application
            //    {
            //        Id = "123456",
            //        ApplicationNumber = "4567",
            //        Applicants = new List<IApplicantData>
            //        {
            //            new ApplicantData
            //            {
            //                ApplicantId = "123",
            //                CurrentEmployer = new Employer
            //                {
            //                    Name = "Gateway",
            //                    Address = new Address
            //                    {
            //                        City = "Ahmedabad",
            //                        Country = "India"
            //                    }
            //                },
            //                PhoneNumbers = new List<IPhoneNumber>
            //                {
            //                    new PhoneNumber {Number = "9825820589"}
            //                }
            //            }
            //        }
            //    }
            //};

            //var applicantService = GetApplicantService(dummyApplicants);
            //var applicationService = GetApplicationService(applicantService, dummyApplication);

            //var response = GetController(applicationService, applicantService)
            //    .UpdateApplication("4567", new ApplicationUpdateRequest
            //    {
            //        Purpose = "New Purpose",
            //        Amount = 500000000
            //    });

            //Assert.IsType<NoContentResult>(response);

            //var application = applicationService.GetByApplicationNumber("4567");
            //Assert.Equal(application.Purpose, "New Purpose");
            //Assert.Equal(application.Amount, 500000000);

        }

        [Fact]
        public void UpdateApplicantReturn404WithInvalidApplicationNumber()
        {
            //var response = GetController()
            //    .UpdateApplicant("InvalidApplicationNumber", "InvalidApplicant", new ApplicantUpdateRequest());

            //Assert.IsType<ErrorResult>(response);
            //var result = response as ErrorResult;
            //Assert.NotNull(result);
            //Assert.Equal(404, result.StatusCode);
        }

        [Fact]
        public void UpdateApplicantReturn204WithValidApplicationNumber()
        {
            //var dummyApplicants = new List<IApplicant>
            //{
            //    new Docitt.Applicant.Applicant
            //    {
            //        Id = "123",
            //        Email = "nayan.paregi@gmail.com",
            //        FirstName = "Nayan",
            //        LastName = "Paregi",
            //        MiddleName = "G"
            //    },
            //};

            //var dummyApplication = new List<IApplication>
            //{
            //    new Docitt.Application.Application
            //    {
            //        Id = "123456",
            //        ApplicationNumber = "4567",
            //        Applicants = new List<IApplicantData>
            //        {
            //            new ApplicantData
            //            {
            //                ApplicantId = "123",
            //                CurrentEmployer = new Employer
            //                {
            //                    Name = "Gateway",
            //                    Address = new Address
            //                    {
            //                        City = "Ahmedabad",
            //                        Country = "India"
            //                    }
            //                },
            //                PhoneNumbers = new List<IPhoneNumber>
            //                {
            //                    new PhoneNumber {Number = "9825820589"}
            //                }
            //            }
            //        }
            //    }
            //};

            //var applicantService = GetApplicantService(dummyApplicants);
            //var applicationService = GetApplicationService(applicantService, dummyApplication);

            //var response = GetController(applicationService, applicantService)
            //    .UpdateApplicant("4567", "123", new ApplicantUpdateRequest { StatedGrossAnnualIncome = 2305.2 });

            //Assert.IsType<NoContentResult>(response);

            //var application = applicationService.GetByApplicationNumber("4567");
            //Assert.Equal(application.Applicants[0].StatedGrossAnnualIncome, 2305.2);

        }

        [Fact]
        public void UpdateApplicantAddressReturn404WithInvalidApplicationNumber()
        {
            //var response = GetController()
            //    .UpdateAddresses("InvalidApplicationNumber", "InvalidApplicant",
            //        new List<Address> { new Address { City = "Ahmedabad" } });

            //Assert.IsType<ErrorResult>(response);
            //var result = response as ErrorResult;
            //Assert.NotNull(result);
            //Assert.Equal(404, result.StatusCode);
        }

        [Fact]
        public void UpdateApplicantAddressReturn204WithValidApplicationNumber()
        {
            //var dummyApplicants = new List<IApplicant>
            //{
            //    new Docitt.Applicant.Applicant
            //    {
            //        Id = "123",
            //        Email = "nayan.paregi@gmail.com",
            //        FirstName = "Nayan",
            //        LastName = "Paregi",
            //        MiddleName = "G"
            //    },
            //};

            //var dummyApplication = new List<IApplication>
            //{
            //    new Docitt.Application.Application
            //    {
            //        Id = "123456",
            //        ApplicationNumber = "4567",
            //        Applicants = new List<IApplicantData>
            //        {
            //            new ApplicantData
            //            {
            //                ApplicantId = "123",
            //                CurrentEmployer = new Employer
            //                {
            //                    Name = "Gateway",
            //                    Address = new Address
            //                    {
            //                        City = "Ahmedabad",
            //                        Country = "India"
            //                    }
            //                },
            //                PhoneNumbers = new List<IPhoneNumber>
            //                {
            //                    new PhoneNumber {Number = "9825820589"}
            //                }
            //            }
            //        }
            //    }
            //};

            //var applicantService = GetApplicantService(dummyApplicants);
            //var applicationService = GetApplicationService(applicantService, dummyApplication);

            //var response = GetController(applicationService, applicantService)
            //    .UpdateAddresses("4567", "123", new List<Address> { new Address { City = "Irvine" } });

            //Assert.IsType<NoContentResult>(response);

            //var application = applicationService.GetByApplicationNumber("4567");
            //Assert.Equal("Irvine", application.Applicants[0].Addresses[0].City);

        }

        [Fact]
        public void UpdateApplicantPrimaryAddressReturn204WithValidApplicationNumber()
        {
            //var dummyApplicants = new List<IApplicant>
            //{
            //    new Docitt.Applicant.Applicant
            //    {
            //        Id = "123",
            //        Email = "nayan.paregi@gmail.com",
            //        FirstName = "Nayan",
            //        LastName = "Paregi",
            //        MiddleName = "G"
            //    },
            //};

            //var dummyApplication = new List<IApplication>
            //{
            //    new Docitt.Application.Application
            //    {
            //        Id = "123456",
            //        ApplicationNumber = "4567",
            //        Applicants = new List<IApplicantData>
            //        {
            //            new ApplicantData
            //            {
            //                ApplicantId = "123",
            //                IsPrimary = true,
            //                CurrentEmployer = new Employer
            //                {
            //                    Name = "Gateway",
            //                    Address = new Address
            //                    {
            //                        City = "Ahmedabad",
            //                        Country = "India"
            //                    }
            //                },
            //                PhoneNumbers = new List<IPhoneNumber>
            //                {
            //                    new PhoneNumber {Number = "9825820589"}
            //                }
            //            }
            //        }
            //    }
            //};

            //var applicantService = GetApplicantService(dummyApplicants);
            //var applicationService = GetApplicationService(applicantService, dummyApplication);

            //var response = GetController(applicationService, applicantService)
            //    .UpdatePrimaryAddress("4567", new Address { City = "Irvine", State = "CA", ZipCode = "92606", Line1 = "532 Santa Maria Drive" });

            //Assert.IsType<NoContentResult>(response);

            //var application = applicationService.GetByApplicationNumber("4567");
            //Assert.Equal("Irvine", application.Applicants[0].Addresses[0].City);
        }

        [Fact]
        public void UpdateApplicantPrimaryAddressReturn404WithInvalidApplicationNumber()
        {
            //var response = GetController()
            //    .UpdatePrimaryAddress("InvalidApplicationNumber",
            //        new Address { City = "Ahmedabad", State = "Gujarat", Line1 = "Ratna High Street", ZipCode = "380008" });

            //Assert.IsType<ErrorResult>(response);
            //var result = response as ErrorResult;
            //Assert.NotNull(result);
            //Assert.Equal(404, result.StatusCode);
        }

        [Fact]
        public void UpdateApplicantPrimaryAddressReturn404WithInvalidApplicantId()
        {
            //var dummyApplication = new List<IApplication>
            //{
            //    new Docitt.Application.Application
            //    {
            //        Id = "123456",
            //        ApplicationNumber = "4567",
            //        Applicants = new List<IApplicantData>
            //        {
            //            new ApplicantData
            //            {
            //                ApplicantId = "123",
            //                IsPrimary = false,
            //                CurrentEmployer = new Employer
            //                {
            //                    Name = "Gateway",
            //                    Address = new Address
            //                    {
            //                        City = "Ahmedabad",
            //                        Country = "India"
            //                    }
            //                },
            //                PhoneNumbers = new List<IPhoneNumber>
            //                {
            //                    new PhoneNumber {Number = "9825820589"}
            //                }
            //            }
            //        }
            //    }
            //};

            //var applicantService = GetApplicantService(null);
            //var applicationService = GetApplicationService(applicantService, dummyApplication);

            //var response = GetController(applicationService, applicantService)
            //    .UpdatePrimaryAddress("4567", new Address { City = "Irvine", State = "CA", ZipCode = "92606", Line1 = "532 Santa Maria Drive" });

            //Assert.IsType<ErrorResult>(response);
            //var result = response as ErrorResult;
            //Assert.NotNull(result);
            //Assert.Equal(404, result.StatusCode);

        }

        [Fact]
        public void UpdateApplicantPrimaryAddressReturn400WithBlankAddress()
        {
            //var dummyApplicants = new List<IApplicant>
            //{
            //    new Docitt.Applicant.Applicant
            //    {
            //        Id = "123",
            //        Email = "nayan.paregi@gmail.com",
            //        FirstName = "Nayan",
            //        LastName = "Paregi",
            //        MiddleName = "G"
            //    },
            //};

            //var dummyApplication = new List<IApplication>
            //{
            //    new Docitt.Application.Application
            //    {
            //        Id = "123456",
            //        ApplicationNumber = "4567",
            //        Applicants = new List<IApplicantData>
            //        {
            //            new ApplicantData
            //            {
            //                ApplicantId = "123",
            //                CurrentEmployer = new Employer
            //                {
            //                    Name = "Gateway",
            //                    Address = new Address
            //                    {
            //                        City = "Ahmedabad",
            //                        Country = "India"
            //                    }
            //                },
            //                PhoneNumbers = new List<IPhoneNumber>
            //                {
            //                    new PhoneNumber {Number = "9825820589"}
            //                }
            //            }
            //        }
            //    }
            //};

            //var applicantService = GetApplicantService(dummyApplicants);
            //var applicationService = GetApplicationService(applicantService, dummyApplication);

            //var response = GetController(applicationService, applicantService)
            //    .UpdatePrimaryAddress("4567", new Address { Line1 = string.Empty, State = "CA", City = "Irvine", ZipCode = "92606" });

            //Assert.IsType<ErrorResult>(response);
            //var result = response as ErrorResult;
            //Assert.NotNull(result);
            //Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public void UpdateApplicantPrimaryAddressReturn400WithBlankState()
        {
            //var dummyApplicants = new List<IApplicant>
            //{
            //    new Docitt.Applicant.Applicant
            //    {
            //        Id = "123",
            //        Email = "nayan.paregi@gmail.com",
            //        FirstName = "Nayan",
            //        LastName = "Paregi",
            //        MiddleName = "G"
            //    },
            //};

            //var dummyApplication = new List<IApplication>
            //{
            //    new Docitt.Application.Application
            //    {
            //        Id = "123456",
            //        ApplicationNumber = "4567",
            //        Applicants = new List<IApplicantData>
            //        {
            //            new ApplicantData
            //            {
            //                ApplicantId = "123",
            //                CurrentEmployer = new Employer
            //                {
            //                    Name = "Gateway",
            //                    Address = new Address
            //                    {
            //                        City = "Ahmedabad",
            //                        Country = "India"
            //                    }
            //                },
            //                PhoneNumbers = new List<IPhoneNumber>
            //                {
            //                    new PhoneNumber {Number = "9825820589"}
            //                }
            //            }
            //        }
            //    }
            //};

            //var applicantService = GetApplicantService(dummyApplicants);
            //var applicationService = GetApplicationService(applicantService, dummyApplication);

            //var response = GetController(applicationService, applicantService)
            //    .UpdatePrimaryAddress("4567", new Address { Line1 = "532 Santa Maria Drive", State = string.Empty, City = "Irvine", ZipCode = "92606" });

            //Assert.IsType<ErrorResult>(response);
            //var result = response as ErrorResult;
            //Assert.NotNull(result);
            //Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public void UpdateApplicantPrimaryAddressReturn400WithBlankCity()
        {
            //var dummyApplicants = new List<IApplicant>
            //{
            //    new Docitt.Applicant.Applicant
            //    {
            //        Id = "123",
            //        Email = "nayan.paregi@gmail.com",
            //        FirstName = "Nayan",
            //        LastName = "Paregi",
            //        MiddleName = "G"
            //    },
            //};

            //var dummyApplication = new List<IApplication>
            //{
            //    new Docitt.Application.Application
            //    {
            //        Id = "123456",
            //        ApplicationNumber = "4567",
            //        Applicants = new List<IApplicantData>
            //        {
            //            new ApplicantData
            //            {
            //                ApplicantId = "123",
            //                CurrentEmployer = new Employer
            //                {
            //                    Name = "Gateway",
            //                    Address = new Address
            //                    {
            //                        City = "Ahmedabad",
            //                        Country = "India"
            //                    }
            //                },
            //                PhoneNumbers = new List<IPhoneNumber>
            //                {
            //                    new PhoneNumber {Number = "9825820589"}
            //                }
            //            }
            //        }
            //    }
            //};

            //var applicantService = GetApplicantService(dummyApplicants);
            //var applicationService = GetApplicationService(applicantService, dummyApplication);

            //var response = GetController(applicationService, applicantService)
            //    .UpdatePrimaryAddress("4567", new Address { Line1 = "532 Santa Maria Drive", State = "CA", City = string.Empty, ZipCode = "92606" });

            //Assert.IsType<ErrorResult>(response);
            //var result = response as ErrorResult;
            //Assert.NotNull(result);
            //Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public void UpdateApplicantPrimaryAddressReturn400WithBlankZipcode()
        {
            //var dummyApplicants = new List<IApplicant>
            //{
            //    new Docitt.Applicant.Applicant
            //    {
            //        Id = "123",
            //        Email = "nayan.paregi@gmail.com",
            //        FirstName = "Nayan",
            //        LastName = "Paregi",
            //        MiddleName = "G"
            //    },
            //};

            //var dummyApplication = new List<IApplication>
            //{
            //    new Docitt.Application.Application
            //    {
            //        Id = "123456",
            //        ApplicationNumber = "4567",
            //        Applicants = new List<IApplicantData>
            //        {
            //            new ApplicantData
            //            {
            //                ApplicantId = "123",
            //                CurrentEmployer = new Employer
            //                {
            //                    Name = "Gateway",
            //                    Address = new Address
            //                    {
            //                        City = "Ahmedabad",
            //                        Country = "India"
            //                    }
            //                },
            //                PhoneNumbers = new List<IPhoneNumber>
            //                {
            //                    new PhoneNumber {Number = "9825820589"}
            //                }
            //            }
            //        }
            //    }
            //};

            //var applicantService = GetApplicantService(dummyApplicants);
            //var applicationService = GetApplicationService(applicantService, dummyApplication);

            //var response = GetController(applicationService, applicantService)
            //    .UpdatePrimaryAddress("4567", new Address { Line1 = "532 Santa Maria Drive", State = "CA", City = "Irvine", ZipCode = string.Empty });

            //Assert.IsType<ErrorResult>(response);
            //var result = response as ErrorResult;
            //Assert.NotNull(result);
            //Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public void GetApplicationReturnOk()
        {
            //var dummyApplicants = new List<IApplicant>
            //{
            //    new Docitt.Applicant.Applicant
            //    {
            //        Id = "123",
            //        Email = "nayan.paregi@gmail.com",
            //        FirstName = "Nayan",
            //        LastName = "Paregi",
            //        MiddleName = "G"
            //    },
            //};
            //var dummyApplications = new List<IApplication>
            //{
            //    new Docitt.Application.Application
            //    {
            //        Id = "123",
            //        ApplicationNumber = "123",
            //        Applicants = new List<IApplicantData>
            //        {
            //            new ApplicantData
            //            {
            //                ApplicantId = "123",
            //                CurrentEmployer = new Employer
            //                {
            //                    Name = "Gateway",
            //                    Address = new Address
            //                    {
            //                        City = "Ahmedabad",
            //                        Country = "India"
            //                    }
            //                },
            //                PhoneNumbers = new List<IPhoneNumber>
            //                {
            //                    new PhoneNumber {Number = "9825820589"}
            //                }
            //            }
            //        }
            //    }
            //};
            //var applicationController = GetController(dummyApplications, dummyApplicants);
            //var response = applicationController.Get("123");
            //Assert.IsType<HttpOkObjectResult>(response);
            //var application = ((HttpOkObjectResult)response).Value as IApplicationResponse;

            //Assert.NotNull(application);

            //Assert.NotNull(application.ApplicationNumber);
            //Assert.Equal("123", application.ApplicationNumber);
        }

        [Fact]
        public void GetApplicationReturn404()
        {
            //var applicationController = GetController();
            //var response = applicationController.Get("456");
            //Assert.IsType<ErrorResult>(response);
            //var result = response as ErrorResult;
            //Assert.NotNull(result);
            //Assert.Equal(result.StatusCode, 404);
        }

        [Fact]
        public void GetApplicationReturn400()
        {
            //var applicationController = GetController();
            //var response = applicationController.Get(null);
            //Assert.IsType<ErrorResult>(response);
            //var result = response as ErrorResult;
            //Assert.NotNull(result);
            //Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public void GetApplicationNumbersByApplicantNumbers400()
        {
            //var applicationController = GetController();
            //var response = applicationController.GetApplicationNumbersByApplicant(null);
            //Assert.IsType<ErrorResult>(response);
            //var result = response as ErrorResult;
            //Assert.NotNull(result);
            //Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public void GetApplicationNumbersByApplicantNumbers400WithEmptyCollection()
        {
            //var applicationController = GetController();
            //var response = applicationController.GetApplicationNumbersByApplicant("");
            //Assert.IsType<ErrorResult>(response);
            //var result = response as ErrorResult;
            //Assert.NotNull(result);
            //Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public void GetApplicationNumbersByApplicantNumbersReturnApplicationNumber()
        {
            //var dummyApplications = new List<IApplication>
            //{
            //    new Docitt.Application.Application
            //    {
            //        ApplicationNumber = "123",
            //        Applicants = new List<IApplicantData>
            //        {
            //            new ApplicantData
            //            {
            //                ApplicantId = "A-123-1"
            //            },
            //            new ApplicantData
            //            {
            //                ApplicantId = "A-123-1"
            //            }
            //        }
            //    },
            //    new Docitt.Application.Application
            //    {
            //        ApplicationNumber = "456",
            //        Applicants = new List<IApplicantData>
            //        {
            //            new ApplicantData
            //            {
            //                ApplicantId = "A-123-1"
            //            },
            //            new ApplicantData
            //            {
            //                ApplicantId = "A-456-1"
            //            }
            //        }
            //    },
            //    new Docitt.Application.Application
            //    {
            //        ApplicationNumber = "789",
            //        Applicants = new List<IApplicantData>
            //        {
            //            new ApplicantData
            //            {
            //                ApplicantId = "A-789-1"
            //            },
            //            new ApplicantData
            //            {
            //                ApplicantId = "A-789-1"
            //            }
            //        }
            //    }
            //};


            //var applicationController = GetController(dummyApplications);
            //var response = applicationController.GetApplicationNumbersByApplicant("A-123-1");
            //Assert.IsType<HttpOkObjectResult>(response);
            //var applicationNumbers = ((HttpOkObjectResult)response).Value as List<string>;

            //Assert.NotNull(applicationNumbers);
            //Assert.NotEmpty(applicationNumbers);
            //Assert.Contains(applicationNumbers, s => s == "123");
            //Assert.Contains(applicationNumbers, s => s == "456");
            //Assert.DoesNotContain(applicationNumbers, s => s == "789");


        }

        [Fact]
        public void GetApplicationNumbersByApplicantNumbers404WithNotFound()
        {
            //var applicantService = GetApplicantService(GetDummyApplicants());
            //var applicationService = GetApplicationService(applicantService, GetDummyApplication());
            //var response = GetController(applicationService, applicantService).
            //    GetApplicationNumbersByApplicant("Invalid");
            //Assert.IsType<ErrorResult>(response);
            //var result = response as ErrorResult;
            //Assert.NotNull(result);
            //Assert.Equal(404, result.StatusCode);
        }

        [Fact]
        public void UpdateScoreReturnOkOnSuccess()
        {
            //var applicantService = GetApplicantService(GetDummyApplicants());
            //var applicationService = GetApplicationService(applicantService, GetDummyApplication());
            //var response = GetController(applicationService, applicantService).UpdateScore(DummyApplicationNumber, DummyApplicantId, new Score
            //{
            //    Date = DateTimeOffset.Now,
            //    Name = "MyScore",
            //    Value = "Val1"
            //});
            //Assert.IsType<NoContentResult>(response);
        }

        [Fact]
        public void UpdateScoreReturn404OnInvalidApplicationNumber()
        {
            //var applicantService = GetApplicantService(GetDummyApplicants());
            //var applicationService = GetApplicationService(applicantService, GetDummyApplication());
            //var response = GetController(applicationService, applicantService)
            //    .UpdateScore("NOT-VALID", "MyScore", new Score
            //    {
            //        Date = DateTimeOffset.Now,
            //        Name = "MyScore",
            //        Value = "Val1"
            //    });
            //var result = response as ErrorResult;
            //Assert.NotNull(result);
            //Assert.Equal(result.StatusCode, 404);
        }

        [Fact]
        public void UpdateScoreReturn400OnInvalidScoreName()
        {
            //var applicantService = GetApplicantService(GetDummyApplicants());
            //var applicationService = GetApplicationService(applicantService, GetDummyApplication());
            //var response = GetController(applicationService, applicantService)
            //    .UpdateScore(DummyApplicationNumber, "", new Score
            //    {
            //        Date = DateTimeOffset.Now,
            //        Name = "",
            //        Value = "Val1"
            //    });
            //var result = response as ErrorResult;
            //Assert.NotNull(result);
            //Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public void UpdatePhoneNumbersReturnOkOnSuccess()
        {
            //var applicantService = GetApplicantService(GetDummyApplicants());
            //var applicationService = GetApplicationService(applicantService, GetDummyApplication());
            //var response = GetController(applicationService, applicantService)
            //    .UpdatePhoneNumbers(DummyApplicationNumber, DummyApplicantId, new List<PhoneNumber>
            //    {
            //    new PhoneNumber {IsPrimary = true, IsVerified = true, Number = "123", Type = "Home"}
            //});
            //Assert.IsType<NoContentResult>(response);
        }

        [Fact]
        public void UpdatePhoneNumbersReturn404OnInvalidApplicationNumber()
        {
            //var applicantService = GetApplicantService(GetDummyApplicants());
            //var applicationService = GetApplicationService(applicantService, GetDummyApplication());
            //var response = GetController(applicationService, applicantService)
            //    .UpdatePhoneNumbers("INVALIDAPPLICATION", "INVALID", new List<PhoneNumber>
            //    {
            //    new PhoneNumber {IsPrimary = true, IsVerified = true, Number = "123", Type = "Home"}
            //});
            //var result = response as ErrorResult;
            //Assert.NotNull(result);
            //Assert.Equal(result.StatusCode, 404);
        }

        [Fact]
        public void UpdatePhoneNumbersReturn400OnInvalidPhoneNumber()
        {
            //var applicantService = GetApplicantService(GetDummyApplicants());
            //var applicationService = GetApplicationService(applicantService, GetDummyApplication());
            //var response = GetController(applicationService, applicantService).UpdatePhoneNumbers("NOT-VALID", "",
            //    new List<PhoneNumber>
            //    {
            //        new PhoneNumber {IsPrimary = true, IsVerified = true, Number = "", Type = "Home"}
            //    });

            //var result = response as ErrorResult;
            //Assert.NotNull(result);
            //Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public void AddBankReturnOkOnSuccess()
        {
            //var applicantService = GetApplicantService(GetDummyApplicants());
            //var applicationService = GetApplicationService(applicantService, GetDummyApplication());

            //var response = GetController(applicationService, applicantService)
            //    .AddBankAccount(DummyApplicationNumber, new BankAccount
            //    {
            //        AccountNumber = "1",
            //        AccountType = BankAccountType.Savings,
            //        RoutingNumber = "R1"
            //    });

            //Assert.IsType<HttpOkObjectResult>(response);

            //var bankAccount = ((HttpOkObjectResult)response).Value as IBankAccount;
            //Assert.NotNull(bankAccount);
            //Assert.Equal(bankAccount.Id, "1-R1");
        }

        [Fact]
        public void AddBankReturn400()
        {
            //var applicantService = GetApplicantService(GetDummyApplicants());
            //var applicationService = GetApplicationService(applicantService, GetDummyApplication());

            //var response = GetController(applicationService, applicantService)
            //    .AddBankAccount(DummyApplicationNumber, new BankAccount
            //    {
            //        RoutingNumber = "R1"
            //    });

            //Assert.IsType<ErrorResult>(response);
            //var result = response as ErrorResult;
            //Assert.NotNull(result);
            //Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public void AddBankReturnNotFound()
        {
            //var applicantService = GetApplicantService(GetDummyApplicants());
            //var applicationService = GetApplicationService(applicantService, GetDummyApplication());

            //var response = GetController(applicationService, applicantService)
            //    .AddBankAccount("NotExist", new BankAccount
            //    {
            //        AccountNumber = "1",
            //        AccountType = BankAccountType.Savings,
            //        RoutingNumber = "R1"
            //    });

            //Assert.IsType<ErrorResult>(response);
            //var result = response as ErrorResult;
            //Assert.NotNull(result);
            //Assert.Equal(404, result.StatusCode);
        }

        //[Fact]
        //public void UpdateBankReturnOkOnSuccess()
        //{
        //    var dummyBankAccounts = new List<IBankAccount>
        //    {
        //        new BankAccount {Id = "BA-1"}
        //    };

        //    var applicantService = GetApplicantService(GetDummyApplicants());
        //    var applicationService = GetApplicationService(applicantService, GetDummyApplication(dummyBankAccounts));

        //    var response = GetController(applicationService, applicantService)
        //        .UpdateBankAccount(DummyApplicationNumber, "BA-1", new BankAccount
        //        {
        //            AccountNumber = "1",
        //            AccountType = BankAccountType.Savings,
        //            RoutingNumber = "R1"
        //        });

        //    Assert.IsType<HttpOkObjectResult>(response);

        //    var bankAccount = ((HttpOkObjectResult)response).Value as IBankAccount;
        //    Assert.NotNull(bankAccount);
        //    Assert.Equal(bankAccount.Id, "1-R1");
        //}

        //[Fact]
        //public void UpdateBankReturn404()
        //{
        //    var dummyBankAccounts = new List<IBankAccount>
        //    {
        //        new BankAccount {Id = "BA-1"}
        //    };

        //    var applicantService = GetApplicantService(GetDummyApplicants());
        //    var applicationService = GetApplicationService(applicantService, GetDummyApplication(dummyBankAccounts));

        //    var response = GetController(applicationService, applicantService)
        //        .UpdateBankAccount(DummyApplicationNumber, "NOTEXIST", new BankAccount
        //        {
        //            AccountNumber = "1",
        //            AccountType = BankAccountType.Savings,
        //            RoutingNumber = "R1"
        //        });

        //    Assert.IsType<ErrorResult>(response);
        //    var result = response as ErrorResult;
        //    Assert.NotNull(result);
        //    Assert.Equal(404, result.StatusCode);
        //}

        //[Fact]
        //public void DeleteBankReturnOkOnSuccess()
        //{
        //    var dummyBankAccounts = new List<IBankAccount>
        //    {
        //        new BankAccount {Id = "BA-1"}
        //    };

        //    var applicantService = GetApplicantService(GetDummyApplicants());
        //    var applicationService = GetApplicationService(applicantService, GetDummyApplication(dummyBankAccounts));

        //    var response = GetController(applicationService, applicantService)
        //        .DeleteBankAccounts(DummyApplicationNumber, "BA-1");

        //    Assert.IsType<NoContentResult>(response);

        //}

        //[Fact]
        //public void DeleteBankReturn404()
        //{
        //    var dummyBankAccounts = new List<IBankAccount>
        //    {
        //        new BankAccount {Id = "BA-1"}
        //    };

        //    var applicantService = GetApplicantService(GetDummyApplicants());
        //    var applicationService = GetApplicationService(applicantService, GetDummyApplication(dummyBankAccounts));

        //    var response = GetController(applicationService, applicantService)
        //        .DeleteBankAccounts(DummyApplicationNumber, "NOTEXIST");

        //    Assert.IsType<ErrorResult>(response);
        //    var result = response as ErrorResult;
        //    Assert.NotNull(result);
        //    Assert.Equal(404, result.StatusCode);
        //}

        //[Fact]
        //public void GetAllBankReturnOkOnSuccess()
        //{
        //    var dummyBankAccounts = new List<IBankAccount>
        //    {
        //        new BankAccount {Id = "BA-1"}
        //    };

        //    var applicantService = GetApplicantService(GetDummyApplicants());
        //    var applicationService = GetApplicationService(applicantService, GetDummyApplication(dummyBankAccounts));

        //    var response = GetController(applicationService, applicantService)
        //        .GetAllBanks(DummyApplicationNumber);

        //    Assert.IsType<HttpOkObjectResult>(response);
        //    var bankAccounts = ((HttpOkObjectResult)response).Value as List<IBankAccount>;
        //    Assert.NotNull(bankAccounts);
        //}

        //[Fact]
        //public void GetBankReturnOkOnSuccess()
        //{
        //    var bankAccounts = new List<IBankAccount>
        //    {
        //        new BankAccount {Id = "BA-1"}
        //    };

        //    var applicantService = GetApplicantService(GetDummyApplicants());
        //    var applicationService = GetApplicationService(applicantService, GetDummyApplication(bankAccounts));

        //    var response = GetController(applicationService, applicantService)
        //        .GetBank(DummyApplicationNumber, "BA-1");

        //    Assert.IsType<HttpOkObjectResult>(response);
        //    var bankAccount = ((HttpOkObjectResult)response).Value as IBankAccount;
        //    Assert.NotNull(bankAccount);
        //}

        //[Fact]
        //public void SetBankAsPrimaryReturnOkOnSuccess()
        //{
        //    var bankAccounts = new List<IBankAccount>
        //    {
        //        new BankAccount {Id = "BA-1"}
        //    };

        //    var applicantService = GetApplicantService(GetDummyApplicants());
        //    var applicationService = GetApplicationService(applicantService, GetDummyApplication(bankAccounts));

        //    var response = GetController(applicationService, applicantService)
        //        .SetBankAsprimary(DummyApplicationNumber, "BA-1");

        //    Assert.IsType<NoContentResult>(response);
        //}

        //[Fact]
        //public void UpdatePrimaryPhoneReturnOkOnSuccess()
        //{
        //    var applicantService = GetApplicantService(GetDummyApplicants());
        //    var applicationService = GetApplicationService(applicantService, GetDummyApplication());
        //    var response = GetController(applicationService, applicantService)
        //        .UpdatePrimaryPhoneNumber(DummyApplicationNumber, DummyApplicantId, new PhoneNumber { IsPrimary = true, IsVerified = true, Number = "123", Type = "Home", Extension = "123" });
        //    Assert.IsType<NoContentResult>(response);
        //}

        //[Fact]
        //public void UpdatePrimaryPhoneNumberReturn404OnInvalidApplicationNumber()
        //{
        //    //var applicantService = GetApplicantService(GetDummyApplicants());
        //    //var applicationService = GetApplicationService(applicantService, GetDummyApplication());
        //    //var response = GetController(applicationService, applicantService)
        //    //    .UpdatePrimaryPhoneNumber("INVALIDAPPLICATION", "INVALID", new PhoneNumber { IsPrimary = true, IsVerified = true, Number = "123", Type = "Home", Extension = "123" });
        //    //var result = response as ErrorResult;
        //    //Assert.NotNull(result);
        //    //Assert.Equal(result.StatusCode, 404);
        //}

        //[Fact]
        //public void UpdatePrimaryPhoneNumberReturn404OnInvalidApplicantId()
        //{
        //    var applicantService = GetApplicantService(GetDummyApplicants());
        //    var applicationService = GetApplicationService(applicantService, GetDummyApplication());
        //    var response = GetController(applicationService, applicantService)
        //        .UpdatePrimaryPhoneNumber(DummyApplicationNumber, "INVALID", new PhoneNumber { IsPrimary = true, IsVerified = true, Number = "123", Type = "Home", Extension = "123" });
        //    var result = response as ErrorResult;
        //    Assert.NotNull(result);
        //    Assert.Equal(result.StatusCode, 404);
        //}

        //[Fact]
        //public void UpdatePrimaryPhoneNumberReturn400OnInvalidPhoneNumber()
        //{
        //    var applicantService = GetApplicantService(GetDummyApplicants());
        //    var applicationService = GetApplicationService(applicantService, GetDummyApplication());
        //    var response = GetController(applicationService, applicantService).UpdatePrimaryPhoneNumber("NOT-VALID", "",
        //        new PhoneNumber { IsPrimary = true, IsVerified = true, Number = "", Type = "Home", Extension = "123" });

        //    var result = response as ErrorResult;
        //    Assert.NotNull(result);
        //    Assert.Equal(result.StatusCode, 400);
        //}

        //[Fact]
        //public void UpdatePrimaryPhoneNumberReturn400OnInvalidPhoneType()
        //{
        //    var applicantService = GetApplicantService(GetDummyApplicants());
        //    var applicationService = GetApplicationService(applicantService, GetDummyApplication());
        //    var response = GetController(applicationService, applicantService).UpdatePrimaryPhoneNumber("NOT-VALID", "",
        //        new PhoneNumber
        //        {
        //            IsPrimary = true,
        //            IsVerified = true,
        //            Number = "123",
        //            Type = "",
        //            Extension = "123"
        //        });

        //    var result = response as ErrorResult;
        //    Assert.NotNull(result);
        //    Assert.Equal(result.StatusCode, 400);
        //}

        private const string DummyApplicantId = "123";
        private const string DummyApplicationNumber = "4567";
        private IApplicationService applicationService;

        public ApplicationControllerTest(IApplicationService applicationService)
        {
            this.applicationService = applicationService;
        }

        private static List<IApplicant> GetDummyApplicants()
        {
            return new List<IApplicant>
            {
                new Docitt.Applicant.Applicant
                {
                    Id = DummyApplicantId,
                    Email = "nayan.paregi@gmail.com",
                    FirstName = "Nayan",
                    LastName = "Paregi",
                    MiddleName = "G"
                },
            };
        }

        private static List<IApplication> GetDummyApplication(List<IBankAccount> bankAccounts = null)
        {

            return new List<IApplication>
            {
                new Docitt.Application.Application
                {
                    Id = "123456",
                    ApplicationNumber = DummyApplicationNumber,
                    Applicants = new List<IApplicantData>
                    {
                        new ApplicantData
                        {
                            ApplicantId = DummyApplicantId,
                            CurrentEmployer = new Employer
                            {
                                Name = "Gateway",
                                Address = new Address
                                {
                                    City = "Ahmedabad",
                                    Country = "India"
                                }
                            },
                            PhoneNumbers = new List<IPhoneNumber>
                            {
                                new PhoneNumber {Number = "9825820589"}
                            }
                        }
                    },
                    BankAccounts = bankAccounts
                }
            };
        }

        private static ApplicationControllerTest GetController(List<IApplication> applications = null, IEnumerable<IApplicant> applicants = null)
        {
            var applicantService = new FakeApplicantService(new UtcTenantTime(), applicants ?? new List<IApplicant>());

            return new ApplicationControllerTest(GetApplicationService(applicantService, applications));
        }

        private static ApplicationControllerTest GetController(IApplicantService applicantService)
        {
            return new ApplicationControllerTest(GetApplicationService(applicantService));
        }

        private static ApplicationControllerTest GetController(IApplicationService applicationService, IApplicantService applicantService)
        {
            return new ApplicationControllerTest(applicationService);
        }

        private static IApplicantService GetApplicantService(IEnumerable<IApplicant> applicants = null)
        {
            return new FakeApplicantService(new UtcTenantTime(), applicants ?? new List<IApplicant>());
        }

        private static IApplicationService GetApplicationService(IApplicantService applicantService, List<IApplication> applications = null)
        {

            return new ApplicationService(Mock.Of<IServiceClient>());
        }
    }
}
