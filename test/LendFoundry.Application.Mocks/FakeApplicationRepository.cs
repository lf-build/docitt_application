﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using Docitt.Application;

namespace LendFoundry.Application.Mocks
{
    public class FakeApplicationRepository : IApplicationRepository
    {
        public FakeApplicationRepository(ITenantTime tenantTime, IEnumerable<IApplication> applicants) : this(tenantTime)
        {
            Applications.AddRange(applicants);
        }

        public FakeApplicationRepository(ITenantTime tenantTime)
        {
            TenantTime = tenantTime;
        }

        public ITenantTime TenantTime { get; set; }

        public List<IApplication> Applications { get; set; } = new List<IApplication>();

        public Task<IApplication> Get(string id)
        {
            return Task.FromResult(Applications.FirstOrDefault(applicant => applicant.Id.Equals(id)));
        }

        public void Add(IApplication application)
        {
            //IT IS JUST FOR ROLLBACK TEST
            if (application.Purpose == "Should fail by Fake Repo")
                throw new ArgumentException(nameof(application));
            Applications.Add(application);
        }

        public void Remove(IApplication application)
        {
            throw new NotImplementedException();
        }

        public void Update(IApplication application)
        {
            Applications.Remove(GetByApplicationNumber(application.ApplicationNumber));
            Applications.Add(application);
        }

        public IApplication GetByApplicationNumber(string applicationNumber)
        {
            return Applications.FirstOrDefault(applicant => applicant.ApplicationNumber.Equals(applicationNumber));
        }

        public List<string> GetApplicationNumbersByApplicantNumber(string applicantId)
        {
            return
                          Applications.Where(
                              application => application.Applicants.Any(applicant => applicantId == applicant.ApplicantId))
                              .Select(application => application.ApplicationNumber).ToList();
        }

        public void UpdateApplication(string applicationNumber, IApplicationUpdateRequest applicationRequest)
        {
            var applicant = GetByApplicationNumber(applicationNumber);
            applicant.Amount = applicationRequest.Amount;
            applicant.Purpose = applicationRequest.Purpose;
        }

        public IApplication UpdateAddresses(string applicationNumber, string applicantId, List<IAddress> addresses)
        {
            var application = GetByApplicationNumber(applicationNumber);
            if (application == null)
                throw new NotFoundException($"Application {applicationNumber} not found");
            var applicants = application.Applicants;
            var applicantToUpdate = applicants.FirstOrDefault(applicant => applicant.ApplicantId == applicantId);
            if (applicantToUpdate == null)
                throw new NotFoundException($"Applicant {applicantId} not found for application {applicationNumber} ");

            applicantToUpdate.Addresses = addresses;

            return application;
        }

        public IApplication UpdatePrimaryAddress(string applicationNumber, List<IAddress> addresses)
        {
            var application = GetByApplicationNumber(applicationNumber);
            if (application == null)
                throw new NotFoundException($"Application {applicationNumber} not found");
            var applicants = application.Applicants;
            var applicantToUpdate = applicants.FirstOrDefault(applicant => applicant.IsPrimary == true);
            if (applicantToUpdate == null)
                throw new NotFoundException($"Primary Applicant not found for application {applicationNumber} ");

            applicantToUpdate.Addresses = addresses;

            return application;
        }

        public IApplication UpdatePhoneNumbers(string applicationNumber, string applicantId, List<IPhoneNumber> phoneNumbers)
        {
            var application = GetByApplicationNumber(applicationNumber);
            if (application == null)
                throw new NotFoundException($"Application {applicationNumber} not found");
            var applicants = application.Applicants;
            var applicantToUpdate = applicants.FirstOrDefault(applicant => applicant.ApplicantId == applicantId);
            if (applicantToUpdate == null)
                throw new NotFoundException($"Applicant {applicantId} not found for application {applicationNumber} ");

            applicantToUpdate.PhoneNumbers = phoneNumbers;

            return application;
        }

        public IApplication UpdateApplicant(string applicationNumber, string applicantId, IApplicantUpdateRequest applicantUpdateRequest)
        {
            var application = GetByApplicationNumber(applicationNumber);
            if (application == null)
                throw new NotFoundException($"Application {applicationNumber} not found");
            var applicants = application.Applicants;
            var applicantToUpdate = applicants.FirstOrDefault(applicant => applicant.ApplicantId == applicantId);
            if (applicantToUpdate == null)
                throw new NotFoundException($"Applicant {applicantId} not found for application {applicationNumber} ");

            applicantToUpdate.CurrentEmployer = applicantUpdateRequest.CurrentEmployer;
            applicantToUpdate.EmploymentStatus = applicantUpdateRequest.EmploymentStatus;
            applicantToUpdate.HomeOwnership = applicantUpdateRequest.HomeOwnership;
            applicantToUpdate.StatedGrossAnnualIncome = applicantUpdateRequest.StatedGrossAnnualIncome;
            return application;
        }

        public Task<IEnumerable<IApplication>> All(Expression<Func<IApplication, bool>> query, int? skip = default(int?), int? quantity = default(int?))
        {
            throw new NotImplementedException();
        }

        public int Count(Expression<Func<IApplication, bool>> query)
        {
            throw new NotImplementedException();
        }

        public IApplication UpdateScore(string applicationNumber, string scoreName, Score score)
        {
            var application = GetByApplicationNumber(applicationNumber);
            if (application == null)
                throw new NotFoundException($"Application {applicationNumber} not found");

            var scores = new List<Score>(application.Scores ?? new List<Score>()) { score };
            application.Scores = scores;
            return application;
        }

        public IApplication UpdateBankAccounts(string applicationNumber, List<IBankAccount> bankAccounts)
        {
            var application = GetByApplicationNumber(applicationNumber);
            if (application == null)
                throw new NotFoundException($"Application {applicationNumber} not found");

            application.BankAccounts = bankAccounts;
            return application;
        }

        public void UpdateAnnualIncome(string applicationNumber, double annualIncome)
        {
            Applications = Applications.Select(a =>
            {
                if (a.ApplicationNumber == applicationNumber)
                {
                    a.VerifiedAnnualIncome = annualIncome;
                }
                return a;
            }).ToList();            
        }
    }
}
