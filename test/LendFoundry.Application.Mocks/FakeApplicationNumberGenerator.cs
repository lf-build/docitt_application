using LendFoundry.NumberGenerator;
using System.Threading.Tasks;

namespace LendFoundry.Application.Mocks
{
    public class FakeApplicationNumberGenerator : IGeneratorService
    {
        private int Id { get; set; }


        public Task<string> TakeNext(string entityId)
        {
            Id++;
            return Task.Run(() => { return Id.ToString(); });
        }
    }
}