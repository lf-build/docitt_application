﻿namespace Docitt.Application
{
    public interface IApplicantUpdateRequest
    {
        double StatedGrossAnnualIncome { get; set; }
        HomeOwnershipType HomeOwnership { get; set; }
        EmploymentStatus EmploymentStatus { get; set; }
        IEmployer CurrentEmployer { get; set; }
    }
}