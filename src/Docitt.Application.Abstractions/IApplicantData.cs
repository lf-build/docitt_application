﻿using System.Collections.Generic;

namespace Docitt.Application
{
    public interface IApplicantData
    {
        string ApplicantId { get; set; }

        List<IAddress> Addresses { get; set; }

        List<IPhoneNumber> PhoneNumbers { get; set; }

        double StatedGrossAnnualIncome { get; set; }

        HomeOwnershipType HomeOwnership { get; set; }

        EmploymentStatus EmploymentStatus { get; set; }

        IEmployer CurrentEmployer { get; set; }

        bool IsPrimary { get; set; }
        ApplicantType ApplicantType { get; set; }
    }
}