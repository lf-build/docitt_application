﻿namespace Docitt.Application
{
    public interface IStatus
    {
        string Code { get; set; }
        string Name { get; set; }
    }
}
