﻿using System;

namespace Docitt.Application
{
    public class Score : IScore
    {
        public string Name { get; set; }
        public DateTimeOffset Date { get; set; }
        public string Value { get; set; }
    }
}
