﻿namespace Docitt.Application
{
    public class ScoreRange : IScoreRange
    {
        public string Type { get; set; }
        public int MinValue { get; set; }
        public int MaxValue { get; set; }
    }
}