﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace Docitt.Application
{
    public class Employer : IEmployer
    {
        public string Name { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress Address { get; set; }
        public string PhoneNumber { get; set; }
        public int LengthOfEmploymentInMonths { get; set; }
        public bool IsVerified { get; set; }
    }
}