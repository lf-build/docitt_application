﻿using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;

namespace Docitt.Application
{
    public interface IApplication : IAggregate, IApplicationDetail
    {        
        List<IApplicantData> Applicants { get; set; }
    }
}
