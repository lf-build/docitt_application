﻿namespace Docitt.Application
{
    public interface IApplicationListener
    {
        void Start();
    }
}
