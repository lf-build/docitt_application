﻿using LendFoundry.Security.Tokens;

namespace Docitt.Application
{
    public interface IApplicationRepositoryFactory
    {
        IApplicationRepository Create(ITokenReader reader);
    }
}
