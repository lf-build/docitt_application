﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Application
{
    public interface IApplicationService
    {
        IApplicationResponse Add(IApplicationRequest applicationRequest);

        IApplicationResponse AddOrUpdateApplicants(string applicationNumber,List<IApplicantRequest> applicants);

        void UpdateApplication(string applicationNumber, IApplicationUpdateRequest applicationRequest);

        void UpdateAddresses(string applicationNumber, string applicantId, List<IAddress> addresses);

        void UpdatePrimaryAddress(string applicationNumber, IAddress address);

        void UpdatePhoneNumbers(string applicationNumber, string applicantId, List<IPhoneNumber> phoneNumbers);

        void UpdatePrimaryPhoneNumber(string applicationNumber, string applicantId, IPhoneNumber primaryPhoneNumber);

        void UpdateApplicant(string applicationNumber, string applicantId, IApplicantUpdateRequest  applicantUpdateRequest);

        IApplicationResponse GetByApplicationNumber(string applicationNumber);

        List<string> GetApplicationNumbersByApplicant(string applicantId);

        void UpdateScore(string applicationNumber, string scoreName, Score score);

        IBankAccount AddBank(string applicationNumber, IBankAccount bankAccount);

        void DeleteBank(string applicationNumber,string bankId);

        IEnumerable<IBankAccount> GetAllBanks(string applicationNumber);

        IBankAccount GetBank(string applicationNumber, string bankId);

        void SetBankAsPrimary(string applicationNumber, string bankId);

        void EnableAutoPayment(string applicationNumber);

        void DisableAutoPayment(string applicationNumber);

        IBankAccount UpdateBank(string applicationNumber, string bankId, IBankAccount bankAccount);

        bool IsDuplicate(string applicantId);

        void UpdateAnnualIncome(string applicationNumber, double annualIncome);
        
        IApplicationUnassigned GetUnassigned(string applicationNumber);

        Task<List<IApplication>> GetApplicationsByUserName(string userName);

        Task UpdateApplicantUserNameByApplicationNumber(string oldUserName,string applicationNumber, string userName);
    }
}