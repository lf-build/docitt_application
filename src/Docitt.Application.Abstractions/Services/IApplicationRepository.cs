﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Persistence;

namespace Docitt.Application
{
    public interface IApplicationRepository : IRepository<IApplication>
    {
        IApplication GetByApplicationNumber(string applicationNumber);

        List<string> GetApplicationNumbersByApplicantNumber(string applicantId);

        void UpdateApplication(string applicationNumber, IApplicationUpdateRequest applicationRequest);

        IApplication UpdateAddresses(string applicationNumber, string applicantId, List<IAddress> addresses);

        IApplication UpdatePrimaryAddress(string applicationNumber, List<IAddress> addresses);

        IApplication UpdatePhoneNumbers(string applicationNumber, string applicantId, List<IPhoneNumber> phoneNumbers);

        IApplication UpdateApplicant(string applicationNumber, string applicantId, IApplicantUpdateRequest applicantUpdateRequest);

        IApplication UpdateScore(string applicationNumber, string scoreName, Score score);

        IApplication UpdateBankAccounts(string applicationNumber, List<IBankAccount> bankAccounts);

        void UpdateAnnualIncome(string applicationNumber, double annualIncome);
        
        Task<IApplication> GetApplicationByApplicantId (string applicantId);
        
        void UpdateStatus(string applicationNumber, Status status);
    }
}