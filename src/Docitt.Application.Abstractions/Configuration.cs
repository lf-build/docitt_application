﻿using System.Collections.Generic;

namespace Docitt.Application
{
    public class Configuration : IConfiguration
    {
        public EventMapping[] Events { get; set; }
        public ExpirationMapping Expiration { get; set; }
        public NotificationMapping Notification { get; set; }

        public IEnumerable<string> DuplicateStatuses { get; set; }

        public string Database { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string ConnectionString { get; set; }
    }
}