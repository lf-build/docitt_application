﻿using LendFoundry.Foundation.Date;
using System.Collections.Generic;
using Newtonsoft.Json;
using LendFoundry.Foundation.Client;

namespace Docitt.Application
{
    public class ApplicationRequest : IApplicationRequest
    {
        public double Amount { get; set; }
        public string Purpose { get; set; }
        public IScoreRange ScoreRange { get; set; }
        public IEnumerable<Score> Scores { get; set; }
        public Source Source { get; set; }
        public TimeBucket SubmittedDate { get; set; }
        public Status Status { get; set; }
        public string ApplicationNumber { get; set; }
        public List<IBankAccount> BankAccounts { get; set; }
        public List<IApplicantRequest> Applicants { get; set; }
        public double VerifiedAnnualIncome { get; set; }
        public TimeBucket ExpirationDate { get; set; }
        public string WorkflowId { get; set; }
        public string Channel { get; set; }
        public List<Status> CompletedStatuses { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress PropertyAddress { get; set; }

        public double PropertyValue { get; set; }
    }
}
