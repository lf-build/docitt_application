﻿namespace Docitt.Application
{
    public interface IAddress
    {
        string Line1 { get; set; }
        string Line2 { get; set; }
        string Line3 { get; set; }
        string City { get; set; }
        string State { get; set; }
        string County {get; set;}
        string ZipCode { get; set; }
        string Country{ get; set; }
        AddressType Type { get; set; }
        bool IsVerified { get; set; }
        bool IsPrimary { get; set; }
    }
}