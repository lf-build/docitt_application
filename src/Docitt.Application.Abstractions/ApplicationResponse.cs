﻿using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Date;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Docitt.Application
{
    public class ApplicationResponse : IApplicationResponse
    {
        public ApplicationResponse() { }

        public ApplicationResponse(IApplication application, List<IApplicantRequest> applicantRequests)
        {
            ApplicationNumber = application.ApplicationNumber;
            Amount = application.Amount;
            Purpose = application.Purpose;
            ScoreRange = application.ScoreRange;
            Applicants = applicantRequests;
            Source = application.Source;
            SubmittedDate = application.SubmittedDate;
            Scores = application.Scores;
            Status = application.Status;
            BankAccounts = application.BankAccounts;
            VerifiedAnnualIncome = application.VerifiedAnnualIncome;
            ExpirationDate = application.ExpirationDate;
            WorkflowId = application.WorkflowId;
            Channel = application.Channel;
            CompletedStatuses = application.CompletedStatuses;
            PropertyAddress = application.PropertyAddress;
            PropertyValue = application.PropertyValue;
        }        

        public double Amount { get; set; }
        public string Purpose { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IScoreRange, ScoreRange>))]
        public IScoreRange ScoreRange { get; set; }
        public IEnumerable<Score> Scores { get; set; }
        public Source Source { get; set; }
        public string ApplicationNumber { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IBankAccount, BankAccount>))]
        public List<IBankAccount> BankAccounts { get; set; }
        public TimeBucket SubmittedDate { get; set; }
        public Status Status { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IApplicantRequest, ApplicantRequest>))]
        public List<IApplicantRequest> Applicants { get; set; }
        public double VerifiedAnnualIncome { get; set; }
        public TimeBucket ExpirationDate { get; set; }
        public string WorkflowId { get; set; }
        public string Channel { get; set; }
        public List<Status> CompletedStatuses { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress PropertyAddress { get; set; }

        public double PropertyValue { get; set; }
    }
}