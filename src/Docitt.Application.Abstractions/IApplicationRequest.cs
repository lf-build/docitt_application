﻿using System.Collections.Generic;

namespace Docitt.Application
{
    public interface IApplicationRequest : IApplicationDetail
    {
        List<IApplicantRequest> Applicants { get; set; }
    }
}