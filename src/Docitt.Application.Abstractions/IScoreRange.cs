﻿namespace Docitt.Application
{
    public interface IScoreRange
    {
        string Type { get; set; }
        int MinValue { get; set; }
        int MaxValue { get; set; }
    }
}