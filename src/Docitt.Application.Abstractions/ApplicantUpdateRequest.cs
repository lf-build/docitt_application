﻿namespace Docitt.Application
{
    public class ApplicantUpdateRequest : IApplicantUpdateRequest
    {
        public double StatedGrossAnnualIncome { get; set; }
        public HomeOwnershipType HomeOwnership { get; set; }
        public EmploymentStatus EmploymentStatus { get; set; }
        public IEmployer CurrentEmployer { get; set; }
    }
}