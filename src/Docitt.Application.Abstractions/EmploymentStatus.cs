﻿namespace Docitt.Application
{
    public enum EmploymentStatus
    {
        Undefined,
        Employed,
        SelfEmployed,
        Retired,
        Other
    }
}