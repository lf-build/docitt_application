﻿using System.Collections.Generic;

namespace Docitt.Application
{
    public interface IApplicationResponse : IApplicationDetail
    {
        List<IApplicantRequest> Applicants { get; set; }        
    }
}