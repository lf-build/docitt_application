using System.Collections.Generic;
using LendFoundry.Foundation.Client;

namespace Docitt.Application
{
    public interface IConfiguration : IDependencyConfiguration
    {
         EventMapping[] Events { get; set; }
         ExpirationMapping Expiration { get; set; }
        IEnumerable<string> DuplicateStatuses { get; set; }
    }
}

