﻿using LendFoundry.Foundation.Date;
using System.Collections.Generic;
using Newtonsoft.Json;
using LendFoundry.Foundation.Client;

namespace Docitt.Application
{
    public interface IApplicationDetail
    {
        double Amount { get; set; }
        string Purpose { get; set; }
        IScoreRange ScoreRange { get; set; }
        Source Source { get; set; }
        TimeBucket SubmittedDate { get; set; }
        IEnumerable<Score> Scores { get; set; }
        Status Status { get; set; }
        List<Status> CompletedStatuses { get; set; }
        string ApplicationNumber { get; set; }
        List<IBankAccount> BankAccounts { get; set; }
        double VerifiedAnnualIncome { get; set; }
        TimeBucket ExpirationDate { get; set; }
        string WorkflowId { get; set; }
        string Channel { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        IAddress PropertyAddress { get; set; }

        double PropertyValue { get; set; } // this will store the Purchase or refinance value.
    }
}