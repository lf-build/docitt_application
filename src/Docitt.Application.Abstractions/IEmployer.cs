﻿namespace Docitt.Application
{
    public interface IEmployer
    {
        string Name { get; set; }
        IAddress Address { get; set; }
        string PhoneNumber { get; set; }
        int LengthOfEmploymentInMonths { get; set; }
        bool IsVerified { get; set; }
    }
}