﻿using LendFoundry.Foundation.Date;
using System.Collections.Generic;
namespace Docitt.Application
{
    public class ApplicationUpdateRequest : IApplicationUpdateRequest
    {
        public double Amount { get; set; }
        public string Purpose { get; set; }
        public IScoreRange ScoreRange { get; set; }
        public Source Source { get; set; }
        public TimeBucket SubmittedDate { get; set; }
        public IEnumerable<Score> Scores { get; set; }
        public string Status { get; set; }
        public string ApplicationNumber { get; set; }
        public string Channel { get; set; }

        public double PropertyValue { get; set; }
    }
}