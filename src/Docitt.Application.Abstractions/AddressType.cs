﻿namespace Docitt.Application
{
   public enum AddressType
    {
        Undefined,
        Home,
        Mailing,
        Business,
        Military
    }
}