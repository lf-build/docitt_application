﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
namespace Docitt.Application
{
    public class ApplicantData : IApplicantData
    {
        public ApplicantData(){}

        public ApplicantData(IApplicantData applicantData)
        {
            ApplicantId = applicantData.ApplicantId;
            Addresses = applicantData.Addresses;
            PhoneNumbers = applicantData.PhoneNumbers;
            StatedGrossAnnualIncome = applicantData.StatedGrossAnnualIncome;
            HomeOwnership = applicantData.HomeOwnership;
            EmploymentStatus = applicantData.EmploymentStatus;
            CurrentEmployer = applicantData.CurrentEmployer;
            IsPrimary = applicantData.IsPrimary;
            ApplicantType = applicantData.ApplicantType;
        }

        public string ApplicantId { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        public List<IAddress> Addresses { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPhoneNumber, PhoneNumber>))]
        public List<IPhoneNumber> PhoneNumbers { get; set; }
        public double StatedGrossAnnualIncome { get; set; }
        public HomeOwnershipType HomeOwnership { get; set; }
        public EmploymentStatus EmploymentStatus { get; set; }
        public IEmployer CurrentEmployer { get; set; }
        public bool IsPrimary { get; set; }
        public ApplicantType ApplicantType { get; set; }
    }
}