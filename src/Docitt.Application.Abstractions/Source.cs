﻿namespace Docitt.Application
{
    public class Source
    {
        public SourceType Type { get; set; }
        public string Id { get; set; }
    }
}