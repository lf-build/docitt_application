﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using LendFoundry.Foundation.Client;

namespace Docitt.Application
{
    public class Application : Aggregate, IApplication
    {
        public Application()
        {
            Applicants = new List<IApplicantData>();
        }

        public Application(IApplicationRequest applicationRequest)
        {
            // by default application must to have default applicant, if it`s not infomed
            // just get the first one and set it.
            if (applicationRequest.Applicants.TrueForAll(a => !a.IsPrimary ))
                applicationRequest.Applicants.First().IsPrimary = true;

            Applicants = new List<IApplicantData>(applicationRequest.Applicants.Select(applicant =>
            {
                // define primary address
                //commenting these below function as it is needed for Docitt
                /*
                if (applicant.Addresses != null && applicant.Addresses.TrueForAll(a => a.IsPrimary == false))
                    applicant.Addresses.First().IsPrimary = true;

                // define primary phone
                if (applicant.PhoneNumbers != null && applicant.PhoneNumbers.TrueForAll(a => a.IsPrimary == false))
                    applicant.PhoneNumbers.First().IsPrimary = true;
                */

                return new ApplicantData(applicant);
            }));

            BankAccounts = applicationRequest.BankAccounts;
            Amount = applicationRequest.Amount;
            Purpose = applicationRequest.Purpose;
            ScoreRange = applicationRequest.ScoreRange;
            Source = applicationRequest.Source;
            SubmittedDate = applicationRequest.SubmittedDate;
            Scores = applicationRequest.Scores;
            Status = applicationRequest.Status;
            CompletedStatuses = applicationRequest.CompletedStatuses ?? new List<Status>();
            VerifiedAnnualIncome = applicationRequest.VerifiedAnnualIncome;
            WorkflowId = applicationRequest.WorkflowId;
            Channel = applicationRequest.Channel;
            PropertyAddress = applicationRequest.PropertyAddress;
            PropertyValue = applicationRequest.PropertyValue;
        }

        public double Amount { get; set; }
        public string Purpose { get; set; }
        public IScoreRange ScoreRange { get; set; }
        public IEnumerable<Score> Scores { get; set; }
        public Source Source { get; set; }
        public string ApplicationNumber { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IBankAccount, BankAccount>))]
        public List<IBankAccount> BankAccounts { get; set; }
        public TimeBucket SubmittedDate { get; set; }
        public Status Status { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IApplicantData, ApplicantData>))]
        public List<IApplicantData> Applicants { get; set; }
        public double VerifiedAnnualIncome { get; set; }
        public TimeBucket ExpirationDate { get; set; }
        public string WorkflowId { get; set; }
        public string Channel { get; set; }

        public List<Status> CompletedStatuses { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress PropertyAddress { get; set; }

        public double PropertyValue { get; set; }
    }
}