﻿namespace Docitt.Application
{
    public interface IBankAccount
    {
        string RoutingNumber { get; }

        string AccountNumber { get; set; }

        BankAccountType AccountType { get; }

        bool IsPrimary { get; set; }

        string Id { get; set; }

        bool AutoPayment { get; set; }
    }
}