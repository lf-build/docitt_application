﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Docitt.Applicant;

namespace Docitt.Application
{
    public interface IApplicationDeclined
    {
         string ApplicationNumber { get; set; }
         IEnumerable<string> Reasons { get; set; }
         object GetTemplatePayload(IApplication application, IApplicant applicant);
    }
}
