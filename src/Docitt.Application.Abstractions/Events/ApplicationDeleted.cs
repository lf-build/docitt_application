﻿namespace Docitt.Application.Events
{
    public class ApplicationDeleted
    {
        public IApplication Application { get; set; }
    }
}