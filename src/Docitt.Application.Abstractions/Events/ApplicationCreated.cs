﻿namespace Docitt.Application.Events
{
    public class ApplicationCreated
    {
        public IApplication Application { get; set; }
    }
}
