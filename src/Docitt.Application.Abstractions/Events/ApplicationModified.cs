﻿namespace Docitt.Application.Events
{
    public class ApplicationModified
    {
        public IApplication Application { get; set; }
    }
}