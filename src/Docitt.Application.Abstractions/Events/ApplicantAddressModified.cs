﻿using System.Collections.Generic;

namespace Docitt.Application.Events
{
    public class ApplicantAddressModified
    {
        public string ApplicationNumber { get; set; }

        public string ApplicantId { get; set; }

        public IEnumerable<IAddress> OldAddresses { get; set; }

        public IEnumerable<IAddress> NewAddresses { get; set; }
    }
}