﻿
namespace Docitt.Application.Events
{
    public class ApplicationOptedInForAutoPayment
    {
        public string ApplicationNumber { get; set; }
    }
}