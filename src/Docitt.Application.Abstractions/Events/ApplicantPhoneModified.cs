﻿using System.Collections.Generic;

namespace Docitt.Application.Events
{
    public class ApplicantPhoneModified
    {
        public string ApplicationNumber { get; set; }

        public string ApplicantId { get; set; }

        public IEnumerable<IPhoneNumber> OldPhoneNumbers { get; set; }

        public IEnumerable<IPhoneNumber> NewPhoneNumbers { get; set; }
    }
}