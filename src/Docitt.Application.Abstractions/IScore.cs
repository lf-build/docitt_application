﻿using System;

namespace Docitt.Application
{
    public interface IScore
    {
        string Name { get; set; }
        DateTimeOffset Date { get; set; }
        string Value { get; set; }
    }
}
