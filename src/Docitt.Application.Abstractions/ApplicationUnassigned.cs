﻿using Docitt.Applicant;
using System;

namespace Docitt.Application
{
    public class ApplicationUnassigned : IApplicationUnassigned
    {
        public ApplicationUnassigned(IApplication application, IApplicant applicant)        
        {
            ApplicationNumber = application.ApplicationNumber;
            FullName = $"{applicant.FirstName} {applicant.LastName}";
            Last4Ssn = applicant.Ssn.Substring(applicant.Ssn.Length - 4); ;
            SubmittedTime = application.SubmittedDate.Time;           
        }

        public string ApplicationNumber { get; set; }

        public string FullName { get; set; }

        public string Last4Ssn { get; set; }

        public DateTimeOffset SubmittedTime { get; set; }
    }
}
