﻿using System;

namespace Docitt.Application
{
    public interface IApplicationUnassigned
    {
        string ApplicationNumber { get; set; }
        DateTimeOffset SubmittedTime { get; set; }
        string FullName { get; set; }
        string Last4Ssn { get; set; }        
    }
}
