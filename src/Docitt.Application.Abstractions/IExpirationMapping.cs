﻿namespace Docitt.Application
{
    public interface IExpirationMapping
    {
         int DaysToExpire { get; set; }

    }
}