﻿namespace Docitt.Application
{
    public enum BankAccountType
    {
        Undefined,
        Checking,
        Savings,
        Others
    }
}
