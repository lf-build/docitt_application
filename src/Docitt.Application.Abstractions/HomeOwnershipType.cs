﻿namespace Docitt.Application
{
    public enum HomeOwnershipType
    {
        Undefined,
        Rent,
        Mortgage,
        Own        
    }
}