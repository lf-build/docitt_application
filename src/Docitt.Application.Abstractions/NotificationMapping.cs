﻿namespace Docitt.Application
{
    public class NotificationMapping
    {
        public string Template { get; set; }

        public string Version { get; set; }
    }
}
