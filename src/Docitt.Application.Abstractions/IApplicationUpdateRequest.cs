﻿using System.Collections.Generic;
using LendFoundry.Foundation.Date;

namespace Docitt.Application
{
    public interface IApplicationUpdateRequest 
    {
        double Amount { get; set; }
        string Purpose { get; set; }
        IScoreRange ScoreRange { get; set; }
        Source Source { get; set; }
        TimeBucket SubmittedDate { get; set; }
        IEnumerable<Score> Scores { get; set; }
        string Status { get; set; }
        string ApplicationNumber { get; set; }
        string Channel { get; set; }
        double PropertyValue { get; set; }

    }
}