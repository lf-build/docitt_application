﻿namespace Docitt.Application
{
    public class PhoneNumber : IPhoneNumber
    {
        public string Number { get; set; }
        public string Type { get; set; }
        public bool IsVerified { get; set; }
        public bool IsPrimary { get; set; }
        public string Extension { get; set; }
    }
}