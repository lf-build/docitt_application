using System.ComponentModel;

namespace Docitt.Application
{
    public enum ApplicantType
    {
        Borrower,
        [Description("Spouse")]
        Spouse,
        [Description("Co-Borrower")]
        CoBorrower,
        [Description("CoBorrowerSpouse")]
        CoBorrowerSpouse
    }
}