﻿namespace Docitt.Application
{
    public enum SourceType
    {
        Undefined,
        Broker,
        Merchant,
        Affiliate,
        Campaign,        
    }
    
}