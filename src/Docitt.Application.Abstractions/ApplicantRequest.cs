﻿using Docitt.Applicant;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Security.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Docitt.Application
{
    public class ApplicantRequest : Aggregate, IApplicantRequest
    {
        public ApplicantRequest() { }

        public ApplicantRequest(IApplicantData applicantData, IApplicant applicant)
        {
            ApplicantId = applicantData.ApplicantId;
            Addresses = applicantData.Addresses;
            CurrentEmployer = applicantData.CurrentEmployer;
            PhoneNumbers = applicantData.PhoneNumbers;
            StatedGrossAnnualIncome = applicantData.StatedGrossAnnualIncome;
            HomeOwnership = applicantData.HomeOwnership;
            EmploymentStatus = applicantData.EmploymentStatus;
            Email = applicant.Email;
            FirstName = applicant.FirstName;
            Identities = applicant.Identities;
            LastName = applicant.LastName;
            MiddleName = applicant.MiddleName;
            Salutation = applicant.Salutation;
            Ssn = applicant.Ssn;
            DateOfBirth = applicant.DateOfBirth;
            IsPrimary = applicantData.IsPrimary;
            ApplicantType = applicantData.ApplicantType;
            UserIdentity = applicant.UserIdentity;
            GenerationOrSuffix = applicant.GenerationOrSuffix;
        }
       
        public List<string> Salutation { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Email { get; set; }
        //public string Password { get; set; }
        public string Ssn { get; set; }
        public DateTime? DateOfBirth { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IIdentity, Identity>))]
        public List<IIdentity> Identities { get; set; }
        public string ApplicantId { get { return Id; } set { Id = value; } }
        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        public List<IAddress> Addresses { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IPhoneNumber, PhoneNumber>))]
        public List<IPhoneNumber> PhoneNumbers { get; set; }
        public double StatedGrossAnnualIncome { get; set; }

        public HomeOwnershipType HomeOwnership { get; set; }

        public EmploymentStatus EmploymentStatus { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IEmployer, Employer>))]
        public IEmployer CurrentEmployer { get; set; }

        public bool IsPrimary { get; set; }
        public ApplicantType ApplicantType { get; set; }

        public string GenerationOrSuffix { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IUserIdentity, UserIdentity>))]
        public IUserIdentity UserIdentity { get; set; }
    }
}