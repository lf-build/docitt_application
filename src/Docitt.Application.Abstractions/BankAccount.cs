﻿namespace Docitt.Application
{
    public class BankAccount : IBankAccount
    {
        public string RoutingNumber { get; set; }

        public string AccountNumber { get; set; }

        public BankAccountType AccountType { get; set; }

        public bool IsPrimary { get; set; }

        public string Id { get; set; }

        public bool AutoPayment { get; set; }
    }
}