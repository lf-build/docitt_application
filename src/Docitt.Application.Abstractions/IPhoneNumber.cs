﻿namespace Docitt.Application
{
    public interface IPhoneNumber
    {
        string Number { get; set; }
        string Type { get; set; }
        bool IsVerified { get; set; }
        bool IsPrimary { get; set; }
        string Extension { get; set; }
    }
}