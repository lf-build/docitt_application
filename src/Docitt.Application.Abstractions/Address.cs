﻿namespace Docitt.Application
{
    public class Address : IAddress
    {
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public string Line3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string County {get; set;}
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public AddressType Type { get; set; }
        public bool IsVerified { get; set; }
        public bool IsPrimary { get; set; }
    }
}