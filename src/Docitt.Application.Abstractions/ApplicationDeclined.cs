﻿using Docitt.Applicant;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Docitt.Application
{
    public class ApplicationDeclined : IApplicationDeclined
    {
        public string ApplicationNumber { get; set; }

        public IEnumerable<string> Reasons { get; set; }

        public object GetTemplatePayload(IApplication application, IApplicant applicant)
        {
            var applicantAddress = application.Applicants
                                        .FirstOrDefault(m => m.IsPrimary)
                                        .Addresses.FirstOrDefault(m => m.IsPrimary);

            return new
            {
                Email = applicant.Email,
                Name = $"#{applicant.FirstName} #{applicant.LastName}",
                // payload
                FirstName = applicant.FirstName,
                LastName = applicant.LastName,
                AddressLine1 = applicantAddress.Line1,
                City = applicantAddress.City,
                State = applicantAddress.State,
                Zipcode = applicantAddress.ZipCode,
                FICO = "XXX",
                Reasons = this.Reasons,
                KeyFactors = this.Reasons,
                Date = DateTime.Now.ToShortDateString(),
                loanNumber = this.ApplicationNumber
            };
        }
    }
}
