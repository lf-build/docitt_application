﻿using LendFoundry.Security.Tokens;

namespace Docitt.Application.Client
{
    public interface IApplicationServiceClientFactory
    {
        IApplicationService Create(ITokenReader reader);
    }
}
