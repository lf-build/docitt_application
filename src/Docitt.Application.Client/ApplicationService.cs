﻿using LendFoundry.Foundation.Client;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Application.Client
{
    public class ApplicationService : IApplicationService
    {
        public ApplicationService(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public IApplicationResponse Add(IApplicationRequest applicationRequest)
        {
            return Client.PutAsync<IApplicationRequest, ApplicationResponse>($"/", applicationRequest, true).Result;
        }

        public IApplicationResponse AddOrUpdateApplicants(string applicationNumber, List<IApplicantRequest> applicants)
        {
            return Client.PutAsync<List<IApplicantRequest>, ApplicationResponse>($"/{applicationNumber}/applicants", applicants, true).Result;
        }

        public void UpdateApplication(string applicationNumber, IApplicationUpdateRequest applicationRequest)
        {
             Client.PostAsync($"/{applicationNumber}", applicationRequest, true).Wait();
        }

        public void UpdateAddresses(string applicationNumber, string applicantId, List<IAddress> addresses)
        {
            Client.PostAsync($"/{applicationNumber}/{applicantId}/address", addresses, true).Wait();
        }

        public void UpdatePrimaryAddress(string applicationNumber, IAddress address)
        {
             Client.PutAsync<IAddress>($"/{applicationNumber}/address/primary", address, true).Wait();
        }

        public void UpdatePhoneNumbers(string applicationNumber, string applicantId, List<IPhoneNumber> phoneNumbers)
        {
            Client.PostAsync($"/{applicationNumber}/{applicantId}/phonenumber", phoneNumbers, true).Wait();
        }

        public void UpdatePrimaryPhoneNumber(string applicationNumber, string applicantId, IPhoneNumber primaryPhonenumber)
        {
            Client.PostAsync($"/{applicationNumber}/{applicantId}/phonenumber/primary", primaryPhonenumber, true).Wait();
        }

        public void UpdateApplicant(string applicationNumber, string applicantId, IApplicantUpdateRequest applicantUpdateRequest)
        {
            Client.PostAsync($"/{applicationNumber}/{applicantId}", applicantUpdateRequest, true).Wait();
        }

        public IApplicationResponse GetByApplicationNumber(string applicationNumber)
        {
            return Client.GetAsync<ApplicationResponse>($"/{applicationNumber}").Result;
        }

        public List<string> GetApplicationNumbersByApplicant(string applicantId)
        {
            return  Client.GetAsync<List<string>>($"/applicant/{applicantId}").Result;
        }

        public void UpdateScore(string applicationNumber, string scoreName, Score score)
        {
            Client.PostAsync($"/{applicationNumber}/{scoreName}/score", score, true).Wait();
        }

        public IBankAccount AddBank(string applicationNumber, IBankAccount bankAccount)
        {
          return Client.PutAsync<IBankAccount,BankAccount>($"/{applicationNumber}/banks", bankAccount, true).Result;
        }

        public IBankAccount UpdateBank(string applicationNumber,  string bankId, IBankAccount bankAccount)
        {
            return Client.PostAsync<IBankAccount,BankAccount>($"/{applicationNumber}/banks/{bankId}", bankAccount, true).Result;
        }

        public void DeleteBank(string applicationNumber,  string bankId)
        {
             Client.DeleteAsync($"/{applicationNumber}/banks/{bankId}").Wait();
        }

        public IEnumerable<IBankAccount> GetAllBanks(string applicationNumber)
        {
            return  Client.GetAsync<List<BankAccount>>($"/{applicationNumber}/banks").Result;
        }

        public IBankAccount GetBank(string applicationNumber, string bankId)
        {
            return  Client.GetAsync<BankAccount>($"/{applicationNumber}/banks/{bankId}").Result;
        }

        public void SetBankAsPrimary(string applicationNumber, string bankId)
        {
             Client.PostAsync<dynamic>($"/{applicationNumber}/banks/{bankId}/primary", null, true).Wait();
        }

        public void EnableAutoPayment(string applicationNumber)
        {
            Client.PostAsync<dynamic>($"/{applicationNumber}/auto-payment/enable", null, true).Wait();
        }

        public void DisableAutoPayment(string applicationNumber)
        {
            Client.PostAsync<dynamic>($"/{applicationNumber}/auto-payment/disable", null, true).Wait();
        }

        public void UpdateAnnualIncome(string applicationNumber, double annualIncome)
        {
            Client.PostAsync<dynamic>($"/{applicationNumber}/{annualIncome}/annual-income", null, true).Wait();
        }

        public bool IsDuplicate(string applicationNumber)
        {
            var response = Client.GetStringAsync($"/is-duplicate/{applicationNumber}").Result;
            return Convert.ToBoolean(response);
        }

        public IApplicationUnassigned GetUnassigned(string applicationNumber)
        {
            var request = new RestRequest("/{applicationNumber}/unassigned", Method.GET);
            request.AddUrlSegment("applicationNumber", applicationNumber);
            return Client.Execute<ApplicationUnassigned>(request);
            //return Client.GetAsync<ApplicationUnassigned>($"/{applicationNumber}/unassigned").Result;
        }
        
        public async Task<List<IApplication>> GetApplicationsByUserName(string userName) 
        {
            var result = await Client.GetAsync<List<Application>>($"/applications/by/user/{userName}");
            return result.ToList<IApplication>();
        }

        public async Task UpdateApplicantUserNameByApplicationNumber(string inviteId,string applicationNumber, string userName)
        {
             var request = new RestRequest($"/map/inviteid/{inviteId}/{userName}/temporaryapplicationnumber/{applicationNumber}", Method.PUT);
             await Client.ExecuteAsync(request);
        }
    }
}