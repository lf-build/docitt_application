﻿using LendFoundry.Security.Tokens;
using Microsoft.Extensions.DependencyInjection;

using System;
namespace Docitt.Application.Client
{
    public static class ApplicationServiceClientExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddApplicationService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IApplicationServiceClientFactory>(p => new ApplicationServiceServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IApplicationServiceClientFactory>().Create(p.GetService<ITokenReader>()));

            return services;
        }

        public static IServiceCollection AddApplicationService(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<IApplicationServiceClientFactory>(p => new ApplicationServiceServiceClientFactory(p, uri));
            services.AddTransient(p => p.GetService<IApplicationServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddApplicationService(this IServiceCollection services)
        {
            services.AddTransient<IApplicationServiceClientFactory>(p => new ApplicationServiceServiceClientFactory(p));
            services.AddTransient(p => p.GetService<IApplicationServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

       
    }
}
