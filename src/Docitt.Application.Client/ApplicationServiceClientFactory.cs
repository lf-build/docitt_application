﻿using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using System;
using Microsoft.Extensions.DependencyInjection;

namespace Docitt.Application.Client
{
    public class ApplicationServiceServiceClientFactory : IApplicationServiceClientFactory
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public ApplicationServiceServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }

        public ApplicationServiceServiceClientFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }
        private IServiceProvider Provider { get; set; }

        private Uri Uri { get; }


        public IApplicationService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("application");
            }


            var client = Provider.GetServiceClient(reader, uri);
            return new ApplicationService(client);
        }


       
    }
}
