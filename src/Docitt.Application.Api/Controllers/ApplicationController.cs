﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;

namespace Docitt.Application.Api.Controllers
{
    /// <summary>
    /// Represents application controller class.
    /// </summary>
    [Route("/")]
    public class ApplicationController : ExtendedController
    {
        /// <summary>
        /// Represents constructor class.
        /// </summary>
        /// <param name="service"></param>
        public ApplicationController(IApplicationService service)
        {
            Service = service;
        }

        /// <summary>
        /// Gets Application Service
        /// </summary>
        private IApplicationService Service { get; }

        private static NoContentResult NoContentResult { get; } = new NoContentResult();

        /// <summary>
        /// Add application
        /// </summary>
        /// <param name="applicationRequest">Application input payload</param>
        /// <returns>IApplicationResponse</returns>
        [HttpPut]
        [ProducesResponseType(typeof(IApplicationResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult Add([FromBody] ApplicationRequest applicationRequest)
        {
            return Execute(() => Ok(Service.Add(applicationRequest)));
        }

        /// <summary>
        /// Add or update applicant
        /// </summary>
        /// <param name="applicationNumber">applicationNumber</param>
        /// <param name="applicants">Applicants payload</param>
        /// <returns>IApplicationResponse</returns>
        [HttpPut("/{applicationNumber}/applicants")]
        [ProducesResponseType(typeof(IApplicationResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult AddOrUpdateApplicants(string applicationNumber,[FromBody] List<ApplicantRequest> applicants)
        {
            return Execute(() => Ok(Service.AddOrUpdateApplicants(applicationNumber,new List<IApplicantRequest>(applicants))));
        }

        /// <summary>
        /// UpdateApplication
        /// </summary>
        /// <param name="applicationNumber">applicationNumber</param>
        /// <param name="applicationRequest">applicationRequest</param>
        /// <returns>NoContentResult</returns>
        [HttpPost("/{applicationNumber}")]
        [ProducesResponseType(typeof(NoContentResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult UpdateApplication(string applicationNumber,
            [FromBody] ApplicationUpdateRequest applicationRequest)
        {
            return Execute(() =>
            {
                Service.UpdateApplication(applicationNumber, applicationRequest);
                return NoContentResult;
            });
        }

        /// <summary>
        /// UpdateApplicant
        /// </summary>
        /// <param name="applicationNumber">applicationNumber</param>
        /// <param name="applicantId">applicantId</param>
        /// <param name="applicationRequest">applicationRequest</param>
        /// <returns>NoContentResult</returns>
        [HttpPost("/{applicationNumber}/{applicantId}")]
        [ProducesResponseType(typeof(NoContentResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult UpdateApplicant(string applicationNumber, string applicantId,
            [FromBody] ApplicantUpdateRequest applicationRequest)
        {
            return Execute(() =>
            {
                Service.UpdateApplicant(applicationNumber, applicantId, applicationRequest);
                return NoContentResult;
            });
        }

        /// <summary>
        /// UpdateAddresses
        /// </summary>
        /// <param name="applicationNumber">applicationNumber</param>
        /// <param name="applicantId">applicantId</param>
        /// <param name="addresses">addresses</param>
        /// <returns>NoContentResult</returns>
        [HttpPost("/{applicationNumber}/{applicantId}/address")]
        [ProducesResponseType(typeof(NoContentResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult UpdateAddresses(string applicationNumber, string applicantId,
            [FromBody] List<Address> addresses)
        {
            return Execute(() =>
            {
                Service.UpdateAddresses(applicationNumber, applicantId, new List<IAddress>(addresses));
                return NoContentResult;
            });
        }

        /// <summary>
        /// UpdatePrimaryAddress
        /// </summary>
        /// <param name="applicationNumber">applicationNumber</param>
        /// <param name="address">address</param>
        /// <returns>NoContentResult</returns>
        [HttpPut("/{applicationNumber}/address/primary")]
        [ProducesResponseType(typeof(NoContentResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult UpdatePrimaryAddress(string applicationNumber, [FromBody] Address address)
        {
            return Execute(() =>
            {
                Service.UpdatePrimaryAddress(applicationNumber, address);
                return NoContentResult;
            });
        }

        /// <summary>
        /// UpdatePhoneNumbers
        /// </summary>
        /// <param name="applicationNumber">applicationNumber</param>
        /// <param name="applicantId">applicantId</param>
        /// <param name="phoneNumbers">phoneNumbers</param>
        /// <returns>NoContentResult</returns>
        [HttpPost("/{applicationNumber}/{applicantId}/phonenumber")]
        [ProducesResponseType(typeof(NoContentResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult UpdatePhoneNumbers(string applicationNumber, string applicantId,
            [FromBody] List<PhoneNumber> phoneNumbers)
        {
            return Execute(() =>
            {
                Service.UpdatePhoneNumbers(applicationNumber, applicantId, new List<IPhoneNumber>(phoneNumbers));
                return NoContentResult;
            });
        }

        /// <summary>
        /// UpdatePrimaryPhoneNumber
        /// </summary>
        /// <param name="applicationNumber">applicationNumber</param>
        /// <param name="applicantId">applicantId</param>
        /// <param name="primaryPhonenumber">primaryPhonenumber</param>
        /// <returns>NoContentResult</returns>
        [HttpPost("/{applicationNumber}/{applicantId}/phonenumber/primary")]
        [ProducesResponseType(typeof(NoContentResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult UpdatePrimaryPhoneNumber(string applicationNumber, string applicantId,
            [FromBody] PhoneNumber primaryPhonenumber)
        {
            return Execute(() =>
            {
                Service.UpdatePrimaryPhoneNumber(applicationNumber, applicantId, primaryPhonenumber);
                return NoContentResult;
            });
        }

        /// <summary>
        /// Get Application
        /// </summary>
        /// <param name="applicationNumber">applicationNumber</param>
        /// <returns>IApplicationResponse</returns>
        [HttpGet("/{applicationNumber}")]
        [ProducesResponseType(typeof(IApplicationResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult Get(string applicationNumber)
        {
            return Execute(() => Ok(Service.GetByApplicationNumber(applicationNumber)));
        }

        /// <summary>
        /// GetApplicationNumbersByApplicant
        /// </summary>
        /// <param name="applicantId">applicantId</param>
        /// <returns>string[]</returns>
        [HttpGet("/applicant/{applicantId}")]
        [ProducesResponseType(typeof(List<string>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetApplicationNumbersByApplicant(string applicantId)
        {
            return Execute(() => Ok(Service.GetApplicationNumbersByApplicant(applicantId)));
        }

        /// <summary>
        /// GetApplicationsByUserName
        /// </summary>
        /// <param name="userName">userName</param>
        /// <returns>aplications</returns>
        [HttpGet("/applications/by/user/{userName}")]
        [ProducesResponseType(typeof(List<Application>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> GetApplicationsByUserName(string userName)
        {
            return ExecuteAsync(async () => Ok(await Service.GetApplicationsByUserName(userName)));
        }


        /// <summary>
        /// UpdateApplicantUserNameByApplicationNumber
        /// </summary>
        /// <param name="inviteId">inviteId</param>
        /// <param name="applicationNumber">applicationNumber</param>
        /// <param name="userName">userName</param>
        /// <returns>aplications</returns>
        [HttpPut("/map/inviteid/{inviteId}/{userName}/temporaryapplicationnumber/{applicationNumber}")]
        [ProducesResponseType(typeof(List<Application>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> UpdateApplicantUserNameByApplicationNumber(string inviteId, string userName,
            string applicationNumber)
        {
            return ExecuteAsync(async () =>
            {
                await Service.UpdateApplicantUserNameByApplicationNumber(inviteId, applicationNumber, userName);
                return Ok();
            });
        }

        /// <summary>
        /// UpdateScore
        /// </summary>
        /// <param name="applicationNumber">applicationNumber</param>
        /// <param name="scoreName">scoreName</param>
        /// <param name="score">score</param>
        /// <returns>NoContentResult</returns>
        [HttpPost("/{applicationNumber}/{scoreName}/score")]
        [ProducesResponseType(typeof(NoContentResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult UpdateScore(string applicationNumber, string scoreName, [FromBody] Score score)
        {
            return Execute(() =>
            {
                Service.UpdateScore(applicationNumber, scoreName, score);
                return NoContentResult;
            });
        }

        /// <summary>
        /// AddBankAccount
        /// </summary>
        /// <param name="applicationNumber">applicationNumber</param>
        /// <param name="bankAccount">bankAccount</param>
        /// <returns>IBankAccount</returns>
        [HttpPost("/{applicationNumber}/banks")]
        [ProducesResponseType(typeof(IBankAccount), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult AddBankAccount(string applicationNumber, [FromBody] BankAccount bankAccount)
        {
            return Execute(() => Ok(Service.AddBank(applicationNumber, bankAccount)));
        }

        /// <summary>
        /// UpdateBankAccount
        /// </summary>
        /// <param name="applicationNumber">applicationNumber</param>
        /// <param name="bankId">bankId</param>
        /// <param name="bankAccount">bankAccount</param>
        /// <returns>IBankAccount</returns>
        [HttpPut("/{applicationNumber}/banks/{bankId}")]
        [ProducesResponseType(typeof(IBankAccount), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult UpdateBankAccount(string applicationNumber, string bankId,
            [FromBody] BankAccount bankAccount)
        {
            return Execute(() => Ok(Service.UpdateBank(applicationNumber, bankId, bankAccount)));
        }

        /// <summary>
        /// UpdateBankAccount
        /// </summary>
        /// <param name="applicationNumber">applicationNumber</param>
        /// <param name="bankId">bankId</param>
        /// <returns>NoContentResult</returns>
        [HttpDelete("/{applicationNumber}/banks/{bankId}")]
        [ProducesResponseType(typeof(NoContentResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult DeleteBankAccounts(string applicationNumber, string bankId)
        {
            return Execute(() =>
            {
                Service.DeleteBank(applicationNumber, bankId);
                return NoContentResult;
            });
        }

        /// <summary>
        /// GetAllBanks
        /// </summary>
        /// <param name="applicationNumber">applicationNumber</param>
        /// <returns>IBankAccount[]</returns>
        [HttpGet("/{applicationNumber}/banks")]
        [ProducesResponseType(typeof(IEnumerable<IBankAccount>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetAllBanks(string applicationNumber)
        {
            return Execute(() => Ok(Service.GetAllBanks(applicationNumber)));
        }

        /// <summary>
        /// GetBank
        /// </summary>
        /// <param name="applicationNumber">applicationNumber</param>
        /// <param name="bankId">bankId</param>
        /// <returns>IBankAccount</returns>
        [HttpGet("/{applicationNumber}/banks/{bankId}")]
        [ProducesResponseType(typeof(IBankAccount), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetBank(string applicationNumber, string bankId)
        {
            return Execute(() => Ok(Service.GetBank(applicationNumber, bankId)));
        }

        /// <summary>
        /// SetBankAsprimary
        /// </summary>
        /// <param name="applicationNumber">applicationNumber</param>
        /// <param name="bankId">bankId</param>
        /// <returns>IBankAccount</returns>
        [HttpPut("/{applicationNumber}/banks/{bankId}/primary")]
        [ProducesResponseType(typeof(NoContentResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult SetBankAsprimary(string applicationNumber, string bankId)
        {
            return Execute(() =>
            {
                Service.SetBankAsPrimary(applicationNumber, bankId);
                return NoContentResult;
            });
        }

        /// <summary>
        /// EnableAutoPayment
        /// </summary>
        /// <param name="applicationNumber">applicationNumber</param>
        /// <returns>IBankAccount</returns>
        [HttpPost("/{applicationNumber}/auto-payment/enable")]
        [ProducesResponseType(typeof(NoContentResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult EnableAutoPayment(string applicationNumber)
        {
            return Execute(() =>
            {
                Service.EnableAutoPayment(applicationNumber);
                return NoContentResult;
            });
        }

        /// <summary>
        /// DisableAutoPayment
        /// </summary>
        /// <param name="applicationNumber">applicationNumber</param>
        /// <returns>IBankAccount</returns>
        [HttpPost("/{applicationNumber}/auto-payment/disable")]
        [ProducesResponseType(typeof(NoContentResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult DisableAutoPayment(string applicationNumber)
        {
            return Execute(() =>
            {
                Service.DisableAutoPayment(applicationNumber);
                return NoContentResult;
            });
        }

        /// <summary>
        /// UpdateAnnualIncome
        /// </summary>
        /// <param name="applicationNumber">applicationNumber</param>
        /// <param name="annualIncome">annualIncome</param>
        /// <returns>IBankAccount</returns>
        [HttpPost("/{applicationNumber}/{annualIncome}/annual-income")]
        [ProducesResponseType(typeof(NoContentResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult UpdateAnnualIncome(string applicationNumber, double annualIncome)
        {
            return Execute(() =>
            {
                Service.UpdateAnnualIncome(applicationNumber, annualIncome);
                return NoContentResult;
            });
        }

        /// <summary>
        /// IsDuplicate
        /// </summary>
        /// <param name="applicationNumber">applicationNumber</param>
        /// <returns>bool</returns>
        [HttpGet("/is-duplicate/{applicationNumber}")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult IsDuplicate(string applicationNumber)
        {
            return Execute(() => Ok(Service.IsDuplicate(applicationNumber)));
        }

        /// <summary>
        /// GetUnassigned
        /// </summary>
        /// <param name="applicationNumber">applicationNumber</param>
        /// <returns>IApplicationUnassigned</returns>
        [HttpGet("/{applicationNumber}/unassigned")]
        [ProducesResponseType(typeof(IApplicationUnassigned), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetUnassigned(string applicationNumber)
        {
            return Execute(() => Ok(Service.GetUnassigned(applicationNumber)));
        }
    }
}