﻿using Docitt.Applicant;
using Docitt.Application.Persistence;
using LendFoundry.Configuration;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.NumberGenerator.Client;
using LendFoundry.StatusManagement.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.Email.Client;
using LendFoundry.Foundation.ServiceDependencyResolver;
using System.Collections.Generic;
using Docitt.Applicant.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace Docitt.Application.Api
{
    internal class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "DocittApplication"
                });
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme()
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
               {
                   { "Bearer", new string[] { } }
               });

                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "Docitt.Application.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();


            // services
            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddConfigurationService<Configuration>(Settings.ServiceName);
            services.AddEventHub(Settings.ServiceName);
            services.AddTenantService();
            services.AddApplicantService();
            services.AddNumberGeneratorService();
            services.AddEmailService();
            services.AddStatusManagementService();
            services.AddDependencyServiceUriResolver<Configuration>(Settings.ServiceName);

            // internals
            services.AddMongoConfiguration(Settings.ServiceName);
            services.AddTransient<IConfiguration>(provider => provider.GetRequiredService<IConfigurationService<Configuration>>().Get());
            services.AddTransient<IApplicationRepository, ApplicationRepository>();
            services.AddTransient<IApplicationRepositoryFactory, ApplicationRepositoryFactory>();
            services.AddTransient<IApplicationService, ApplicationService>();
            services.AddTransient<IApplicationListener, ApplicationListener>();

            // interface resolver
            services.AddMvc().AddLendFoundryJsonOptions((options) =>
            {
                options
                    .AddInterfaceConverter<IApplicantRequest, ApplicantRequest>()
                    .AddInterfaceConverter<IIdentity, Identity>()
                    .AddInterfaceConverter<IAddress, Address>()
                    .AddInterfaceConverter<IPhoneNumber, PhoneNumber>()
                    .AddInterfaceConverter<IEmployer, Employer>()
                    .AddInterfaceConverter<IBankAccount, BankAccount>()
                    .AddInterfaceConverter<IScoreRange, ScoreRange>();
            });

            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
		    app.UseCors(env);

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "DOCITT Applicaion Service");
            });

            
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();
            app.UseApplicationListener();
           

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            // Enable middleware to serve swagger-ui (HTML, JS, CSS etc.), specifying the Swagger JSON endpoint.

            app.UseConfigurationCacheDependency();
        }
    }
}
