﻿using LendFoundry.Configuration;
using LendFoundry.Email.Client;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using System;
using System.Linq;

using Docitt.Applicant.Client;

namespace Docitt.Application
{
    public class ApplicationDeclinedEventSubscription : IEventSubscription<ApplicationDeclined>
    {
        private IApplicationRepositoryFactory ApplicationRepositoryFactory { get; }

        private IApplicantServiceFactory ApplicantServiceFactory { get; }

        private ILoggerFactory LoggerFactory { get; }

        private IEmailServiceFactory EmailServiceFactory { get; }

        private IConfigurationServiceFactory<Configuration> ConfigurationServiceFactory { get; }

        private ITokenReaderFactory TokenHandlerFactory { get; }

        public ApplicationDeclinedEventSubscription(
            IApplicationRepositoryFactory applicationRepositoryFactory,
            IApplicantServiceFactory applicantServiceFactory,
            ILoggerFactory loggerFactory,
            IEmailServiceFactory emailServiceFactory,
            IConfigurationServiceFactory<Configuration> configurationServiceFactory,
            ITokenReaderFactory tokenHandlerFactory)
        {
            ApplicationRepositoryFactory = applicationRepositoryFactory;
            LoggerFactory = loggerFactory;
            EmailServiceFactory = emailServiceFactory;
            ConfigurationServiceFactory = configurationServiceFactory;
            ApplicantServiceFactory = applicantServiceFactory;
            TokenHandlerFactory = tokenHandlerFactory;
        }

        public async void Handle(IEventInfo @event, ApplicationDeclined applicationDeclined)
        {
            var reader = TokenHandlerFactory.Create(@event.TenantId, string.Empty);
            var applicationRepository = ApplicationRepositoryFactory.Create(reader);
            var configurationService = ConfigurationServiceFactory.Create(reader);
            var emailService = EmailServiceFactory.Create(reader);
            var applicantService = ApplicantServiceFactory.Create(reader);
            var loggerService = LoggerFactory.Create(NullLogContext.Instance);

            var application = applicationRepository.GetByApplicationNumber(applicationDeclined.ApplicationNumber);

            try
            {
                if (application != null)
                {
                    var applicantData = application.Applicants.FirstOrDefault(m => m.IsPrimary);
                    if (applicantData == null) return;
                    var applicant = applicantService.Get(applicantData.ApplicantId);

                    var notification = configurationService.Get().Notification;
                    await emailService.Send("application",applicationDeclined.ApplicationNumber,notification.Template,  
                        applicationDeclined.GetTemplatePayload(application, applicant));

                    loggerService.Info($"Applicant #{applicant.FirstName} was notified on #{applicant.Email}  about "
                                       + "the declined application #{application.ApplicationNumber}");
                }
                else
                {
                    loggerService.Warn($"Application #{applicationDeclined.ApplicationNumber} could "
                        + "not be found while processing subscription #{@event.Name}");
                }

            }
            catch (Exception e)
            {
                loggerService.Error("Uncaught exception", e);
            }

        }
    }
}
