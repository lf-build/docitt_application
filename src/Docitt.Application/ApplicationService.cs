﻿using Docitt.Applicant;
using Docitt.Application.Events;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.NumberGenerator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Application
{
    public class ApplicationService : IApplicationService
    {
        public ApplicationService
        (
            IApplicantService applicantService,
            IApplicationRepository repository,
            IGeneratorService applicationNumberGenerator,
            ILogger logger,
            IEventHubClient eventHubClient,
            ITenantTime tenantTime,
            IConfiguration configuration
        )
        {
            TenantTime = tenantTime;
            ApplicantService = applicantService;
            Repository = repository;
            ApplicationNumberGenerator = applicationNumberGenerator;
            EventHubClient = eventHubClient;
            Configuration = configuration ?? throw new ArgumentException("Application configuration cannot be found, please check");
            CommandExecutor = new CommandExecutor(logger);
            Configuration = configuration;
        }

        private ITenantTime TenantTime { get; }

        private IApplicantService ApplicantService { get; }

        private IApplicationRepository Repository { get; }

        private IGeneratorService ApplicationNumberGenerator { get; }

        private IEventHubClient EventHubClient { get; }

        private IConfiguration Configuration { get; }

        private CommandExecutor CommandExecutor { get; }

        public IApplicationResponse Add(IApplicationRequest applicationRequest)
        {
            EnsureInputIsValid(applicationRequest);
            Application application = null;

            var addApplicantsCommand = applicationRequest.Applicants.Select
            (
                applicant => new Command
                (
                    execute: () =>
                    {
                        var applicantResponse = ApplicantService.Add(applicant);
                        applicant.ApplicantId = applicantResponse.Id;
                    },
                    rollback: () => ApplicantService.Delete(applicant.Id)
                )
            ).ToList();

            var addApplicationCommand = new Command
            (
                execute: () =>
                {
                    if (applicationRequest.BankAccounts != null)
                    {
                        EnsureBankAccountIsValid(applicationRequest.BankAccounts);
                        SetBankAccountId(applicationRequest.BankAccounts);
                        EnsureNoDuplicatedBankAccount(applicationRequest.BankAccounts);

                        // if not informed first bank must to be assumed as primary
                        if (applicationRequest.BankAccounts.TrueForAll(b => b.IsPrimary == false))
                            applicationRequest.BankAccounts.First().IsPrimary = true;
                    }
                    else
                        applicationRequest.BankAccounts = new List<IBankAccount>();

                    application = new Application(applicationRequest)
                    {
                        ApplicationNumber = String.IsNullOrEmpty(applicationRequest.ApplicationNumber) ?ApplicationNumberGenerator.TakeNext("application").Result:applicationRequest.ApplicationNumber
                    };

                    if (string.IsNullOrWhiteSpace(application.ApplicationNumber))
                        throw new Exception("Unable to generate application number");

                    application.SubmittedDate = new TimeBucket(TenantTime.Now);
                    if (Configuration.Expiration != null)
                        application.ExpirationDate = new TimeBucket(TenantTime.Today.AddDays(Configuration.Expiration.DaysToExpire));

                    Repository.Add(application);
                },
                rollback: () => Repository.Remove(application)
            );

            CommandExecutor.Execute(new List<Command>(addApplicantsCommand) { addApplicationCommand });
            EventHubClient.Publish(new ApplicationCreated() { Application = application }).Wait();
            return new ApplicationResponse(application, new List<IApplicantRequest>(applicationRequest.Applicants));
        }

        public IApplicationResponse AddOrUpdateApplicants(string applicationNumber,List<IApplicantRequest>  applicants)
        {
            EnsureApplicantsAreValid(applicants);
            var application=GetApplicationByApplicationNumber(applicationNumber);
            
            if (application == null)
                throw new NotFoundException($"Application ${applicationNumber} not found");
            
            applicants.ForEach((applicantRequest) =>
            {
                if (!string.IsNullOrWhiteSpace(applicantRequest.ApplicantId))
                {
                    if (!application.Applicants.Exists(a=>a.ApplicantId==applicantRequest.ApplicantId))
                        throw new NotFoundException($"Application ${applicationNumber} not contain applicant ${applicantRequest.ApplicantId}");
                    
                    var applicant = ApplicantService.Get(applicantRequest.ApplicantId);
                    if(applicant==null)
                            throw new NotFoundException($"Application ${applicationNumber} not contain applicant ${applicantRequest.ApplicantId}");
                    
                    applicantRequest.UserIdentity = applicant.UserIdentity;
                }
            });
            
            applicants.ForEach((applicant) =>
            {
                if (string.IsNullOrWhiteSpace(applicant.ApplicantId))
                {
                    var applicantResponse = ApplicantService.Add(applicant);
                    applicant.ApplicantId = applicantResponse.Id;
                }
                else
                {
                    ApplicantService.Update(applicant.ApplicantId, new Applicant.Applicant()
                    {
                        DateOfBirth = applicant.DateOfBirth,
                        Email = applicant.Email,
                        FirstName = applicant.FirstName,
                        GenerationOrSuffix = applicant.GenerationOrSuffix,
                        Id = applicant.Id,
                        Identities = applicant.Identities,
                        LastName = applicant.LastName,
                        MiddleName = applicant.MiddleName,
                        Salutation = applicant.Salutation,
                        Ssn = applicant.Ssn,
                        TenantId = applicant.TenantId,
                        UserIdentity = applicant.UserIdentity
                    });
                }
            });

            var applicantsToAddInApplication = new List<IApplicantData>();
            applicantsToAddInApplication.AddRange(application.Applicants.Where(a =>
                applicants.Exists(app => app.ApplicantId == a.ApplicantId) == false));
            applicantsToAddInApplication.AddRange(new List<IApplicantData>(applicants.Select(applicant => new ApplicantData(applicant))));
            
            application=GetApplicationByApplicationNumber(applicationNumber);
            application.Applicants = applicantsToAddInApplication;
            Repository.Update(application);
            EventHubClient.Publish(new ApplicationModified() { Application = application }).Wait();

            return new ApplicationResponse(application, new List<IApplicantRequest>(applicants));

        }
        
        public void UpdateApplication(string applicationNumber, IApplicationUpdateRequest applicationRequest)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(applicationNumber));

            if (applicationRequest == null)
                throw new ArgumentNullException(nameof(applicationRequest));

            var application = GetApplicationByApplicationNumber(applicationNumber);
            if (application == null)
                throw new NotFoundException($"Application {applicationNumber} not found");

            Repository.UpdateApplication(applicationNumber, applicationRequest);
            application.Amount = applicationRequest.Amount;
            application.Purpose = applicationRequest.Purpose;
            application.ScoreRange = applicationRequest.ScoreRange;
            application.Source = applicationRequest.Source;
            applicationRequest.Channel = applicationRequest.Channel;
            EventHubClient.Publish(new ApplicationModified() { Application = application }).Wait();
        }

        public void UpdateAddresses(string applicationNumber, string applicantId, List<IAddress> addresses)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(applicationNumber));

            if (string.IsNullOrWhiteSpace(applicantId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(applicantId));

            UpdateAddress(applicationNumber, applicantId, addresses);
        }

        public void UpdatePrimaryAddress(string applicationNumber, IAddress address)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(applicationNumber));

            if (address != null)
            {
                //validations for required fields in primary address
                if (string.IsNullOrWhiteSpace(address.Line1))
                    throw new ArgumentException("Value cannot be null or whitespace.", nameof(address.Line1));

                if (string.IsNullOrWhiteSpace(address.City))
                    throw new ArgumentException("Value cannot be null or whitespace.", nameof(address.City));

                if (string.IsNullOrWhiteSpace(address.State))
                    throw new ArgumentException("Value cannot be null or whitespace.", nameof(address.State));

                if (string.IsNullOrWhiteSpace(address.ZipCode))
                    throw new ArgumentException("Value cannot be null or whitespace.", nameof(address.ZipCode));

                address.IsPrimary = true;

                UpdateAddress(applicationNumber, null, new List<IAddress> { address });
            }
            else
                throw new ArgumentNullException(nameof(address));
        }

        private void UpdateAddress(string applicationNumber, string applicantId, List<IAddress> addresses)
        {
            var application = GetApplicationByApplicationNumber(applicationNumber);
            var applicants = application.Applicants;
            if (applicants == null || !applicants.Any())
                throw new NotFoundException($"Applicants not found for application {applicationNumber} ");

            IApplicantData applicantToUpdate = string.IsNullOrWhiteSpace(applicantId) ?
                applicants.FirstOrDefault(applicant => applicant.IsPrimary) :
                applicants.FirstOrDefault(applicant => applicant.ApplicantId == applicantId);

            if (applicantToUpdate == null)
                throw new NotFoundException($"Applicant not found for application {applicationNumber} ");

            var newAddresses = new List<IAddress>(addresses);
            if (string.IsNullOrWhiteSpace(applicantId)) // It's used by UpdatePrimaryAddress(applicationNumber, address) method
            {
                var addressesToUpdate = applicantToUpdate.Addresses ?? new List<IAddress>();
                if (addressesToUpdate.Any())
                {
                    newAddresses = addressesToUpdate.Select(address =>
                    {
                        if (address.IsPrimary)
                        {
                            return addresses.FirstOrDefault(a => a.IsPrimary);
                        }
                        return address;
                    }).ToList();
                }
            }

            var applicantAddressModified = new ApplicantAddressModified
            {
                ApplicationNumber = applicationNumber,
                ApplicantId = applicantToUpdate.ApplicantId,
                OldAddresses = applicantToUpdate.Addresses,
                NewAddresses = newAddresses
            };

            application = Repository.UpdateAddresses(applicationNumber, applicantToUpdate.ApplicantId, addresses);

            EventHubClient.Publish(applicantAddressModified).Wait();
            EventHubClient.Publish(new ApplicationModified { Application = application }).Wait();
        }

        public void UpdatePhoneNumbers(string applicationNumber, string applicantId, List<IPhoneNumber> phoneNumbers)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(applicationNumber));

            if (string.IsNullOrWhiteSpace(applicantId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(applicantId));

            UpdatePhoneNumbers(applicationNumber, applicantId, phoneNumbers, false);
        }

        public void UpdatePrimaryPhoneNumber(string applicationNumber, string applicantId, IPhoneNumber primaryPhoneNumber)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(applicationNumber));

            if (string.IsNullOrWhiteSpace(applicantId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(applicantId));

            // number and type are also mandatory
            if (primaryPhoneNumber != null)
            {
                if (string.IsNullOrWhiteSpace(primaryPhoneNumber.Number))
                    throw new ArgumentException("Value cannot be null or whitespace.", nameof(primaryPhoneNumber.Number));

                if (string.IsNullOrWhiteSpace(primaryPhoneNumber.Type))
                    throw new ArgumentException("Value cannot be null or whitespace.", nameof(primaryPhoneNumber.Type));

                primaryPhoneNumber.IsPrimary = true;

                UpdatePhoneNumbers(applicationNumber, applicantId, new List<IPhoneNumber> { primaryPhoneNumber }, true);
            }
            else
                throw new ArgumentNullException(nameof(primaryPhoneNumber.Number));
        }

        private void UpdatePhoneNumbers(string applicationNumber, string applicantId, List<IPhoneNumber> phoneNumbers, bool isPrimary)
        {
            var application = GetApplicationByApplicationNumber(applicationNumber);
            var applicants = application.Applicants;
            if (applicants == null || !applicants.Any())
                throw new NotFoundException($"Applicants not found for application {applicationNumber} ");

            var applicantToUpdate = applicants.FirstOrDefault(applicant => applicant.ApplicantId == applicantId);
            if (applicantToUpdate == null)
                throw new NotFoundException($"Applicant {applicantId} not found for application {applicationNumber} ");

            var newPhoneNumbers = new List<IPhoneNumber>(phoneNumbers);
            if (isPrimary)
            {
                var phoneNumbersToUpdate = applicantToUpdate.PhoneNumbers ?? new List<IPhoneNumber>();
                if (phoneNumbersToUpdate.Any())
                {
                    newPhoneNumbers = phoneNumbersToUpdate.Select(phoneNumber =>
                    {
                        if (phoneNumber.IsPrimary)
                        {
                            return phoneNumbers.FirstOrDefault(a => a.IsPrimary);
                        }
                        return phoneNumber;
                    }).ToList();
                }
            }

            var applicantPhoneModified = new ApplicantPhoneModified
            {
                ApplicationNumber = applicationNumber,
                ApplicantId = applicantToUpdate.ApplicantId,
                OldPhoneNumbers = applicantToUpdate.PhoneNumbers,
                NewPhoneNumbers = newPhoneNumbers
            };

            application = Repository.UpdatePhoneNumbers(applicationNumber, applicantId, phoneNumbers);

            EventHubClient.Publish(applicantPhoneModified);
            EventHubClient.Publish(new ApplicationModified() { Application = application });
        }

        public void UpdateApplicant(string applicationNumber, string applicantId, IApplicantUpdateRequest applicantUpdateRequest)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(applicationNumber));
            if (string.IsNullOrWhiteSpace(applicantId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(applicantId));

            var application = Repository.UpdateApplicant(applicationNumber, applicantId, applicantUpdateRequest);

            EventHubClient.Publish(new ApplicationModified() { Application = application });
        }

        public IApplicationResponse GetByApplicationNumber(string applicationNumber)
        {
            var application = GetApplicationByApplicationNumber(applicationNumber);

            var applicants = new List<IApplicantRequest>();
            foreach (var applicantData in application.Applicants)
            {
                var applicant = ApplicantService.Get(applicantData.ApplicantId);
                if (applicant == null)
                {
                    throw new NotFoundException("Applicant not found");
                }

                applicants.Add(new ApplicantRequest(applicantData, applicant));
            }

            return new ApplicationResponse(application, applicants);
        }

        public List<string> GetApplicationNumbersByApplicant(string applicantId)
        {
            if (string.IsNullOrWhiteSpace(applicantId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(applicantId));

            var applicationNumbers = Repository.GetApplicationNumbersByApplicantNumber(applicantId);
            if (applicationNumbers == null || !applicationNumbers.Any())
                throw new NotFoundException("No Application found");
            return applicationNumbers;
        }

        public async Task<List<IApplication>> GetApplicationsByUserName(string userName) 
        {
            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(userName));

            var applicants = await ApplicantService.GetApplicantsByUserName(userName);

            var applications = new List<IApplication>();
            foreach (var applicant in applicants)
            {
                var applicationDetails = await Repository.GetApplicationByApplicantId(applicant.Id);
                if (applicationDetails != null)
                {
                    applications.Add(applicationDetails);
                }
            }

            return applications;
        }

        public async Task UpdateApplicantUserNameByApplicationNumber(string oldUserName,string applicationNumber, string userName)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentException("Argument is null or whitespace", nameof(applicationNumber));

            var application = Repository.GetByApplicationNumber(applicationNumber);
            if (application == null)
                throw new NotFoundException($"Application {applicationNumber} not found");

            var applicantIds = application.Applicants.Select(x => x.ApplicantId).ToArray();
            var applicant = ApplicantService.GetByUserId(oldUserName);
            if (applicant == null)
                throw new NotFoundException($"User not found for the {oldUserName}");

            if (!applicantIds.Contains(applicant.Id))
                throw new NotFoundException($"Applicant {applicant.Id} not found in application {applicationNumber}");

            ApplicantService.LinkToUser(applicant.Id, userName);

            await EventHubClient.Publish(new ApplicationModified() {Application = application});
        }


        private IApplication GetApplicationByApplicationNumber(string applicationNumber)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentException("Argument is null or whitespace", nameof(applicationNumber));

            var application = Repository.GetByApplicationNumber(applicationNumber);
            if (application == null)
                throw new NotFoundException($"Application {applicationNumber} not found");

            return application;
        }

        public void UpdateScore(string applicationNumber, string scoreName, Score score)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new InvalidArgumentException($"{nameof(applicationNumber)} is mandatory");

            if (string.IsNullOrWhiteSpace(scoreName))
                throw new InvalidArgumentException($"{nameof(scoreName)} is mandatory");

            if (score == null)
                throw new InvalidArgumentException($"{nameof(score)} is mandatory");

            if (score.Date == null)
                throw new InvalidArgumentException($"{nameof(score.Date)} is mandatory");

            if (string.IsNullOrWhiteSpace(score.Value))
                throw new InvalidArgumentException($"{nameof(score.Value)} is mandatory");

            score.Name = scoreName;
            var application = Repository.UpdateScore(applicationNumber, scoreName, score);
            EventHubClient.Publish(new ApplicationModified() { Application = application });
        }

        public IBankAccount AddBank(string applicationNumber, IBankAccount bankAccount)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentNullException(nameof(applicationNumber));

            if (bankAccount == null)
                throw new ArgumentNullException(nameof(bankAccount));

            var optedInAutoPayment = false;
            var tmpBankAccounts = new[] { bankAccount }.ToList();
            EnsureBankAccountIsValid(tmpBankAccounts);
            SetBankAccountId(tmpBankAccounts);

            var applicant = GetApplicationByApplicationNumber(applicationNumber);
            var bankAccounts = applicant.BankAccounts ?? new List<IBankAccount>();
            if (bankAccounts.Any(b => b.Id == bankAccount.Id))
                throw new InvalidOperationException($"Bank AccountNumber {bankAccount.AccountNumber} and RoutingNumber {bankAccount.RoutingNumber} already exists");

            if (bankAccount.IsPrimary)
                bankAccounts.ForEach(b => { b.IsPrimary = false; });

            if (bankAccount.IsPrimary && bankAccount.AutoPayment)
            {
                bankAccounts.ForEach(b => { b.AutoPayment = false; });
                optedInAutoPayment = true;
            }

            bankAccounts.Add(bankAccount);

            EnsurePrimaryBankAccount(bankAccounts);

            var application = Repository.UpdateBankAccounts(applicationNumber, bankAccounts);

            EventHubClient.Publish(new ApplicationModified() { Application = application });


            if (optedInAutoPayment)
                PublishOptedInForAutoPayment(application.ApplicationNumber);


            return bankAccount;
        }

        public IBankAccount UpdateBank(string applicationNumber, string bankId, IBankAccount bankAccount)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentNullException(nameof(applicationNumber));

            if (string.IsNullOrWhiteSpace(bankId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(bankId));

            var optedInAutoPayment = false;
            var tmpBankAccounts = new[] { bankAccount }.ToList();
            EnsureBankAccountIsValid(tmpBankAccounts);
            SetBankAccountId(tmpBankAccounts);

            // stop process in case of not found bank to update
            var applicant = GetApplicationByApplicationNumber(applicationNumber);
            var bankAccounts = applicant.BankAccounts ?? new List<IBankAccount>();
            if (bankAccounts.All(b => b.Id != bankId))
                throw new NotFoundException($"Bank Account {bankId} could not be found");

            // set this bank as primary if informed
            if (bankAccount.IsPrimary)
                bankAccounts.ForEach(b => { b.IsPrimary = false; });

            if (bankAccount.IsPrimary && bankAccount.AutoPayment)
            {
                bankAccounts.ForEach(b => { b.AutoPayment = false; });
                optedInAutoPayment = true;
            }

            // update internal bank reference
            bankAccounts = bankAccounts.Select(bank => bank.Id == bankId ? bankAccount : bank).ToList();

            // ensure after performed update there's now invalid entries
            if (bankAccounts.Count(b => b.Id == bankAccount.Id) != 1)
                throw new InvalidOperationException($"Bank AccountNumber {bankAccount.AccountNumber}, RoutingNumber {bankAccount.RoutingNumber} already exists");

            EnsurePrimaryBankAccount(bankAccounts);

            var application = Repository.UpdateBankAccounts(applicationNumber, bankAccounts);
            EventHubClient.Publish(new ApplicationModified() { Application = application });

            if (optedInAutoPayment)
                PublishOptedInForAutoPayment(application.ApplicationNumber);

            return bankAccount;
        }

        public void DeleteBank(string applicationNumber, string bankId)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentNullException(nameof(applicationNumber));

            if (string.IsNullOrWhiteSpace(bankId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(bankId));

            // stop process in case of not found bank to update
            var applicant = GetApplicationByApplicationNumber(applicationNumber);
            var bankAccounts = applicant.BankAccounts ?? new List<IBankAccount>();
            if (!bankAccounts.Any(b => b.Id == bankId))
                throw new NotFoundException($"Bank Account id {bankId} could not be found");

            // primary bank account cannot be deleted
            if (bankAccounts.First(b => b.Id == bankId).IsPrimary)
                throw new InvalidOperationException("Primary bank account cannot be deleted");

            // remove from list informed bank account
            bankAccounts = bankAccounts.Where(b => b.Id != bankId).ToList();

            EnsurePrimaryBankAccount(bankAccounts);
            var application = Repository.UpdateBankAccounts(applicationNumber, bankAccounts);
            EventHubClient.Publish(new ApplicationModified() { Application = application });
        }

        public IEnumerable<IBankAccount> GetAllBanks(string applicationNumber)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentNullException(nameof(applicationNumber));

            // stop process in case of not found bank to update
            var applicant = GetApplicationByApplicationNumber(applicationNumber);
            return applicant.BankAccounts ?? new List<IBankAccount>();
        }

        public IBankAccount GetBank(string applicationNumber, string bankId)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentNullException(nameof(applicationNumber));

            if (string.IsNullOrWhiteSpace(bankId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(bankId));

            // stop process in case of not found bank to update
            var applicant = GetApplicationByApplicationNumber(applicationNumber);
            var bankAccounts = applicant.BankAccounts ?? new List<IBankAccount>();
            var bankAccount = bankAccounts.FirstOrDefault(b => b.Id == bankId);
            if (bankAccount == null)
                throw new NotFoundException($"Bank account {bankId} could not be found");
            return bankAccount;
        }

        public void EnableAutoPayment(string applicationNumber)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new InvalidArgumentException(nameof(applicationNumber));

            var application = GetApplicationByApplicationNumber(applicationNumber);
            var bankAccounts = application.BankAccounts ?? new List<IBankAccount>();
            var primaryBankAccount = bankAccounts.FirstOrDefault(b => b.IsPrimary);
            if (primaryBankAccount == null)
                throw new NotFoundException($"The Primary Bank account could not be found");

            primaryBankAccount.AutoPayment = true;

            Repository.UpdateBankAccounts(applicationNumber, bankAccounts);
            PublishOptedInForAutoPayment(applicationNumber);
        }

        public void DisableAutoPayment(string applicationNumber)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new InvalidArgumentException(nameof(applicationNumber));

            var application = GetApplicationByApplicationNumber(applicationNumber);
            var bankAccounts = application.BankAccounts ?? new List<IBankAccount>();
            var primaryBankAccount = bankAccounts.FirstOrDefault(b => b.IsPrimary);
            if (primaryBankAccount == null)
                throw new NotFoundException($"The Primary Bank account could not be found");

            primaryBankAccount.AutoPayment = false;

            Repository.UpdateBankAccounts(applicationNumber, bankAccounts);
            PublishOptedInForAutoPayment(applicationNumber);
        }

        public void SetBankAsPrimary(string applicationNumber, string bankId)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentNullException(nameof(applicationNumber));

            if (string.IsNullOrWhiteSpace(bankId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(bankId));

            // stop process in case of not found bank to update
            var applicant = GetApplicationByApplicationNumber(applicationNumber);
            var bankAccounts = applicant.BankAccounts ?? new List<IBankAccount>();
            if (!bankAccounts.Any(b => b.Id == bankId))
                throw new NotFoundException($"Bank Account {bankId} could not be found");

            // update internal bank reference
            bankAccounts = bankAccounts.Select(bank =>
            {
                bank.IsPrimary = bank.Id == bankId;
                return bank;
            }).ToList();

            var application = Repository.UpdateBankAccounts(applicationNumber, bankAccounts);

            EventHubClient.Publish(new ApplicationModified() { Application = application });
        }

        private void EnsureInputIsValid(IApplicationRequest applicationRequest)
        {
            if (applicationRequest == null)
                throw new InvalidArgumentException("application cannot be empty");
/*
            if (applicationRequest.Amount <= 0)
                throw new InvalidArgumentException($"{nameof(applicationRequest.Amount)} is mandatory");
                */

            if (string.IsNullOrWhiteSpace(applicationRequest.Purpose))
                throw new InvalidArgumentException($"{nameof(applicationRequest.Purpose)} is mandatory");

            //------ SOURCE
            if (applicationRequest.Source == null)
                throw new InvalidArgumentException($"{nameof(applicationRequest.Source)} is mandatory");

            if (applicationRequest.Source.Type == SourceType.Undefined)
                throw new InvalidArgumentException($"Source {nameof(applicationRequest.Source.Type)} is not valid", GetEnumNames<SourceType>());

            if (string.IsNullOrWhiteSpace(applicationRequest.Source.Id))
                throw new InvalidArgumentException($"Source {nameof(applicationRequest.Source.Id)} is mandatory");

            //------ APPLICANTS
            if (applicationRequest.Applicants == null || !applicationRequest.Applicants.Any())
                throw new InvalidArgumentException("At least one applicant must to be informed");

            //------ BANK ACCOUNTS
            applicationRequest.BankAccounts?.ForEach(bank =>
            {
                if (string.IsNullOrWhiteSpace(bank.AccountNumber))
                    throw new InvalidArgumentException($"Applicant {nameof(bank.AccountNumber)} is mandatory");

                if (string.IsNullOrWhiteSpace(bank.RoutingNumber))
                    throw new InvalidArgumentException($"Applicant{nameof(bank.RoutingNumber)} is mandatory");

                if (bank.AccountType == BankAccountType.Undefined)
                    throw new InvalidArgumentException($"Applicant{nameof(bank.AccountType)} is not valid", GetEnumNames<BankAccountType>());
            });

            EnsureApplicantsAreValid(applicationRequest.Applicants);

        }

        private void EnsureApplicantsAreValid(List<IApplicantRequest> applicants)
        {
            applicants.ForEach(applicant =>
            {
                if (string.IsNullOrWhiteSpace(applicant.FirstName))
                    throw new InvalidArgumentException($"Applicant {nameof(applicant.FirstName)} is mandatory");

                if (string.IsNullOrWhiteSpace(applicant.LastName))
                    throw new InvalidArgumentException($"Applicant {nameof(applicant.LastName)} is mandatory");

                //// For non signup user , alllow ssn to be null
                // if (string.IsNullOrWhiteSpace(applicant.Ssn))
                //     throw new InvalidArgumentException($"Applicant {nameof(applicant.Ssn)} is mandatory");

                //------ ADDRESS

                if (applicant.Addresses.Any())
                {

                    applicant.Addresses.ForEach(address =>
                    {
                        if (address != null)
                        {
                            /*if (string.IsNullOrWhiteSpace(address.Line1))
                                throw new InvalidArgumentException($"Applicant address {nameof(address.Line1)} is mandatory");*/

                            if (string.IsNullOrWhiteSpace(address.City))
                                throw new InvalidArgumentException($"Applicant address {nameof(address.City)} is mandatory");

                            /*if (string.IsNullOrWhiteSpace(address.State))
                                throw new InvalidArgumentException($"Applicant address {nameof(address.State)} is mandatory");*/

                            if (string.IsNullOrWhiteSpace(address.ZipCode))
                                throw new InvalidArgumentException($"Applicant address {nameof(address.ZipCode)} is mandatory");

                            if (string.IsNullOrWhiteSpace(address.Country))
                                address.Country = "USA";

                            if (address.Type == AddressType.Undefined)
                                throw new InvalidArgumentException($"Applicant address {nameof(address.Type)} is not valid", GetEnumNames<AddressType>());
                        }
                    });
                }

                //// For non signup user , alllow phonenumbers to be null or empty
                //------ PHONE NUMBERS
                // if (applicant.PhoneNumbers == null || !applicant.PhoneNumbers.Any())
                //     throw new InvalidArgumentException($"Applicant {nameof(applicant.PhoneNumbers)} is mandatory");
                // if(applicant.PhoneNumbers != null)
                // {
                //     applicant.PhoneNumbers.ForEach(phone =>
                //     {
                //         if (string.IsNullOrWhiteSpace(phone.Number))
                //             throw new InvalidArgumentException($"Applicant phone {nameof(phone.Number)} is mandatory");

                //         if (string.IsNullOrWhiteSpace(phone.Type))
                //             throw new InvalidArgumentException($"Applicant phone {nameof(phone.Type)} is mandatory");
                //     });
                // }

                if (string.IsNullOrWhiteSpace(applicant.Email))
                    throw new InvalidArgumentException($"Applicant {nameof(applicant.Email)} is mandatory");

                //if (string.IsNullOrWhiteSpace(applicant.Password))
                //    throw new InvalidArgumentException($"Applicant {nameof(applicant.Password)} is mandatory");

                /*
                if (applicant.StatedGrossAnnualIncome <= 0)
                    throw new InvalidArgumentException($"Applicant {nameof(applicant.StatedGrossAnnualIncome)} is mandatory");

                /*if (applicant.HomeOwnership == HomeOwnershipType.Undefined)
                    throw new InvalidArgumentException($"Applicant {nameof(applicant.HomeOwnership)} is not valid", GetEnumNames<HomeOwnershipType>());
                */
            });
        }

        private string GetEnumNames<T>() => $"Supported values => {EnumExtensions.GetEnumListStringNames<T>().Replace("Undefined,", "").Trim()}";

        private void EnsureBankAccountIsValid(List<IBankAccount> bankAccounts)
        {
            if (bankAccounts == null || bankAccounts.Any() == false)
                throw new InvalidArgumentException("At least one bank account must to be informed");

            bankAccounts.ForEach(bankAccount =>
            {
                if (string.IsNullOrWhiteSpace(bankAccount.AccountNumber))
                    throw new InvalidArgumentException($"{nameof(bankAccount.AccountNumber)} is mandatory");

                if (string.IsNullOrWhiteSpace(bankAccount.RoutingNumber))
                    throw new InvalidArgumentException($"{nameof(bankAccount.RoutingNumber)} is mandatory");

                if (bankAccount.AccountType == BankAccountType.Undefined)
                    throw new InvalidArgumentException($"Applicant {nameof(bankAccount.AccountType)} is not valid", GetEnumNames<BankAccountType>());

                if (!Enum.IsDefined(typeof(BankAccountType), bankAccount.AccountType))
                    throw new InvalidArgumentException($"Applicant {nameof(bankAccount.AccountType)} is not valid", GetEnumNames<BankAccountType>());
            });

            var hasMultipleActiveAccount = bankAccounts.Count(account => account.IsPrimary);


            if (hasMultipleActiveAccount > 1)
                throw new InvalidArgumentException("There is multiple active bank accounts in the payload, please verify");

            if (hasMultipleActiveAccount > 1)
                throw new InvalidArgumentException("There is multiple active bank accounts in the payload, please verify");

            var hasMultipleAutoPayment = bankAccounts.Count(account => account.AutoPayment);
            if (hasMultipleAutoPayment > 1)
                throw new InvalidArgumentException("There is multiple bank accounts with auto payment in the payload, please verify");

        }

        private void SetBankAccountId(List<IBankAccount> bankAccounts)
        {
            bankAccounts.ForEach(bankAccount => { bankAccount.Id = $"{bankAccount.AccountNumber}-{bankAccount.RoutingNumber}"; });
        }

        private void EnsureNoDuplicatedBankAccount(List<IBankAccount> bankAccounts)
        {
            var hasDuplicated = bankAccounts
                .GroupBy(b => new { b.AccountNumber, b.RoutingNumber })
                .Any(group => group.Count() > 1);

            if (hasDuplicated)
                throw new InvalidArgumentException("There is duplicated bank accounts in the payload, please verify");
        }

        private void EnsurePrimaryBankAccount(List<IBankAccount> bankAccounts)
        {
            // if not informed first bank must to be assumed as primary
            if (bankAccounts.Any() && bankAccounts.TrueForAll(b => b.IsPrimary == false))
                bankAccounts.First().IsPrimary = true;
        }

        private void PublishOptedInForAutoPayment(string applicationNumber)
        {
            EventHubClient.Publish(new ApplicationOptedInForAutoPayment { ApplicationNumber = applicationNumber });
        }

        public void UpdateAnnualIncome(string applicationNumber, double annualIncome)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new InvalidArgumentException($"{nameof(applicationNumber)} is mandatory");

            if (annualIncome <= 0)
                throw new InvalidArgumentException($"{nameof(annualIncome)} must to be greater then zero");

            var application = Repository.GetByApplicationNumber(applicationNumber);
            if (application == null)
                throw new NotFoundException($"{nameof(applicationNumber)} cannot be found");

            Repository.UpdateAnnualIncome(applicationNumber, annualIncome);
            EventHubClient.Publish(new ApplicationModified() { Application = application });
        }

        public bool IsDuplicate(string applicationNumber)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new InvalidArgumentException($"The {nameof(applicationNumber)} cannot be null", nameof(applicationNumber));

            var application = Repository.GetByApplicationNumber(applicationNumber);
            if (application == null)
                throw new NotFoundException($"Application {applicationNumber} not found");

            if (Configuration == null)
                throw new InvalidArgumentException($"The {nameof(Configuration)} related to Application cannot be found", nameof(Configuration));

            if (Configuration.DuplicateStatuses == null || !Configuration.DuplicateStatuses.Any())
                throw new InvalidArgumentException($"The {nameof(Configuration.DuplicateStatuses)} cannot be null", nameof(Configuration.DuplicateStatuses));

            var duplicateStatuses = Configuration.DuplicateStatuses;

            if (!application.Applicants.Any())
                throw new NotFoundException($"Applicants not found");

            var applicantData = application.Applicants.FirstOrDefault(a => a.IsPrimary);
            if (applicantData == null)
                throw new NotFoundException($"Applicant not found");

            var applicant = ApplicantService.Get(applicantData.ApplicantId);
            if (applicant == null)
                throw new NotFoundException($"Applicant not found");

            var duplicateApplicants = ApplicantService.Search
            (
                new SearchApplicantRequest
                {
                    Ssn = applicant.Ssn,
                    Email = applicant.Email,
                    FirstName = applicant.FirstName,
                    LastName = applicant.LastName,
                    DateOfBirth = applicant.DateOfBirth
                }
            )?.Where(applic => applic.Id != applicant.Id).ToList();

            return duplicateStatuses.Contains(application.Status.Code) && duplicateApplicants.Any();

        }

        public IApplicationUnassigned GetUnassigned(string applicationNumber)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new InvalidArgumentException($"{nameof(applicationNumber)} is mandatory");

            var application = GetApplicationByApplicationNumber(applicationNumber);

            var applicantData = application.Applicants.FirstOrDefault(x => x.IsPrimary);
            if (applicantData == null)
                throw new NotFoundException($"Applicant not found for application {application.ApplicationNumber}");

            var applicant = ApplicantService.Get(applicantData.ApplicantId);
            return new ApplicationUnassigned(application, applicant);
        }
    }
}