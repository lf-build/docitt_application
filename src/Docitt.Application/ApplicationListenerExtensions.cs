﻿
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Docitt.Application
{
    public static class ApplicationListenerExtensions
    {
        public static void UseApplicationListener(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetRequiredService<IApplicationListener>().Start();
        }
    }
}