﻿using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Listener;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.StatusManagement;
using LendFoundry.StatusManagement.Client;
using LendFoundry.Tenant.Client;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Docitt.Application
{
    public class ApplicationListener : ListenerBase, IApplicationListener
    {
        public ApplicationListener
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            IApplicationRepositoryFactory repositoryFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
            IStatusManagementServiceFactory statusManagementFactory
        ) : base(tokenHandler, eventHubFactory, loggerFactory, tenantServiceFactory, Settings.ServiceName)

        {
            StatusManagementFactory = statusManagementFactory;
            EventHubFactory = eventHubFactory;
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            RepositoryFactory = repositoryFactory;
        }

        private IConfigurationServiceFactory ConfigurationFactory { get; }

        private IEventHubClientFactory EventHubFactory { get; }

        private ITokenHandler TokenHandler { get; }

        private IApplicationRepositoryFactory RepositoryFactory { get; }

        private IStatusManagementServiceFactory StatusManagementFactory { get; }

        public override List<string> SubscribeEvents(string tenant, ILogger logger)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);

            var eventhub = EventHubFactory.Create(reader);

            var repository = RepositoryFactory.Create(reader);

            var statusManagementService = StatusManagementFactory.Create(reader);

            var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            configurationService.ClearCache(Settings.ServiceName);
            var configuration = configurationService.Get();


            if (configuration == null)
            {
                logger.Error(
                    $"The configuration for service #{Settings.ServiceName} could not be found for {tenant} , please verify");
                return null;
            }
            else
            {
                logger.Info($"#{configuration.Events.Length} events found from configuration: {Settings.ServiceName}");

                var uniqueEvents = configuration
                    .Events
                    .Distinct()
                    .ToList();


                uniqueEvents
                    .ForEach(eventConfig =>
                    {
                        eventhub.On(eventConfig.Name,
                            UpdateStatus(eventConfig, repository, logger, statusManagementService));
                        logger.Info(
                            $"It was made subscription to EventHub with the Event: #{eventConfig.Name} for tenant {tenant}");
                    });


                return uniqueEvents.Select(p => p.Name).Distinct().ToList();
            }
        }

        public override List<string> GetEventNames(string tenant)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            configurationService.ClearCache(Settings.ServiceName);
            var configuration = configurationService.Get();
            return configuration?.Events.Select(evnt => evnt.Name).Distinct().ToList();
        }


        private static Action<EventInfo> UpdateStatus(EventMapping eventConfiguration,
            IApplicationRepository repository, ILogger logger, IEntityStatusService statusManagementService)
        {
            return @event =>
            {
                try
                {
                    var applicationNumber = eventConfiguration.ApplicationNumber.FormatWith(@event);
                    var applicationStatus = statusManagementService
                        .GetStatusByEntity(Constants.Application, applicationNumber).Result;

                    if (applicationStatus != null)
                    {
                        var status = new Status
                        {
                            Code = applicationStatus.Code,
                            Name = applicationStatus.Name,
                            Label = applicationStatus.Label
                        };
                        
                        repository.UpdateStatus(applicationNumber,status);

                    }
                    else
                    {
                        logger.Warn(
                            $"Status for application #{applicationNumber} has been received empty, no changes affected");
                    }
                }
                catch (Exception ex)
                {
                    logger.Error($"Unhadled exception while listening event #{@event.Name}", ex, @event);
                }
            };
        }
    }
}