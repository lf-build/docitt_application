﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.Logging;
using Microsoft.Extensions.DependencyInjection;

using System;

namespace Docitt.Application.Persistence
{
    public class ApplicationRepositoryFactory : IApplicationRepositoryFactory
    {
        public ApplicationRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IApplicationRepository Create(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            
            var mongoConfigurationFactory =Provider.GetService<IMongoConfigurationFactory>();
            var mongoConfiguration = mongoConfigurationFactory.Get(reader, Provider.GetService<ILogger>());

            return new ApplicationRepository(tenantService, mongoConfiguration);
        }
    }
}