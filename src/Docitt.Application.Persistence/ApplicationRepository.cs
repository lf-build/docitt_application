﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver.Linq;

namespace Docitt.Application.Persistence
{
    public class ApplicationRepository : MongoRepository<IApplication, Application>, IApplicationRepository
    {
        static ApplicationRepository()
        {
            BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IApplicantData, ApplicantData>());

            BsonClassMap.RegisterClassMap<Application>(map =>
            {
                map.AutoMap();
                map.MapProperty(p => p.Channel);
                map.MapProperty(p => p.Applicants).SetIgnoreIfDefault(true);
                map.MapProperty(p => p.ScoreRange).SetIgnoreIfDefault(true);
                var type = typeof(Application);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<ApplicantData>(map =>
            {
                map.AutoMap();
                map.MapProperty(p => p.HomeOwnership).SetSerializer(new EnumSerializer<HomeOwnershipType>(BsonType.String));
                map.MapProperty(p => p.EmploymentStatus).SetSerializer(new EnumSerializer<EmploymentStatus>(BsonType.String));
                map.MapProperty(p => p.Addresses).SetIgnoreIfDefault(true);
                map.MapProperty(p => p.PhoneNumbers).SetIgnoreIfDefault(true);
                map.MapProperty(p => p.ApplicantType).SetSerializer(new EnumSerializer<ApplicantType>(BsonType.String));
                var type = typeof(ApplicantData);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<Address>(map =>
            {
                map.AutoMap();
                map.MapProperty(p => p.Type).SetSerializer(new EnumSerializer<AddressType>(BsonType.String));
                var type = typeof(Address);
                map.SetDiscriminator($"{type.FullName}, Docitt.Application.Abstractions");
            });

            BsonClassMap.RegisterClassMap<PhoneNumber>(map =>
            {
                map.AutoMap();
                var type = typeof(PhoneNumber);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<Employer>(map =>
            {
                map.AutoMap();
                var type = typeof(Employer);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<ScoreRange>(map =>
            {
                map.AutoMap();
                var type = typeof(ScoreRange);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<Source>(map =>
            {
                map.AutoMap();
                map.MapProperty(p => p.Type).SetSerializer(new EnumSerializer<SourceType>(BsonType.String));
                var type = typeof(Source);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<BankAccount>(map =>
            {
                map.AutoMap();
                map.MapProperty(p => p.AccountType).SetSerializer(new EnumSerializer<BankAccountType>(BsonType.String));
                var type = typeof(BankAccount);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<Status>(map =>
            {
                map.AutoMap();
                map.MapProperty(p => p.Label);
                var type = typeof(Status);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public ApplicationRepository
        (
            LendFoundry.Tenant.Client.ITenantService tenantService, 
            IMongoConfiguration configuration
        )
        : base(tenantService, configuration, "applications")
        {
            CreateIndexIfNotExists("applications_applicationNumber", Builders<IApplication>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.ApplicationNumber), true);
        }

        public IApplication GetByApplicationNumber(string applicationNumber)
        {
            return Query.FirstOrDefault(application => application.ApplicationNumber == applicationNumber);
        }

        public List<string> GetApplicationNumbersByApplicantNumber(string applicantId)
        {
            return
                Query.Where(
                    application =>
                        application.Applicants.Any(applicant => applicantId == applicant.ApplicantId))
                    .Select(application => application.ApplicationNumber)
                    .ToList();
        }
        public async Task<IApplication> GetApplicationByApplicantId (string applicantId) 
        {
            return await Query.FirstOrDefaultAsync (a => a.Applicants.Any(x=> x.ApplicantId == applicantId));
        }

        public void UpdateStatus(string applicationNumber, Status status)
        {
            Collection.UpdateOne
            (
                Builders<IApplication>.Filter
                    .Where(a => a.TenantId == TenantService.Current.Id &&
                                a.ApplicationNumber == applicationNumber),
                Builders<IApplication>.Update
                    .Set(a => a.Status, status).AddToSet(a => a.CompletedStatuses, status));

        }

        public void UpdateApplication(string applicationNumber, IApplicationUpdateRequest applicationRequest)
        {
            Collection.UpdateOne
            (
                Builders<IApplication>.Filter
                    .Where(a => a.TenantId == TenantService.Current.Id && 
                                a.ApplicationNumber == applicationNumber),
                Builders<IApplication>.Update.Set(a => a.Purpose, applicationRequest.Purpose)
                    .Set(a => a.Amount, applicationRequest.Amount)
                    .Set(a => a.Source, applicationRequest.Source)
                    .Set(a => a.Channel, applicationRequest.Channel)
                    .Set(a => a.ScoreRange, applicationRequest.ScoreRange));
        }

        public IApplication UpdateAddresses(string applicationNumber, string applicantId, List<IAddress> addresses)
        {
            var application = GetByApplicationNumber(applicationNumber);

            if (application == null)
                throw new NotFoundException($"Application {applicationNumber} not found");

            var applicants = application.Applicants;
            var applicantToUpdate = applicants.FirstOrDefault(applicant => applicant.ApplicantId == applicantId);

            if (applicantToUpdate == null)
                throw new NotFoundException($"Applicant {applicantId} not found for application {applicationNumber} ");

            applicantToUpdate.Addresses = addresses;

            Collection.UpdateOne
            (
                Builders<IApplication>.Filter
                    .Where(a => a.TenantId == TenantService.Current.Id &&
                                a.ApplicationNumber == applicationNumber),
                Builders<IApplication>.Update.Set(a => a.Applicants, applicants)
            );

            return application;
        }

        public IApplication UpdatePrimaryAddress(string applicationNumber, List<IAddress> addresses)
        {
            var application = GetByApplicationNumber(applicationNumber);

            if (application == null)
                throw new NotFoundException($"Application {applicationNumber} not found");

            var applicants = application.Applicants;
            var applicantToUpdate = applicants.FirstOrDefault(applicant => applicant.IsPrimary);

            if (applicantToUpdate == null)
                throw new NotFoundException($"Primary Applicant not found for application {applicationNumber} ");

            applicantToUpdate.Addresses = addresses;

            Collection.UpdateOne
            (
                Builders<IApplication>.Filter
                    .Where(a => a.TenantId == TenantService.Current.Id &&
                                a.ApplicationNumber == applicationNumber),
                Builders<IApplication>.Update.Set(a => a.Applicants, applicants)
            );

            return application;
        }

        public IApplication UpdatePhoneNumbers(string applicationNumber, string applicantId, List<IPhoneNumber> phoneNumbers)
        {
            var application = GetByApplicationNumber(applicationNumber);

            if (application == null)
                throw new NotFoundException($"Application {applicationNumber} not found");

            var applicants = application.Applicants;
            var applicantToUpdate = applicants.FirstOrDefault(applicant => applicant.ApplicantId == applicantId);

            if (applicantToUpdate == null)
                throw new NotFoundException($"Applicant {applicantId} not found for application {applicationNumber} ");

            applicantToUpdate.PhoneNumbers = phoneNumbers;

            Collection.UpdateOne
            (
                Builders<IApplication>.Filter
                    .Where(a => a.TenantId == TenantService.Current.Id &&
                                a.ApplicationNumber == applicationNumber),
                Builders<IApplication>.Update.Set(a => a.Applicants, applicants)
            );

            return application;
        }

        public IApplication UpdateApplicant(string applicationNumber, string applicantId, IApplicantUpdateRequest applicantUpdateRequest)
        {
            var application = GetByApplicationNumber(applicationNumber);

            if (application == null)
                throw new NotFoundException($"Application {applicationNumber} not found");

            var applicants = application.Applicants;
            var applicantToUpdate = applicants.FirstOrDefault(applicant => applicant.ApplicantId == applicantId);

            if (applicantToUpdate == null)
                throw new NotFoundException($"Applicant {applicantId} not found for application {applicationNumber} ");

            applicantToUpdate.CurrentEmployer = applicantUpdateRequest.CurrentEmployer;
            applicantToUpdate.EmploymentStatus = applicantUpdateRequest.EmploymentStatus;
            applicantToUpdate.HomeOwnership = applicantUpdateRequest.HomeOwnership;
            applicantToUpdate.StatedGrossAnnualIncome = applicantUpdateRequest.StatedGrossAnnualIncome;

            Collection.UpdateOne(Builders<IApplication>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                          a.ApplicationNumber == applicationNumber),
                Builders<IApplication>.Update.Set(a => a.Applicants, applicants));
            return application;
        }

        public IApplication UpdateScore(string applicationNumber, string scoreName, Score score)
        {
            var application = GetByApplicationNumber(applicationNumber);

            if (application == null)
                throw new NotFoundException($"Application {applicationNumber} cannot be found");

            if (!application.Scores.Any(s => s.Name == scoreName))
                throw new NotFoundException($"Score {scoreName} cannot be found");

            application.Scores = application.Scores.Select(s =>
            {
                if (s.Name == scoreName)
                    s = score;
                return s;
            });

            Collection.UpdateOne
            (
                filter: Builders<IApplication>.Filter.Where(a => a.TenantId == TenantService.Current.Id && a.ApplicationNumber == applicationNumber),
                update: Builders<IApplication>.Update.Set(a => a.Scores, application.Scores)
            );
            return application;
        }

        public IApplication UpdateBankAccounts(string applicationNumber, List<IBankAccount> bankAccounts)
        {
            var application = GetByApplicationNumber(applicationNumber);

            if (application == null)
                throw new NotFoundException($"Application {applicationNumber} not found");

            application.BankAccounts = bankAccounts;

            var filter = Builders<IApplication>.Filter.Where(a => a.TenantId == TenantService.Current.Id && a.ApplicationNumber == applicationNumber);
            Collection.UpdateOne(filter, Builders<IApplication>.Update.Set(a => a.BankAccounts, bankAccounts));
            return application;
        }

        public void UpdateAnnualIncome(string applicationNumber, double annualIncome)
        {            
            Collection.UpdateOne
            (
                filter: Builders<IApplication>.Filter.Where(a => a.TenantId == TenantService.Current.Id && a.ApplicationNumber == applicationNumber),
                update: Builders<IApplication>.Update.Set(a => a.VerifiedAnnualIncome, annualIncome)
            );            
        }
    }
}